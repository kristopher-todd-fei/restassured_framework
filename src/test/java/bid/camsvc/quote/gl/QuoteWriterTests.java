package bid.camsvc.quote.gl;

import static io.restassured.RestAssured.given;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;

import camsvc.writers.common.RestUtilities;
import camsvc.writers.common.ReusableMethods;
import camsvc.writers.constants.Endpoints;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import payLoads.bid.quotegl.Quote;

@RunWith(DataProviderRunner.class)
public class QuoteWriterTests {
	private static Logger log = LogManager.getLogger(QuoteWriterTests.class.getName());

	Long syncId;
	RequestSpecification reqSpec;
	ResponseSpecification resSpec;

	@Before
	public void setup() {
		syncId = ReusableMethods.syncId();

		reqSpec = RestUtilities.getRequestSpecification_Quotes();

		resSpec = RestUtilities.getResponseSpecification();
		resSpec.header("amq-destination-name", "rti.order");
	}

	@Test
	@UseDataProvider("data")
	public void quote_writer_tests(String type, String test_name) throws IOException {
		log.info("Begin Test: " + test_name);
		log.info("Sync ID used for this test: " + syncId);

		given()
			.spec(reqSpec)
			.body(Quote.quoteData(type, ReusableMethods.dateTime(), syncId))
		.when()
			.post(Endpoints.MESSAGE_PUBLISHER)
		.then()
			.spec(resSpec);

		ReusableMethods.validateMongoDBData("quote", type, "eventMeta.syncId", syncId);

		log.info("End Test: " + test_name + "\n");
	}

	@DataProvider
	public static Object[][] data() {
		return new Object[][] { 
			{ "TEST", "MTC001_quote_invalid_quote_type" }, 
			{ "ADD", "MTC002_quote_EMP_ADD" },
			{ "UPDATE", "MTC003_quote_EMP_UPDATE" },
			{ "BULK", "MTC004_quote_EMP_BULK" },
			{ "DELETE", "MTC005_quote_EMP_DELETE" } 
		};
	}
}
