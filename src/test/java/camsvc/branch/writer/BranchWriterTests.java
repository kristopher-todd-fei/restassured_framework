package camsvc.branch.writer;

import static io.restassured.RestAssured.given;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;

import camsvc.writers.common.RestUtilities;
import camsvc.writers.common.ReusableMethods;
import camsvc.writers.constants.Endpoints;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import payLoads.Branch;

@RunWith(DataProviderRunner.class)
public class BranchWriterTests {
	private static Logger log = LogManager.getLogger(BranchWriterTests.class.getName());
	
	Long syncId;
	RequestSpecification reqSpec;
	ResponseSpecification resSpec;
	
	@Before
	public void setup() {
		syncId = ReusableMethods.syncId();		
		reqSpec = RestUtilities.getRequestSpecification();		
		resSpec = RestUtilities.getResponseSpecification();
		resSpec.header("amq-destination-name", "rti.branch");
	}
	
	@Test
	@UseDataProvider("data")
	public void branch_tests(String type, String test_name) throws IOException {
		log.info("Begin Test: " + test_name);
		log.info("Sync ID used for this test: " + syncId);
		given()
			.spec(reqSpec)
			.body(Branch.branchData(type, ReusableMethods.dateTime(), syncId))
		.when()
			.post(Endpoints.MESSAGE_PUBLISHER)
		.then()
			.spec(resSpec);
		
		ReusableMethods.validateMongoDBData("branch", type, "eventMeta.syncId", syncId);		
		log.info("End Test: " + test_name + "\n");
	}
	
	@DataProvider
	public static Object[][] data() {
		return new Object[][] {
			{"TEST", "MTC009_branch_invalid_branch_type"},
			{"ADD", "MTC011_branch_EMP_ADD"},
			{"UPDATE", "MTC012_branch_EMP_UPDATE"},
			{"BULK", "MTC013_branch_EMP_BULK"},
			{"DELETE", "MTC014_branch_EMP_DELETE"}
		};
	}
}
