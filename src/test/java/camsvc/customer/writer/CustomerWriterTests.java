package camsvc.customer.writer;

import static io.restassured.RestAssured.given;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;

import camsvc.writers.common.RestUtilities;
import camsvc.writers.common.ReusableMethods;
import camsvc.writers.constants.Endpoints;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import payLoads.Customer;

@RunWith(DataProviderRunner.class)
public class CustomerWriterTests {
	private static Logger log = LogManager.getLogger(CustomerWriterTests.class.getName());
	
	Long syncId;
	RequestSpecification reqSpec;
	ResponseSpecification resSpec;
	
	@Before
	public void setup() {
		syncId = ReusableMethods.syncId();
		
		reqSpec = RestUtilities.getRequestSpecification();
		
		resSpec = RestUtilities.getResponseSpecification();
		resSpec.header("amq-destination-name", "rti.customer");
	}
	
	@Test
	@UseDataProvider("data")
	public void customer_tests(String type, String test_name) throws IOException {
		log.info("Begin Test: " + test_name);
		log.info("Sync ID used for this test: " + syncId);
		
		given()
			.spec(reqSpec)
			.body(Customer.customerData(
					type, 
					ReusableMethods.dateTime(), 
					syncId))
		.when()
			.post(Endpoints.MESSAGE_PUBLISHER)
		.then()
			.spec(resSpec);
		
		ReusableMethods.validateMongoDBData("customer", type, "eventMeta.syncId", syncId);
		
		log.info("End Test: " + test_name + "\n");
	}
		
	@DataProvider
	public static Object[][] data() {
		return new Object[][] {
			{"TEST", "MTC001_customer_invalid_customer_type"},
			{"ADD", "MTC002_customer_EMP_ADD"},
			{"UPDATE", "MTC003_customer_EMP_UPDATE"},
			{"BULK", "MTC004_customer_EMP_BULK"},
			{"DELETE", "MTC005_customer_EMP_DELETE"}
		};
	}
}
