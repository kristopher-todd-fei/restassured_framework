package camsvc.bidtracer.publisher.branch;

import static io.restassured.RestAssured.given;

import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;

import camsvc.writers.common.RestUtilities;
import camsvc.writers.common.ReusableMethods;
import camsvc.writers.constants.Endpoints;
import io.restassured.specification.RequestSpecification;
import payLoads.bidtracerPublisher_Branch.BidtracerPublisherBranch;

@RunWith(DataProviderRunner.class)
public class BidtracerPublisher_Branch_Tests {
	
	RequestSpecification reqSpec;
	
	@Before
	public void init() {
		reqSpec = RestUtilities.getRequestSpecification();
	}
	
	@Test
	@UseDataProvider("data")
	public void MCT001_jobCustFlagEquals_N(String type, Long syncId) throws IOException, InterruptedException {
		System.out.println("SYNC ID: " + syncId);
		given()
			.spec(reqSpec)
			.body(BidtracerPublisherBranch.branchCustomer(type, syncId, "N"))
		.when()
			.post(Endpoints.MESSAGE_PUBLISHER)
		.then()
			.statusCode(200);
		
		ReusableMethods.validateMongoData_HashCode("hash", "syncId", syncId);
		
	}
	
	@After
	public void tearDown() throws InterruptedException {
		Thread.sleep(10000);
		System.out.println("test complete\n");
	}
	
	@DataProvider
	public static Object[][] data() {
		Long syncId = ReusableMethods.syncId();
		return new Object[][] {
			{"ADD", syncId},
			{"UPDATE", syncId + 1},
			{"UPDATE", syncId + 1}
		};
	}
}
