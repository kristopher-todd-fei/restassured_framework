package camsvc.address.verification;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

import org.junit.Assert;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import camsvc.writers.common.RestUtilities;
import camsvc.writers.constants.Endpoints;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AddressVerifificationTests {
	
	RequestSpecification reqSpec;
	ResponseSpecification respSpec;
	static String city = null;
	static String stateAbbr = null;
	
	@Before
	public void init() {
		reqSpec = RestUtilities.getRequestSpecification_AddressValidation();
		respSpec = RestUtilities.getResponseSpecification_AddressValidation();
	}
	
	@Test
	public void MTC001_valid_request_US() {
		Response resp = 
		given()
			.spec(RestUtilities.createQueryParam(reqSpec, "country", "US"))
		.when()
			.get(Endpoints.ZIP_TO_CITY + "/44212")
		.then()
			.spec(respSpec)
			.extract().response();
		
		JsonPath jsPath = RestUtilities.getJsonPath(resp);
		city = jsPath.get("City");
		stateAbbr = jsPath.get("State");
		Assert.assertThat(city, equalTo("Brunswick"));
		Assert.assertThat(stateAbbr, equalTo("OH"));
	}
	
	@Test
	public void MTC001_valid_request_CA() {
		Response resp = 
		given()
			.spec(RestUtilities.createQueryParam(reqSpec, "country", "CANADA"))
		.when()
			.get(Endpoints.ZIP_TO_CITY + "/V6E1C3")
		.then()
			.spec(respSpec)
			.extract().response();
		
		JsonPath jsPath = RestUtilities.getJsonPath(resp);
		city = jsPath.get("City");
		stateAbbr = jsPath.get("State");
		Assert.assertThat(city, equalTo("Vancouver"));
		Assert.assertThat(stateAbbr, equalTo("BC"));
	}
	
	@Test
	public void MTC002_invalid_request_invalid_zip() {
		Response resp = 
		given()
			.spec(RestUtilities.createQueryParam(reqSpec, "country", "US"))
		.when()
			.get(Endpoints.ZIP_TO_CITY + "/11111")
		.then()
			.statusCode(404).extract().response();
		
		JsonPath jsPath = RestUtilities.getJsonPath(resp);
		boolean isError = jsPath.get("isError");
		String responseMessage = jsPath.get("respMsg");
		String responseCode = jsPath.get("respCode");
		
		Assert.assertThat(isError, equalTo(true));
		Assert.assertThat(responseMessage, equalTo("empty.response.list"));
		Assert.assertThat(responseCode, equalTo("not.found"));
	}
	
	@Test
	public void MTC002_invalid_request_invalid_country() {
		Response resp = 
		given()
		.spec(RestUtilities.createQueryParam(reqSpec, "country", "FRANCE"))
		.when()
			.get(Endpoints.ZIP_TO_CITY + "/23664")
		.then()
			.statusCode(400).extract().response();
		
		JsonPath jsPath = RestUtilities.getJsonPath(resp);
		boolean isError = jsPath.get("isError");
		String responseMessage = jsPath.get("respMsg");
		String responseCode = jsPath.get("respCode");
		
		Assert.assertThat(isError, equalTo(true));
		Assert.assertThat(responseMessage, equalTo("Invalid ZipCode or Country"));
		Assert.assertThat(responseCode, equalTo("bad.request"));
	}
	
	@Test
	public void MTC003_only_valid_zip_code() {
		Response resp = 
		given()
			.spec(reqSpec)
		.when()
			.get(Endpoints.ZIP_TO_CITY + "/23664")
		.then()
			.statusCode(200).extract().response();
		
		JsonPath jsPath = RestUtilities.getJsonPath(resp);
		city = jsPath.get("City");
		stateAbbr = jsPath.get("State");
		Assert.assertThat(city, equalTo("Hampton"));
		Assert.assertThat(stateAbbr, equalTo("VA"));
	}
	
	@Test
	public void MTC004_invalid_request_empty_request() {
		given()
			.spec(reqSpec)
		.when()
			.get(Endpoints.ZIP_TO_CITY)
		.then()
			.log().all()
			.statusCode(404);
	}
}
