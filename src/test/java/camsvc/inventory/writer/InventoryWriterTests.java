package camsvc.inventory.writer;

import static io.restassured.RestAssured.given;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;

import camsvc.writers.common.RestUtilities;
import camsvc.writers.common.ReusableMethods;
import camsvc.writers.constants.Endpoints;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import payLoads.Inventory;

@RunWith(DataProviderRunner.class)
public class InventoryWriterTests {
	private static Logger log = LogManager.getLogger(InventoryWriterTests.class.getName());
	
	Long syncId;
	RequestSpecification reqSpec;
	ResponseSpecification resSpec;
	
	@Before
	public void setup() {
		syncId = ReusableMethods.syncId();
		
		reqSpec = RestUtilities.getRequestSpecification();
		
		resSpec = RestUtilities.getResponseSpecification();
		resSpec.header("amq-destination-name", "rti.inventory");
	}
	
	@Test
	@UseDataProvider("data")
	public void inventory_writer_tests(String type, String test_name) throws IOException {
		log.info("Begin Test: " + test_name);
		log.info("Sync ID used for this test: " + syncId);
		
		given()
			.spec(reqSpec)
			.body(Inventory.inventoryData(
					type,
					ReusableMethods.dateTime(),
					syncId))
		.when()
			.post(Endpoints.MESSAGE_PUBLISHER)
		.then()
			.spec(resSpec);
		
		ReusableMethods.validateMongoDBData("inventory", type, "eventMeta.syncId", syncId);
		
		log.info("End Test: " + test_name + "\n");
	}
	
	@DataProvider
	public static Object[][] data() {
		return new Object[][] {
			{"TEST", "MTC009_inventory_invalid_inventory_type"},
			{"ADD", "MTC011_inventory_EMP_ADD"},
			{"UPDATE", "MTC012_inventory_EMP_UPDATE"},
			{"BULK", "MTC013_inventory_EMP_BULK"},
			{"DELETE", "MTC014_inventory_EMP_DELETE"}
		};
	}
}
