package camsvc.regression.writer;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import bid.camsvc.quote.gl.QuoteWriterTests;
import camsvc.branch.writer.BranchWriterTests;
import camsvc.customer.writer.CustomerWriterTests;
import camsvc.employee.writer.EmployeeWriterTests;
import camsvc.inventory.writer.InventoryWriterTests;
import camsvc.sales.order.GetSalesOrderTests;

@RunWith(Suite.class)
@SuiteClasses(
		{
			BranchWriterTests.class,
			CustomerWriterTests.class,
			EmployeeWriterTests.class,
			InventoryWriterTests.class,
			QuoteWriterTests.class,
			GetSalesOrderTests.class
		}
	)

public class RegressionTests {
	// This is a test  of the Git repository system
}
