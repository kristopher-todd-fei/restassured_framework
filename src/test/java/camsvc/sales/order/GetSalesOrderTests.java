package camsvc.sales.order;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.Assert;
// import org.apache.logging.log4j.LogManager;
// import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import camsvc.writers.common.RestUtilities;
import camsvc.writers.constants.Endpoints;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class GetSalesOrderTests {
	
	// private static Logger log = LogManager.getLogger(GetSalesOrderTests.class.getName());
	
	RequestSpecification reqSpec;
	ResponseSpecification resSpec;	
	Properties prop = new Properties();
	Response resp;
	JsonPath jsPath;
	boolean isError;
	String responseMessage;
	String responseCode;
	
	@Before
	public void setup() {
		reqSpec = RestUtilities.getRequestSpecification_SalesOrders();		
		resSpec = RestUtilities.getResponseSpecification_SalesOrders();
	}
	
	@Test
	public void MTC001_getSalesOrderWhenListOfAttributesIsGiven() {
		Map<String, String> params = new HashMap<String, String>();
		params.put("orderAcctId", "MYERS");
		params.put("orderAttrList", "header,lineItems");
		
		resp = given()
			.spec(RestUtilities.createQueryParam(reqSpec, params))
		.when()
			// Valid Order ID
			.get(Endpoints.SALES_ORDER + "/CM180052")
		.then()
			// .log().all()
			.statusCode(200)
			.spec(resSpec)
			.extract().response();
		
		jsPath = RestUtilities.getJsonPath(resp);
		isError = jsPath.get("isError");
		Assert.assertThat(isError, equalTo(false));
		
		// TODO: validate attributes specified in test
	}
	
	@Test
	public void MTC002_getSalesOrderWhenIdAndOrderAcctIdIsGiven() {
		
		resp = given()
			.spec(RestUtilities.createQueryParam(reqSpec, "orderAcctId", "MYERS"))
	    .when()
	    	.get(Endpoints.SALES_ORDER + "/0882514")
		.then()
			// .log().all()
			.spec(resSpec)
			.extract().response();
		
		jsPath = RestUtilities.getJsonPath(resp);
		isError = jsPath.get("isError");
		Assert.assertThat(isError, equalTo(false));
	}
	
	@Test
	public void MTC003_getSalesOrderWhenSalesOrderDetailsAreRequested() {
		Map<String, String> params = new HashMap<String, String>();
		params.put("orderAcctId", "MYERS");
		params.put("orderAttrList", "header");
		params.put("page", "1");
		
		resp = given()
			.spec(RestUtilities.createQueryParam(reqSpec, params))
	    .when()
	    	.get(Endpoints.SALES_ORDER + "/CM180052")
		.then()
			// .log().all()
			.statusCode(200)
			.spec(resSpec)
			.extract().response();
		
		jsPath = RestUtilities.getJsonPath(resp);
		isError = jsPath.get("isError");
		Assert.assertThat(isError, equalTo(false));
	}
	
	@Test
	public void MTC004_getSalesOrderWhenNoPageFieldIsGiven() {
		Map<String, String> params = new HashMap<String, String>();
		params.put("orderAcctId", "MYERS");
		params.put("orderAttrList", "header");
		
		resp = given()
			.spec(RestUtilities.createQueryParam(reqSpec, params))		
	    .when()
	    	.get(Endpoints.SALES_ORDER + "/CM180052")
		.then()
			// .log().all()
			.statusCode(200)
			.spec(resSpec)
			.extract().response();
		
		jsPath = RestUtilities.getJsonPath(resp);
		isError = jsPath.get("isError");
		Assert.assertThat(isError, equalTo(false));
	}
	
	@Test
	public void MTC005a_getSalesWhenAttributeList_LTFifty() throws IOException {
		FileInputStream fis = new FileInputStream(
				System.getProperty("user.dir") + File.separator + "writer.properties");
		prop.load(fis);
		
		Map<String, String> params = new HashMap<String, String>();
		params.put("orderAcctId", "MYERS");
		params.put("orderAttrList", prop.getProperty("ORDER_ATTR_LIST_LT_FIFTY"));
		params.put("page", "1");
		
		resp = given()
			.spec(RestUtilities.createQueryParam(reqSpec, params))
	    .when()
	    	.get(Endpoints.SALES_ORDER + "/0882514")
		.then()
			// .log().all()
			.statusCode(200)
			.spec(resSpec)
			.extract().response();
		
		jsPath = RestUtilities.getJsonPath(resp);
		isError = jsPath.get("isError");
		
		Assert.assertThat(isError, equalTo(false));
	}
	
	@Test
	public void MTC005b_getSalesWhenAttributeList_ETFifty() throws IOException {
		FileInputStream fis = new FileInputStream(
				System.getProperty("user.dir") + File.separator + "writer.properties");
		prop.load(fis);
		Map<String, String> params = new HashMap<String, String>();
		params.put("orderAcctId", "MYERS");
		params.put("orderAttrList", prop.getProperty("ORDER_ATTR_LIST_ET_FIFTY"));
		params.put("page", "1");
		
		resp = given()
			.spec(RestUtilities.createQueryParam(reqSpec, params))
	    .when()
	    	.get(Endpoints.SALES_ORDER + "/0882514")
		.then()
			// .log().all()
			.statusCode(200)
			.spec(resSpec)
			.extract().response();
		
		jsPath = RestUtilities.getJsonPath(resp);
		isError = jsPath.get("isError");
		
		Assert.assertThat(isError, equalTo(false));
	}
	
	@Test
	public void MTC005c_getSalesWhenAttributeList_GTFifty() throws IOException {
		FileInputStream fis = new FileInputStream(
				System.getProperty("user.dir") + File.separator + "writer.properties");
		prop.load(fis);
		
		Map<String, String> params = new HashMap<String, String>();
		params.put("orderAcctId", "MYERS");
		params.put("orderAttrList", prop.getProperty("ORDER_ATTR_LIST_GT_FIFTY"));
		params.put("page", "1");
		
		resp = given()
			.spec(RestUtilities.createQueryParam(reqSpec, params))
	    .when()
	    	.get(Endpoints.SALES_ORDER + "/0882514")
		.then()
			// .log().all()
			.statusCode(400)
			.spec(resSpec)
			.extract().response();
		
		jsPath = RestUtilities.getJsonPath(resp);
		isError = jsPath.get("isError");
		responseMessage = jsPath.get("respMsg");
		responseCode = jsPath.get("respCode");
		
		Assert.assertThat(isError, equalTo(true));
		Assert.assertThat(responseMessage, equalTo("Max # of 50 attributes can be requested in the payload. Please modify list"));
		Assert.assertThat(responseCode, equalTo("max.attributes.count.exceeded"));
	}
	
	@Test
	public void MTC006a_getSaleswhenOrderIdIsNotFound() throws IOException {		
		Map<String, String> params = new HashMap<String, String>();
		params.put("orderAcctId", "MYERS");
		params.put("orderAttrList", "header");
		params.put("page", "1");
		
		resp = given()
			.log().all()
			.spec(RestUtilities.createQueryParam(reqSpec, params))
	    .when()
	    	// invalid OrderID
	    	.get(Endpoints.SALES_ORDER + "/1882514")
		.then()
			.log().all()
			.statusCode(404)
			.spec(resSpec)
			.extract().response();
		
		jsPath = RestUtilities.getJsonPath(resp);
		isError = jsPath.get("isError");
		responseMessage = jsPath.get("respMsg");
		responseCode = jsPath.get("respCode");
		
		Assert.assertThat(isError, equalTo(true));
		Assert.assertThat(responseMessage, equalTo("Data Not Found / Invalid data"));
		Assert.assertThat(responseCode, equalTo("data.not.found"));
	}
	
	@Test
	public void MTC006b_getSaleswhenOrderIdIsNotEntered() throws IOException {		
		Map<String, String> params = new HashMap<String, String>();
		params.put("orderAcctId", "MYERS");
		params.put("orderAttrList", "header");
		params.put("page", "1");
		
		String resp = given()
			.log().all()
			.spec(RestUtilities.createQueryParam(reqSpec, params))
	    .when()
	    	// NO OrderID entered
	    	.get(Endpoints.SALES_ORDER).asString();
		
		Document document = Jsoup.parse(resp);
		String pageTitle = document.title().toUpperCase();
		
	}
}
