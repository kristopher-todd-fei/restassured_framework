package camsvc.carrier.rate.api;

import static io.restassured.RestAssured.given;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import camsvc.writers.common.ReusableMethods;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.xml.XmlPath;
import io.restassured.response.Response;
import payLoads.Carrier_XML;

public class GetCarrierAndRateFromTMS {
	
	private static Logger log = LogManager.getLogger(GetCarrierAndRateFromTMS.class.getName());
	
	@Before
	public void init() {
		RestAssured.baseURI = "http://f00387.sys.ds.wolseley.com:8080/carrier-rate";
		RestAssured.basePath = "/svc/v1";
	}
	
	@Test
	public void getProductSheet() {
		Response response = given()
			.headers("Content-Type", "application/xml")
			.headers("accept", "*/*")
			.body(Carrier_XML.carrierData())
		.when()
			.post("/rate/warehouse")
		.then()
			.assertThat()
			.statusCode(200)
			.and()
			.contentType(ContentType.XML)
			.extract()
			.response();
		
		XmlPath xmlPath = ReusableMethods.rawToXML(response);
		
		System.out.println("Pricesheet Total: " + xmlPath.getString("freightCostResponse.priceSheets.priceSheet.Total"));
		log.info("Pricesheet Total: " + xmlPath.getString("freightCostResponse.priceSheets.priceSheet.Total"));
		
	}
}
