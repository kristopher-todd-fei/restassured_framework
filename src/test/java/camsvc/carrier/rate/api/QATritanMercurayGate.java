package camsvc.carrier.rate.api;

import static io.restassured.RestAssured.given;

import org.junit.Before;
import org.junit.Test;

import io.restassured.RestAssured;
import io.restassured.authentication.FormAuthConfig;
import io.restassured.filter.session.SessionFilter;

public class QATritanMercurayGate {
	
	public static SessionFilter filter;
	
	@Before
	public void init() {
		filter = new SessionFilter();
		RestAssured.baseURI = "https://qa-tritan.mercurygate.net/MercuryGate/common/remoteServiceTest.jsp";
		
		given()
			.auth().form(
					"WSQA-FERGUSON", "mode2018*", new FormAuthConfig("/MercuryGate/common/remoteServiceTest.jsp", "userid", "password"))
			.filter(filter)
			.get();
		
		System.out.println("Session ID: " + filter.getSessionId() + "\n");
		
	}
	
	@Test
	public void test001() {
		given()
			.log().all()
			.sessionId(filter.getSessionId())
			.get()
			.then().log().all();
	}

}
