package camsvc.writers.common;

import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Projections.excludeId;
import static com.mongodb.client.model.Projections.fields;
import static com.mongodb.client.model.Projections.include;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Properties;

import org.apache.commons.codec.binary.Base64;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bson.Document;
import org.junit.Assert;

import com.mongodb.Block;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

import io.restassured.path.json.JsonPath;
import io.restassured.path.xml.XmlPath;
import io.restassured.response.Response;

public class ReusableMethods {

	private static Logger log = LogManager.getLogger(ReusableMethods.class.getName());
	static Properties prop = new Properties();

	public static XmlPath rawToXML(Response r) {
		String raw_resp = r.asString();
		XmlPath xml = new XmlPath(raw_resp);

		return xml;
	}

	public static JsonPath rawToJSON(Response r) {
		String raw_resp = r.asString();
		JsonPath json = new JsonPath(raw_resp);

		return json;
	}

	public static String dateTime() {
		LocalDateTime myDateObj = LocalDateTime.now();
		DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		String formattedDate = myDateObj.format(myFormatObj);

		return formattedDate;
	}

	public static String dateTime_2() {
		LocalDateTime myDateObj = LocalDateTime.now();
		DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		String formattedDate = myDateObj.format(myFormatObj);

		return formattedDate;
	}
	
	public static String isoLocalDateTime() {
		LocalDateTime myDateObj = LocalDateTime.now();
		DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");
		String formattedDate = myDateObj.format(myFormatObj);

		return formattedDate;
	}

	public static Long syncId() {
		Date d = new Date();
		Long sync_id = d.getTime();

		return sync_id;
	}

	public static String masterProductNo() {
		Date d = new Date();
		String masterProd = new Long(d.getTime()).toString();

		return masterProd;
	}

	public static String mongo_un() throws IOException {
		FileInputStream fis = new FileInputStream(
				System.getProperty("user.dir") + File.separator + "writer.properties");
		prop.load(fis);

		String u_name = prop.getProperty("MONGO_UN");
		byte[] decode = Base64.decodeBase64(u_name);

		return new String(decode);
	}

	public static String mongo_pwd() throws IOException {
		FileInputStream fis = new FileInputStream(
				System.getProperty("user.dir") + File.separator + "writer.properties");
		prop.load(fis);

		String pwd = prop.getProperty("MONGO_PWD");
		byte[] decode = Base64.decodeBase64(pwd);

		return new String(decode);
	}

	@SuppressWarnings("deprecation")
	public static void validateMongoDBData(final String coll, final String type, final String key, final Long value)
			throws IOException {
		// code to create the connection
		MongoClientURI uri = new MongoClientURI(
				"mongodb+srv://" + mongo_un() + ":" + mongo_pwd() + "@fei-dev-rti-001-wio8i.azure.mongodb.net");
		@SuppressWarnings("resource")
		MongoClient mongoClient = new MongoClient(uri);
		// Access Database
		MongoDatabase mongoDB = mongoClient.getDatabase("eventMessages");
		// Access Collection
		MongoCollection<Document> collection = mongoDB.getCollection(coll);

		switch (type) {
		case "ADD":
			break;
		case "UPDATE":
			break;
		case "DELETE":
			break;
		case "BULK":
			break;
		default:
			log.error("Message type is invalid. It will be ADD/UPDATE/BULK/DELETE");
		}

		Block<Document> printBlock = new Block<Document>() {
			public void apply(final Document document) {
				String jsonData = document.toJson();
				log.info(jsonData);

				String _id = JsonPath.with(jsonData).get("_id");
				log.info("_id: " + _id);
				String eventMeta_account = JsonPath.with(jsonData).get("eventMeta.account");
				String eventMeta_id = JsonPath.with(jsonData).get("eventMeta.id");
				String eventMeta_type = JsonPath.with(jsonData).get("eventMeta.type");				
				log.info("Type: " + eventMeta_type);
				String primaryKey = eventMeta_account + "*" + eventMeta_id;
				Long eventMeta_expireDate = JsonPath.with(jsonData).get("eventMeta.expireDate.$date");

				if (eventMeta_expireDate != null) {
					Date expDate = new Date(eventMeta_expireDate);

					String pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
					SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
					String date = simpleDateFormat.format(expDate);
					log.info("Expiration Date in MongoDB: " + date);
				} else {
					log.info("Expiration Date is null");
				}
				
				switch(coll) {
				case "customer":
					if (eventMeta_type.equalsIgnoreCase("ADD")) {
						Assert.assertEquals(_id, primaryKey + "*STOMER");
						Assert.assertEquals(null, eventMeta_expireDate);
						Assert.assertEquals(type, eventMeta_type);
					} else if (eventMeta_type.equalsIgnoreCase("UPDATE")) {
						Assert.assertEquals(_id, primaryKey + "*STOMER");
						Assert.assertEquals(null, eventMeta_expireDate);
						Assert.assertEquals(type, eventMeta_type);
					} else if (eventMeta_type.equalsIgnoreCase("BULK")) {
						Assert.assertEquals(_id, primaryKey + "*STOMER");
						Assert.assertEquals(null, eventMeta_expireDate);
						Assert.assertEquals(type, eventMeta_type);
					} else if (eventMeta_type.equalsIgnoreCase("DELETE")) {
						Assert.assertEquals(_id, primaryKey + "*STOMER");
						Assert.assertNotEquals(null, eventMeta_expireDate); 
						Assert.assertEquals(type, eventMeta_type);
					}
					break;
				case "quote":
					if (eventMeta_type.equalsIgnoreCase("ADD")) {
						Assert.assertEquals(_id, primaryKey + "*1");
						Assert.assertEquals(null, eventMeta_expireDate);
						Assert.assertEquals(type, eventMeta_type);
					} else if (eventMeta_type.equalsIgnoreCase("UPDATE")) {
						Assert.assertEquals(_id, primaryKey + "*1");
						Assert.assertEquals(null, eventMeta_expireDate);
						Assert.assertEquals(type, eventMeta_type);
					} else if (eventMeta_type.equalsIgnoreCase("BULK")) {
						Assert.assertEquals(_id, primaryKey + "*1");
						Assert.assertEquals(null, eventMeta_expireDate);
						Assert.assertEquals(type, eventMeta_type);
					} else if (eventMeta_type.equalsIgnoreCase("DELETE")) {
						Assert.assertEquals(_id, primaryKey + "*1");
						Assert.assertNotEquals(null, eventMeta_expireDate); 
						Assert.assertEquals(type, eventMeta_type);
					}
					break;
				default:
					if (eventMeta_type.equalsIgnoreCase("ADD")) {
						// Assert.assertEquals(_id, primaryKey);
						Assert.assertEquals(null, eventMeta_expireDate);
						Assert.assertEquals(type, eventMeta_type);
					} else if (eventMeta_type.equalsIgnoreCase("UPDATE")) {
						// Assert.assertEquals(_id, primaryKey);
						Assert.assertEquals(null, eventMeta_expireDate);
						Assert.assertEquals(type, eventMeta_type);
					} else if (eventMeta_type.equalsIgnoreCase("BULK")) {
						// Assert.assertEquals(_id, primaryKey);
						Assert.assertEquals(null, eventMeta_expireDate);
						Assert.assertEquals(type, eventMeta_type);
					} else if (eventMeta_type.equalsIgnoreCase("DELETE")) {
						// Assert.assertEquals(_id, primaryKey);
						Assert.assertNotEquals(null, eventMeta_expireDate); 
						Assert.assertEquals(type, eventMeta_type);
					}
				}
			}
		};

		collection.find(eq(key, value))
			.projection(fields(include(
					"eventMeta.account", 
					"eventMeta.id", 
					"eventMeta.type",
					"eventMeta.file", 
					"eventMeta.expireDate", 
					"eventMeta.syncId")))
			.forEach(printBlock);
	}
	
	@SuppressWarnings("deprecation")
	public static void validateMongoData_HashCode(String coll, String key, Long value) throws IOException {
		// code to create the connection
		MongoClientURI uri = new MongoClientURI(
				"mongodb+srv://"+mongo_un()+":"+mongo_pwd()+"@fei-dev-rti-001-wio8i.azure.mongodb.net");
		@SuppressWarnings("resource")
		MongoClient mongoClient = new MongoClient(uri);
		// code to connect to database
		MongoDatabase mongoDB = mongoClient.getDatabase("eventMessages");
		MongoCollection<Document> collection = mongoDB.getCollection(coll);
		Block<Document> printBlock = new Block<Document>() {
			public void apply(final Document document) {
				String jsonData = document.toJson();
				// System.out.println(jsonData);
				log.info(jsonData + "\n");
				
				String hashCode = JsonPath.with(jsonData).getString("hashCode");
				System.out.println(hashCode);
			}
		};
		
		if (coll.equalsIgnoreCase("product")) {
		collection.find(eq(key, value))
			.projection(fields(include(
					"eventMeta.type",
					"eventMeta.syncid"),
					excludeId()))
			.forEach(printBlock);
		} else if (coll.equalsIgnoreCase("hash")) {
			collection.find(eq(key, value))
			.projection(fields(include(
					"hashCode"),
					excludeId()))
			.forEach(printBlock);
		}
	}
}
