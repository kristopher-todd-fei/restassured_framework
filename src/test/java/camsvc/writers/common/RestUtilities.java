package camsvc.writers.common;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

import java.util.Map;

import camsvc.writers.constants.ExpectBody;
import camsvc.writers.constants.ExpectHeader;
import camsvc.writers.constants.Paths;
import camsvc.writers.constants.QueryParams;
import camsvc.writers.constants.StatusCodes;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.path.xml.XmlPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;

public class RestUtilities {
	public static String ENDPOINT;
	public static RequestSpecBuilder REQUEST_BUILDER;
	public static RequestSpecification REQUEST_SPEC;
	public static ResponseSpecBuilder RESPONSE_BUILDER;
	public static ResponseSpecification RESPONSE_SPEC;
	
	public static void setEndpoint(String endpoint) {
		ENDPOINT = endpoint;
	}
	
	public static RequestSpecification getRequestSpecification() {
		REQUEST_BUILDER = new RequestSpecBuilder();
		REQUEST_BUILDER.setBaseUri(Paths.BASE_URI);
		REQUEST_BUILDER.setBasePath(Paths.BASE_PATH);
		REQUEST_BUILDER.addQueryParam("key", QueryParams.QUERY_KEY);
		REQUEST_BUILDER.addQueryParam("name", QueryParams.QUERY_NAME);
		REQUEST_BUILDER.addQueryParam("value", QueryParams.QUERY_VALUE);
		REQUEST_BUILDER.addQueryParam("type", QueryParams.QUERY_TYPE);
		
		REQUEST_SPEC = REQUEST_BUILDER.build();
		
		return REQUEST_SPEC;
	}
	
	public static ResponseSpecification getResponseSpecification() {
		RESPONSE_BUILDER = new ResponseSpecBuilder();
		
		RESPONSE_BUILDER.expectStatusCode(StatusCodes.SUCCESS_STATUS_CODE);
		
		RESPONSE_BUILDER.expectHeader("Server", ExpectHeader.SERVER);
		RESPONSE_BUILDER.expectHeader("accept", ExpectHeader.ACCEPT);
		RESPONSE_BUILDER.expectHeader("accept-encoding", ExpectHeader.ACCEPT_ENCODING);
		RESPONSE_BUILDER.expectHeader("key", ExpectHeader.HEADER_KEY);
		RESPONSE_BUILDER.expectHeader("name", ExpectHeader.HEADER_NAME);
		RESPONSE_BUILDER.expectHeader("value", ExpectHeader.HEADER_VALUE);
		RESPONSE_BUILDER.expectHeader("Content-Type", ExpectHeader.HEADER_CONTENT_TYPE);
		RESPONSE_BUILDER.expectHeader("Transfer-Encoding", ExpectHeader.HEADER_TRANSFER_ENCODING);
		
		RESPONSE_BUILDER.expectBody("isError", equalTo(ExpectBody.BODY_IS_ERROR));
		RESPONSE_BUILDER.expectBody("respMsg", equalTo(ExpectBody.BODY_RESPONSE_MESSAGE));
		RESPONSE_BUILDER.expectBody("respCode", equalTo(ExpectBody.BODY_RESPONSE_CODE));
		
		RESPONSE_SPEC = RESPONSE_BUILDER.build();
		
		return RESPONSE_SPEC;
	}
	
	public static RequestSpecification getRequestSpecification_AddressValidation() {
		REQUEST_BUILDER = new RequestSpecBuilder();
		REQUEST_BUILDER.setBaseUri(Paths.BASE_URI_ADDRESS_VALIDATE);
		REQUEST_BUILDER.setBasePath(Paths.BASE_PATH);
				
		REQUEST_SPEC = REQUEST_BUILDER.build();
		
		return REQUEST_SPEC;
	}
	
	public static ResponseSpecification getResponseSpecification_AddressValidation() {
		RESPONSE_BUILDER = new ResponseSpecBuilder();
		
		RESPONSE_BUILDER.expectStatusCode(StatusCodes.SUCCESS_STATUS_CODE);
		
		RESPONSE_BUILDER.expectHeader("Server", ExpectHeader.SERVER);
		RESPONSE_BUILDER.expectHeader("Content-Type", ExpectHeader.HEADER_CONTENT_TYPE);
		RESPONSE_BUILDER.expectHeader("Transfer-Encoding", ExpectHeader.HEADER_TRANSFER_ENCODING);
		
		RESPONSE_SPEC = RESPONSE_BUILDER.build();
		
		return RESPONSE_SPEC;
	}
	
	public static RequestSpecification getRequestSpecification_Quotes() {
		REQUEST_BUILDER = new RequestSpecBuilder();
		REQUEST_BUILDER.setBaseUri(Paths.BASE_URI_QUOTE);
		REQUEST_BUILDER.setBasePath(Paths.BASE_PATH);
		REQUEST_BUILDER.addQueryParam("key", QueryParams.QUERY_KEY);
		REQUEST_BUILDER.addQueryParam("name", QueryParams.QUERY_NAME);
		REQUEST_BUILDER.addQueryParam("value", QueryParams.QUERY_VALUE);
		REQUEST_BUILDER.addQueryParam("type", QueryParams.QUERY_TYPE);
		
		REQUEST_SPEC = REQUEST_BUILDER.build();
		
		return REQUEST_SPEC;
	}
	
	public static RequestSpecification getRequestSpecification_SalesOrders() {
		REQUEST_BUILDER = new RequestSpecBuilder();
		REQUEST_BUILDER.setBaseUri(Paths.BASE_URI_SALES_ORDER);
		REQUEST_BUILDER.setBasePath(Paths.BASE_PATH);
				
		REQUEST_SPEC = REQUEST_BUILDER.build();
		
		return REQUEST_SPEC;
	}
	
	public static ResponseSpecification getResponseSpecification_SalesOrders() {
		RESPONSE_BUILDER = new ResponseSpecBuilder();
		
		RESPONSE_BUILDER.expectHeader("Server", ExpectHeader.SERVER);
		// RESPONSE_BUILDER.expectHeader("Content-Type", ExpectHeader.HEADER_CONTENT_TYPE);
		// RESPONSE_BUILDER.expectHeader("Transfer-Encoding", ExpectHeader.HEADER_TRANSFER_ENCODING);
		
		RESPONSE_SPEC = RESPONSE_BUILDER.build();
		
		return RESPONSE_SPEC;
	}
	
	public static RequestSpecification getRequestSpecification_ProductChannelIndicator() {
		REQUEST_BUILDER = new RequestSpecBuilder();
		REQUEST_BUILDER.setBaseUri(Paths.BASE_URI_PRODUCT_CHANNEL_INDICATOR);
		REQUEST_BUILDER.setBasePath(Paths.BASE_PATH);
		
		REQUEST_SPEC = REQUEST_BUILDER.build();
		
		return REQUEST_SPEC;
	}
	
	public static RequestSpecification getRequestSpecification_testSomething() {
		REQUEST_BUILDER = new RequestSpecBuilder();
		REQUEST_BUILDER.setBaseUri(Paths.BASE_URI_GOOGLE);
		REQUEST_BUILDER.setBasePath(Paths.BASE_PATH);
		
		REQUEST_SPEC = REQUEST_BUILDER.build();
		
		return REQUEST_SPEC;
	}
	
	public static ResponseSpecification getResponseSpecification_ProductChannelIndicator() {
		RESPONSE_BUILDER = new ResponseSpecBuilder();
		RESPONSE_BUILDER.expectHeader("Server", ExpectHeader.SERVER);
		
		RESPONSE_SPEC = RESPONSE_BUILDER.build();
		return RESPONSE_SPEC;
	}
	
	public static RequestSpecification getRequestSpecification_ProductPrice() {
		REQUEST_BUILDER = new RequestSpecBuilder();
		REQUEST_BUILDER.setBaseUri(Paths.BASE_URI_PRODUCT_PRICING);
		REQUEST_BUILDER.setBasePath(Paths.BASE_PATH);
		
		REQUEST_SPEC = REQUEST_BUILDER.build();
		
		return REQUEST_SPEC;
	}
	
	public static ResponseSpecification getResponseSpecification_ProductPrice() {
		RESPONSE_BUILDER = new ResponseSpecBuilder();
		RESPONSE_BUILDER.expectHeader("Server", ExpectHeader.SERVER);
		
		RESPONSE_SPEC = RESPONSE_BUILDER.build();
		return RESPONSE_SPEC;
	}
	
	public static RequestSpecification getRequestSpecification_ProductPPQRetriever() {
		REQUEST_BUILDER = new RequestSpecBuilder();
		REQUEST_BUILDER.setBaseUri(Paths.BASE_URI_PRODUCT_PPQ);
		REQUEST_BUILDER.setBasePath(Paths.BASE_PATH);
		
		REQUEST_SPEC = REQUEST_BUILDER.build();
		
		return REQUEST_SPEC;
	}
	
	public static ResponseSpecification getResponseSpecification_ProductPPQRetriever() {
		RESPONSE_BUILDER = new ResponseSpecBuilder();
		RESPONSE_BUILDER.expectHeader("Server", ExpectHeader.SERVER);
		RESPONSE_BUILDER.expectHeader("Content-Type", ExpectHeader.HEADER_CONTENT_TYPE);
		RESPONSE_BUILDER.expectHeader("Transfer-Encoding", ExpectHeader.HEADER_TRANSFER_ENCODING);
		
		RESPONSE_SPEC = RESPONSE_BUILDER.build();
		return RESPONSE_SPEC;
	}
	
	public static RequestSpecification createQueryParam(RequestSpecification rspec, String param, String value) {
		
		return rspec.queryParam(param, value);
	}
	
	public static RequestSpecification createQueryParam(RequestSpecification rspec, Map<String, String> queryMap) {
		return rspec.queryParams(queryMap);
	}
	
	public static RequestSpecification createPathParam(RequestSpecification rspec, String param, String value) {
		return rspec.pathParam(param, value);
	}
	
	public static RequestSpecification createPathParam(RequestSpecification rspec, Map<String, String> pathMap) {
		return rspec.pathParams(pathMap);
	}

	public static Response getResponse() {
		return given().get(ENDPOINT);
	}
	
	public static Response getResponse(RequestSpecification reqSpec, String type) {
		REQUEST_SPEC.spec(reqSpec);
		Response response = null;
		
		if(type.equalsIgnoreCase("get")) {
			response = given().spec(REQUEST_SPEC).get(ENDPOINT);
		} else if (type.equalsIgnoreCase("post")) {
			response = given().spec(REQUEST_SPEC).post(ENDPOINT);
		} else if (type.equalsIgnoreCase("put")) {
			response = given().spec(REQUEST_SPEC).put(ENDPOINT);
		} else if (type.equalsIgnoreCase("delete")) {
			response = given().spec(REQUEST_SPEC).delete(ENDPOINT);
		} else {
			System.out.println("Type is not supported");
		}
		
		response.then().log().headers();
		response.then().spec(RESPONSE_SPEC);
		
		return response;
	}

	public static JsonPath getJsonPath(Response res) {
		String path = res.asString();
		return new JsonPath(path);
	}
	
	public static XmlPath getXmlPath(Response res) {
		String path = res.asString();
		return new XmlPath(path);
	}
	
	public static void resetBasePath() {
		RestAssured.basePath = null;
	}
	
	public static void setContentType(ContentType type) {
		given().contentType(type);
	}
}
