package camsvc.writers.e2e.tests;

import static io.restassured.RestAssured.given;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;

import camsvc.writers.common.RestUtilities;
import camsvc.writers.common.ReusableMethods;
import camsvc.writers.constants.Endpoints;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import payLoads.Branch;

@RunWith(DataProviderRunner.class)
public class E2E_Branch {
	
	private static Logger log = LogManager.getLogger(E2E_Branch.class.getName());
	
	Long syncId;
	RequestSpecification reqSpec;
	ResponseSpecification resSpec;
	
	@Before
	public void init() throws IOException, Exception {

		syncId = ReusableMethods.syncId();	
		
		// REQUEST Specification
		reqSpec = RestUtilities.getRequestSpecification();
		
		// RESPONSE Specifications
		resSpec = RestUtilities.getResponseSpecification();
		resSpec.header("amq-destination-name", "rti.branch");
	}
	
	@Test
	@UseDataProvider("data")
	public void Branch(
			String collection,
			String eventMeta_type,
			String eventMeta_key) throws IOException, InterruptedException {
		
		log.info(eventMeta_type + " Branch");
		
		given()
			.spec(reqSpec)
			.body(Branch.branchData(eventMeta_type, ReusableMethods.dateTime(), syncId))
		.when()
			.post(Endpoints.MESSAGE_PUBLISHER)
		.then()
			.spec(resSpec);
		
		ReusableMethods.validateMongoDBData(
				collection, eventMeta_type, eventMeta_key, syncId);
	}
	
	@After
	public void afterTest() throws InterruptedException, IOException {
		Thread.sleep(1000);
	}
	
	@DataProvider
	public static Object[][] data() {
		return new Object[][] {
			{"branch", "ADD", "eventMeta.syncId"},
			{"branch", "UPDATE", "eventMeta.syncId"},
			{"branch", "DELETE", "eventMeta.syncId"}
		};
	}
}
