package camsvc.writers.e2e.tests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses(
		{ 
			E2E_Branch.class,
			E2E_Customer.class,
			E2E_Employee.class,
			E2E_Inventory.class,
			E2E_Quotes.class 
		}
	)
public class E2E_Tests {

}
