package camsvc.writers.e2e.tests;

import static io.restassured.RestAssured.given;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;

import camsvc.writers.common.RestUtilities;
import camsvc.writers.common.ReusableMethods;
import camsvc.writers.constants.Endpoints;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import payLoads.Inventory;

@RunWith(DataProviderRunner.class)
public class E2E_Inventory {
	
	private static Logger log = LogManager.getLogger(E2E_Inventory.class.getName());
	
	Long syncId;
	RequestSpecification reqSpec;
	ResponseSpecification resSpec;
	
	@Before
	public void init() throws IOException, Exception {
		
		syncId = ReusableMethods.syncId();	
		
		// REQUEST Specification
		reqSpec = RestUtilities.getRequestSpecification();
		
		// RESPONSE Specifications
		resSpec = RestUtilities.getResponseSpecification();
		resSpec.header("amq-destination-name", "rti.inventory");
	}
	
	@Test
	@UseDataProvider("data")
	public void inventory(
			String collection, String eventMeta_type, String eventMeta_key) throws IOException {
		
		log.info(eventMeta_type + " Inventory");
		
		given()
			.spec(reqSpec)
			.body(Inventory.inventoryData(
					eventMeta_type, ReusableMethods.dateTime(), syncId))
		.when()
			.post(Endpoints.MESSAGE_PUBLISHER)
		.then()
			.spec(resSpec);
		
		ReusableMethods.validateMongoDBData(
				collection, eventMeta_type, eventMeta_key, syncId);
	}
	
	@After
	public void tearDown() throws IOException, InterruptedException {
		Thread.sleep(1000);
	}
	
	@DataProvider
	public static Object[][] data() {
		return new Object[][] {
			{"inventory", "ADD", "eventMeta.syncId"},
			{"inventory", "UPDATE", "eventMeta.syncId"},
			{"inventory", "DELETE", "eventMeta.syncId"}
		};
	}

}
