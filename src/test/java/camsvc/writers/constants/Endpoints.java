package camsvc.writers.constants;

public class Endpoints {
	public static final String MESSAGE_PUBLISHER = "/message-publisher";
	public static final String SALES_ORDER = "/sales-order";
	public static final String ZIP_TO_CITY = "/ziptocitytransformer";
	public static final String LIST = "/list";
}
