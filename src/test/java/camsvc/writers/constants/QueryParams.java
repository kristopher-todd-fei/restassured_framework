package camsvc.writers.constants;

public class QueryParams {
	public static final String QUERY_KEY = "Content-Type";
	public static final String QUERY_NAME = "Content-Type";
	public static final String QUERY_VALUE = "application/json";
	public static final String QUERY_TYPE = "text";
}
