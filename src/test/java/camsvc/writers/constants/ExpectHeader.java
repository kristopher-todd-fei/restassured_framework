package camsvc.writers.constants;

public class ExpectHeader {
	public static final String SERVER = "Apache-Coyote/1.1";
	public static final String ACCEPT = "*/*";
	public static final String ACCEPT_ENCODING = "gzip,deflate";
	public static final String HEADER_KEY = "Content-Type";
	public static final String HEADER_NAME = "Content-Type";
	public static final String HEADER_TYPE = "text";
	public static final String HEADER_VALUE = "application/json";
	public static final String HEADER_CONTENT_TYPE = "application/json";
	public static final String HEADER_TRANSFER_ENCODING = "chunked";
}
