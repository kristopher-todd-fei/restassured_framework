package camsvc.writers.constants;

public class ExpectBody {
	public static final Boolean BODY_IS_ERROR = false;
	public static final String BODY_RESPONSE_MESSAGE = "The document was successfully published";
	public static final String BODY_RESPONSE_CODE = "document.published";
}
