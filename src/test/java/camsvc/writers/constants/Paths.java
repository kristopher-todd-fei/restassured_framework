package camsvc.writers.constants;

public class Paths {
	public static final String BASE_URI = "http://fusedev/event-message-publisher";
	public static final String BASE_URI_QUOTE = "http://fusedev/order-event-message-publisher";
	public static final String BASE_URI_SALES_ORDER = "http://f00387.sys.ds.wolseley.com:8080/sales-order-gl";
	public static final String BASE_URI_ADDRESS_VALIDATE = "http://fusedev.sys.ds.wolseley.com/address-validate";
	public static final String BASE_URI_PRODUCT_CHANNEL_INDICATOR = "http://fusedev.sys.ds.wolseley.com/product-channel-indicator";
	public static final String BASE_URI_PRODUCT_PRICING = "http://fusedev.sys.ds.wolseley.com/product-pricing";
	public static final String BASE_URI_PRODUCT_PPQ = "http://fusedev/product-ppq";
	public static final String BASE_URI_GOOGLE = "www.google.com";
	
	public static final String BASE_PATH = "/svc/v1";
}
