package camsvc.writers.tests;

import static io.restassured.RestAssured.given;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import camsvc.writers.common.RestUtilities;
import camsvc.writers.common.ReusableMethods;
import camsvc.writers.constants.Endpoints;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import payLoads.Branch;

public class Branch_DELETE {
	
	private static Logger log = LogManager.getLogger(Branch_DELETE.class.getName());
	
	Long syncId;
	RequestSpecification reqSpec;
	ResponseSpecification resSpec;
	
	@Before
	public void init() throws IOException, Exception {
		log.info("********** BEGIN TEST **********");
		
		syncId = ReusableMethods.syncId();	
		// REQUEST SPECIFICATIONS
		reqSpec = RestUtilities.getRequestSpecification();
		reqSpec.body(Branch.branchData("DELETE", ReusableMethods.dateTime(), syncId));
				
		// RESPONSE SPECIFICATIONS
		resSpec = RestUtilities.getResponseSpecification();
		resSpec.header("amq-destination-name", "rti.branch");
	}
	
	@Test
	public void deleteBranch() throws IOException {
		log.info("Sync ID used for this test: " + syncId);
		Response resp = 
				given()
					.spec(reqSpec)
				.when()
					.post(Endpoints.MESSAGE_PUBLISHER)
				.then()
					.spec(resSpec)
					.extract()
					.response();
		
		JsonPath jsp = ReusableMethods.rawToJSON(resp);
		String expDate = jsp.getString("eventMeta.expireDate.$date");
		
		System.out.println(expDate);
		
		if(expDate == null) {
			log.info("Add Branch test FAILED.");
		} else {
			log.error("Add Branch test PASSED.");
		}
	}
	
	@After
	public void tearDown() throws IOException {
		/**
		 * Validate MongoDB Data needed
		 * 1.) MongoDB Collection (e.g. quote)
		 * 2.) VALUE for the eventMeta.type KEY (e.g. ADD)
		 * 3.) KEY   --> eventMeta.syncId
		 * 4.) VALUE --> syncId
		 */
		ReusableMethods.validateMongoDBData(
				"branch",
				"DELETE",
				"eventMeta.syncId",
				syncId);
		
		log.info("********** END TEST **********\n");
	}
}
