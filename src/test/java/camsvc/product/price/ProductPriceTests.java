package camsvc.product.price;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;

import org.junit.Assert;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import camsvc.writers.common.RestUtilities;
import camsvc.writers.constants.Endpoints;
import io.restassured.path.json.JsonPath;
import io.restassured.path.xml.XmlPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import payLoads.product_price.GetProductPrice;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ProductPriceTests {
	
	RequestSpecification reqSpec;
	ResponseSpecification respSpec;
	
	@Before
	public void setup() {
		reqSpec = RestUtilities.getRequestSpecification_ProductPrice();
		respSpec = RestUtilities.getResponseSpecification_ProductPrice();
	}
	
	@Test
	public void MTC001_verifyingResponseAfterSubmittingRequestWithValidFields() {
		Response resp = 
		given()
			.spec(reqSpec)
			.body(GetProductPrice.validFields())
		.when()
			.post(Endpoints.LIST)
		.then()
			// .log().all()
			.spec(respSpec)
			.statusCode(200)
			.extract().response();
		
		XmlPath xmlPath = RestUtilities.getXmlPath(resp);
		String isError = xmlPath.get("productPriceResponse.isError");
		// String productId = xmlPath.get("productPriceResponse.product.productId");
		String unitListPrice = xmlPath.get("productPriceResponse.product.unitListPrice");
		String unitNetPrice = xmlPath.get("productPriceResponse.product.unitNetPrice");
		String priceCol = xmlPath.get("productPriceResponse.product.priceColumn");
		
		Assert.assertThat(isError, equalTo("false"));
		// Assert.assertThat(productId, equalTo("88526"));
		Assert.assertThat(unitListPrice, equalTo("78.00"));
		Assert.assertThat(unitNetPrice, equalTo("29.700"));
		Assert.assertThat(priceCol, equalTo("520"));
	}
	
	@Test
	public void MTC002a_verifyingResponseAfterSubmittingRequest_invalidCustomerId() {
		Response resp = 
		given()
			.spec(reqSpec)
			.body(GetProductPrice.invalidCustomerId())
		.when()
			.post(Endpoints.LIST)
		.then()
			// .log().all()
			.spec(respSpec)
			.statusCode(400)
			.extract().response();
		
		JsonPath jsPath = RestUtilities.getJsonPath(resp);
		boolean isError = jsPath.get("isError");
		String responseMsg = jsPath.get("respMsg");
		String responseCode = jsPath.get("respCode");
		
		Assert.assertThat(isError, equalTo(true));
		Assert.assertThat(responseMsg, equalTo("Invalid customer. Trilogie response - DP02 UNABLE TO READ CUSTOMER 454354354354 IN DIST."));
		Assert.assertThat(responseCode, equalTo("product.pricing.bad.request"));
	}
	
	@Test
	public void MTC002b_verifyingResponseAfterSubmittingRequest_nullCustomerId() {
		Response resp = 
		given()
			.spec(reqSpec)
			.body(GetProductPrice.emptyCustomerId())
		.when()
			.post(Endpoints.LIST)
		.then()
			// .log().all()
			.spec(respSpec)
			.statusCode(400)
			.extract().response();
		
		JsonPath jsPath = RestUtilities.getJsonPath(resp);
		String responseCode = jsPath.get("respCode");
		String responseMsg = jsPath.get("respMsg");
		boolean isError = jsPath.get("isError");
		String errorCode = jsPath.get("validationError[0].errorCode");
		String errorMsg = jsPath.get("validationError[0].errorMsg");
		String field = jsPath.get("validationError[0].field");
		
		Assert.assertThat(responseCode, equalTo("product.pricing.bad.request"));
		Assert.assertThat(responseMsg, equalTo("Bad request."));
		Assert.assertThat(isError, equalTo(true));
		Assert.assertThat(errorCode, equalTo("product.pricing.not.empty"));
		Assert.assertThat(errorMsg, equalTo("field cannot be empty or null."));
		Assert.assertThat(field, equalTo("customerId"));		
	}
	
	@Test
	public void MTC003a_verifyingResponseAfterSubmittingRequest_invalidBranchId() {
		Response resp = 
		given()
			.spec(reqSpec)
			.body(GetProductPrice.invalidBranchId())
		.when()
			.post(Endpoints.LIST)
		.then()
			// .log().all()
			.spec(respSpec)
			.statusCode(400)
			.extract().response();
		
		JsonPath jsPath = RestUtilities.getJsonPath(resp);
		boolean isError = jsPath.get("isError");
		String responseMsg = jsPath.get("respMsg");
		String responseCode = jsPath.get("respCode");
		
		Assert.assertThat(isError, equalTo(true));
		Assert.assertThat(responseMsg, containsString("Invalid branch id "));
		Assert.assertThat(responseCode, equalTo("product.pricing.invalid.account"));
	}
	
	@Test
	public void MTC003b_verifyingResponseAfterSubmittingRequest_emptyBranchId() {
		Response resp = 
		given()
			.spec(reqSpec)
			.body(GetProductPrice.emptyBranchId())
		.when()
			.post(Endpoints.LIST)
		.then()
			// .log().all()
			.spec(respSpec)
			.statusCode(400)
			.extract().response();
		
		JsonPath jsPath = RestUtilities.getJsonPath(resp);
		String responseCode = jsPath.get("respCode");
		String responseMsg = jsPath.get("respMsg");
		boolean isError = jsPath.get("isError");
		String errorCode = jsPath.get("validationError[0].errorCode");
		String errorMsg = jsPath.get("validationError[0].errorMsg");
		String field = jsPath.get("validationError[0].field");
		
		Assert.assertThat(responseCode, equalTo("product.pricing.bad.request"));
		Assert.assertThat(responseMsg, equalTo("Bad request."));
		Assert.assertThat(isError, equalTo(true));
		Assert.assertThat(errorCode, equalTo("product.pricing.not.empty"));
		Assert.assertThat(errorMsg, equalTo("field cannot be empty or null."));
		Assert.assertThat(field, equalTo("branchId"));		
	}
	
	@Test
	public void MTC004a_verifyingResponseAfterSubmittingRequest_invalidProduct() {
		Response resp = 
		given()
			.spec(reqSpec)
			.body(GetProductPrice.invalidProduct())
		.when()
			.post(Endpoints.LIST)
		.then()
			// .log().all()
			.spec(respSpec)
			.statusCode(200)
			.extract().response();
		
		XmlPath xmlPath = RestUtilities.getXmlPath(resp);
		String isError = xmlPath.get("productPriceResponse.isError");
		String pricingError = xmlPath.get("productPriceResponse.product.pricingError");
		
		Assert.assertThat(isError, equalTo("false"));
		Assert.assertThat(pricingError, equalTo("DP02 UNABLE TO READ GROUP"));
	}
	
	@Test
	public void MTC004b_verifyingResponseAfterSubmittingRequest_nullProduct() {
		Response resp = 
		given()
			.spec(reqSpec)
			.body(GetProductPrice.emptyProduct())
		.when()
			.post(Endpoints.LIST)
		.then()
			// .log().all()
			.spec(respSpec)
			.statusCode(400)
			.extract().response();
		
//		List<String> jsonPath = resp.jsonPath().getList("validationError");
//		System.out.println(jsonPath);
		
		JsonPath jsPath = RestUtilities.getJsonPath(resp);
		String responseCode = jsPath.get("respCode");
		String responseMsg = jsPath.get("respMsg");
		boolean isError = jsPath.get("isError");
		
		Assert.assertThat(responseCode, equalTo("product.pricing.bad.request"));
		Assert.assertThat(responseMsg, equalTo("Bad request."));
		Assert.assertThat(isError, equalTo(true));
		
		// TODO Use HashMap to assert product.quantity and product.productId can be found in array list.
		
//		String errorCode = jsPath.get("validationError[0].errorCode");
//		String errorMsg = jsPath.get("validationError[0].errorMsg");
//		String field = jsPath.get("validationError[0].field");
//		
//		Assert.assertThat(errorCode, equalTo("product.pricing.not.null"));
//		Assert.assertThat(errorMsg, equalTo("field cannot be null."));
//		Assert.assertThat(field, equalTo("product.quantity"));
//		
//		String errorCode2 = jsPath.get("validationError[1].errorCode");
//		String errorMsg2 = jsPath.get("validationError[1].errorMsg");
//		String field2 = jsPath.get("validationError[1].field");
//		
//		Assert.assertThat(errorCode2, equalTo("product.pricing.not.null"));
//		Assert.assertThat(errorMsg2, equalTo("field cannot be null."));
//		Assert.assertThat(field2, equalTo("product.productId"));
	}
	
	@Test
	public void MTC005a_verifyingResponseAfterSubmittingRequest_invalidProductId() {
		Response resp = 
		given()
			.spec(reqSpec)
			.body(GetProductPrice.invalidProductId())
		.when()
			.post(Endpoints.LIST)
		.then()
			// .log().all()
			.spec(respSpec)
			.statusCode(200)
			.extract().response();
		
		XmlPath xmlPath = RestUtilities.getXmlPath(resp);
		String isError = xmlPath.get("productPriceResponse.isError");
		String pricingError = xmlPath.get("productPriceResponse.product.pricingError");
		Assert.assertThat(isError, equalTo("false"));
		Assert.assertThat(pricingError, equalTo("DP02 UNABLE TO READ GROUP"));
	}
	
	@Test
	public void MTC005b_verifyingResponseAfterSubmittingRequest_nullProductId() {
		Response resp = 
		given()
			.spec(reqSpec)
			.body(GetProductPrice.emptyProductId())
		.when()
			.post(Endpoints.LIST)
		.then()
			// .log().all()
			.spec(respSpec)
			.statusCode(400)
			.extract().response();
		
		JsonPath jsPath = RestUtilities.getJsonPath(resp);
		String responseCode = jsPath.get("respCode");
		String responseMsg = jsPath.get("respMsg");
		boolean isError = jsPath.get("isError");
		
		Assert.assertThat(responseCode, equalTo("product.pricing.bad.request"));
		Assert.assertThat(responseMsg, equalTo("Bad request."));
		Assert.assertThat(isError, equalTo(true));
		
		String errorCode = jsPath.get("validationError[0].errorCode");
		String errorMsg = jsPath.get("validationError[0].errorMsg");
		String field = jsPath.get("validationError[0].field");
		
		Assert.assertThat(errorCode, equalTo("product.pricing.not.null"));
		Assert.assertThat(errorMsg, equalTo("field cannot be null."));
		Assert.assertThat(field, equalTo("product.productId"));
	}
	
	@Test
	public void MTC005c_verifyingResponseAfterSubmittingRequest_invalidQuantity() {
		Response resp = 
		given()
			.spec(reqSpec)
			.body(GetProductPrice.invalidQuantity())
		.when()
			.post(Endpoints.LIST)
		.then()
			// .log().all()
			.spec(respSpec)
			.statusCode(500)
			.extract().response();
		
		JsonPath jsPath = RestUtilities.getJsonPath(resp);
		boolean isError = jsPath.get("isError");
		String responseMsg = jsPath.get("respMsg");
		String responseCode = jsPath.get("respCode");
		Assert.assertThat(isError, equalTo(true));
		Assert.assertThat(responseMsg, containsString("Can not construct instance of java.lang.Integer from String value "));
		Assert.assertThat(responseCode, equalTo("product.pricing.invalid.response"));
	}
	
	@Test
	public void MTC005d_verifyingResponseAfterSubmittingRequest_nullQuantity() {
		Response resp = 
		given()
			.spec(reqSpec)
			.body(GetProductPrice.emptyQuantity())
		.when()
			.post(Endpoints.LIST)
		.then()
			// .log().all()
			.spec(respSpec)
			.statusCode(400)
			.extract().response();
		
		JsonPath jsPath = RestUtilities.getJsonPath(resp);
		String responseCode = jsPath.get("respCode");
		String responseMsg = jsPath.get("respMsg");
		boolean isError = jsPath.get("isError");
		
		Assert.assertThat(responseCode, equalTo("product.pricing.bad.request"));
		Assert.assertThat(responseMsg, equalTo("Bad request."));
		Assert.assertThat(isError, equalTo(true));
		
		String errorCode = jsPath.get("validationError[0].errorCode");
		String errorMsg = jsPath.get("validationError[0].errorMsg");
		String field = jsPath.get("validationError[0].field");
		
		Assert.assertThat(errorCode, equalTo("product.pricing.not.null"));
		Assert.assertThat(errorMsg, equalTo("field cannot be null."));
		Assert.assertThat(field, equalTo("product.quantity"));
	}
	
	@Test
	@Ignore
	public void MTC006_verifyingTheResponseAfterSubmittingMaxProductRequest() {
		// TODO
	}
}
