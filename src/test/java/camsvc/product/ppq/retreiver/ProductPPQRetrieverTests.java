package camsvc.product.ppq.retreiver;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.Assert;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import camsvc.writers.common.RestUtilities;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ProductPPQRetrieverTests {
	
	RequestSpecification reqSpec;
	ResponseSpecification resSpec;
	
	@Before
	public void setup() {
		reqSpec = RestUtilities.getRequestSpecification_ProductPPQRetriever();
		resSpec = RestUtilities.getResponseSpecification_ProductPPQRetriever();
	}

	@Test
	public void MTC001a_verifyingResponse_validFields_SingleProdDetails() {
		Response resp = given()
			.spec(reqSpec)
		.when()
			.get("/DIST/3383670/338")
		.then()
			// .log().all()
			.statusCode(200)
			.spec(resSpec)
			.extract().response();
		
		JsonPath jsPath = RestUtilities.getJsonPath(resp);
		
		boolean isError = jsPath.get("isError");
		String prodId = jsPath.get("productId");
		float weight = jsPath.get("productPPQInfo[0].weight");
		String uom = jsPath.get("productPPQInfo[0].uom");
		String ppq = jsPath.get("productPPQInfo[0].ppq");
		
		Assert.assertThat(isError, equalTo(false));
		Assert.assertThat(prodId, equalTo("3383670"));
		Assert.assertThat(weight, equalTo(0.000F));
		Assert.assertThat(uom, equalTo("INNER PACK"));
		Assert.assertThat(ppq, equalTo("1"));
	}
	
	@Test
	public void MTC001b_verifyingResponse_validFields_MultipleProdDetails() {
		Response resp = given()
				.spec(reqSpec)
			.when()
				.get("/DIST/100275/338")
			.then()
				// .log().all()
				.statusCode(200)
				.spec(resSpec)
				.extract().response();
			
			JsonPath jsPath = RestUtilities.getJsonPath(resp);
			boolean isError = jsPath.get("isError");
			String prodId = jsPath.get("productId");
			
			Assert.assertThat(isError, equalTo(false));
			Assert.assertThat(prodId, equalTo("100275"));
			
			String ppq = jsPath.get("productPPQInfo[0].ppq");
			String uom = jsPath.get("productPPQInfo[0].uom");
			float weight = jsPath.get("productPPQInfo[0].weight");		
		
			Assert.assertThat(ppq, equalTo("1"));
			Assert.assertThat(uom, equalTo("EACH"));
			Assert.assertThat(weight, equalTo(0.310F));
			
			String ppq2 = jsPath.get("productPPQInfo[1].ppq");
			String uom2 = jsPath.get("productPPQInfo[1].uom");
			float weight2 = jsPath.get("productPPQInfo[1].weight");		
		
			Assert.assertThat(ppq2, equalTo("1"));
			Assert.assertThat(uom2, equalTo("INNER PACK"));
			Assert.assertThat(weight2, equalTo(0.000F));
	}
	
	@Test
	public void MTC002a_verifyingResponse_validFields_invalidProductId() {
		Response resp = given()
				.spec(reqSpec)
			.when()
				.get("/DIST/NaN/338")
			.then()
				// .log().all()
				.statusCode(500)
				.spec(resSpec)
				.extract().response();
			
			JsonPath jsPath = RestUtilities.getJsonPath(resp);
			boolean isError = jsPath.get("isError");
			String responseMessage = jsPath.get("respMsg");
			String responseCode = jsPath.get("respCode");
			
			Assert.assertThat(isError, equalTo(true));
			Assert.assertThat(responseMessage, equalTo("Invalid product ppq response is converted from trilogie   : Trilogie Exception: ."));
			Assert.assertThat(responseCode, equalTo("product.ppq.invalid.response"));
	}
	
	@Test
	public void MTC002b_verifyingResponse_validFields_emptyProductId() {
		String resp = given().when().get("http://fusedev/product-ppq/svc/v1/DIST/388").asString();
		Document doc = Jsoup.parse(resp);
		String pageTitle = doc.title().toUpperCase();
		
		Assert.assertThat(pageTitle, equalTo("JBWEB000065: HTTP STATUS 404 -"));
	}
	
	@Test
	public void MTC003a_verifyingResponse_validFields_invalidBranchId() {
		Response resp = given()
				.spec(reqSpec)
			.when()
				.get("/DIST2/3383670/338")
			.then()
				// .log().all()
				.statusCode(400)
				.spec(resSpec)
				.extract().response();
			
			JsonPath jsPath = RestUtilities.getJsonPath(resp);
			boolean isError = jsPath.get("isError");
			String responseMessage = jsPath.get("respMsg");
			String responseCode = jsPath.get("respCode");
			
			Assert.assertThat(isError, equalTo(true));
			Assert.assertThat(responseMessage, equalTo("product.ppq.invalid.account"));
			Assert.assertThat(responseCode, equalTo("product.ppq.invalid.account"));
	}
	
	@Test
	public void MTC003b_verifyingResponse_validFields_emptyBranchId() {
		String resp = given().when().get("http://fusedev/product-ppq/svc/v1/3383670/338").asString();
		Document doc = Jsoup.parse(resp);
		String pageTitle = doc.title().toUpperCase();
		
		Assert.assertThat(pageTitle, equalTo("JBWEB000065: HTTP STATUS 404 -"));
	}
	
	@Test
	public void MTC004a_verifyingResponse_validFields_invalidWhseId() {
		Response resp = given()
				.spec(reqSpec)
			.when()
				.get("/DIST/3383670/123")
			.then()
				// .log().all()
				.statusCode(404)
				.spec(resSpec)
				.extract().response();
			
			JsonPath jsPath = RestUtilities.getJsonPath(resp);
			boolean isError = jsPath.get("isError");
			String responseMessage = jsPath.get("respMsg");
			String responseCode = jsPath.get("respCode");
			
			Assert.assertThat(isError, equalTo(true));
			Assert.assertThat(responseMessage, equalTo("Warehouse Not Found"));
			Assert.assertThat(responseCode, equalTo("product.ppq.not.found"));
	}
	
	@Test
	public void MTC004b_verifyingResponse_validFields_emptyWhseId() {
		String resp = given().when().get("http://fusedev/product-ppq/svc/v1/3383670").asString();
		Document doc = Jsoup.parse(resp);
		String pageTitle = doc.title().toUpperCase();
		
		Assert.assertThat(pageTitle, equalTo("JBWEB000065: HTTP STATUS 404 -"));
	}
	
	@Test
	public void MTC005a_verifyingResponse_validFields_invalidProductIdBranchIdWhseId() {
		Response resp = given()
				.spec(reqSpec)
			.when()
				.get("/DIST2/NaN/123")
			.then()
				// .log().all()
				.statusCode(400)
				.spec(resSpec)
				.extract().response();
			
			JsonPath jsPath = RestUtilities.getJsonPath(resp);
			boolean isError = jsPath.get("isError");
			String responseMessage = jsPath.get("respMsg");
			String responseCode = jsPath.get("respCode");
			
			Assert.assertThat(isError, equalTo(true));
			Assert.assertThat(responseMessage, equalTo("product.ppq.invalid.account"));
			Assert.assertThat(responseCode, equalTo("product.ppq.invalid.account"));
	}
	
	@Test
	public void MTC005b_verifyingResponse_validFields_emptyProductIdBranchIdWhseId() {
		String resp = given().when().get("http://fusedev/product-ppq/svc/v1").asString();
		Document doc = Jsoup.parse(resp);
		String pageTitle = doc.title().toUpperCase();
		
		Assert.assertThat(pageTitle, equalTo("JBWEB000065: HTTP STATUS 404 -"));
	}
}
