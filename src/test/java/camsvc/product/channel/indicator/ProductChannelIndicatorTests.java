package camsvc.product.channel.indicator;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;

import camsvc.writers.common.RestUtilities;
import camsvc.writers.common.ReusableMethods;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import payLoads.product_channel_indicator.ItemId;
import payLoads.product_channel_indicator.MessageWithMultipleItemId;
import payLoads.product_channel_indicator.MultipleItemIdsWithValidAndInvalidChannelIndicators;
import payLoads.product_channel_indicator.SizeOfChannelIndicator;
import payLoads.product_channel_indicator.ValidMsgHeader;
import payLoads.product_channel_indicator.ValidateChannelIndicator;

@RunWith(DataProviderRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ProductChannelIndicatorTests {
	
	public static final Logger log = LogManager.getLogger(ProductChannelIndicatorTests.class.getName());
	
	RequestSpecification reqSpec;
	ResponseSpecification respSpec;
	
	@Before
	public void setup() {
		reqSpec = RestUtilities.getRequestSpecification_ProductChannelIndicator();
		respSpec = RestUtilities.getResponseSpecification_ProductChannelIndicator();
	}
	
	@Test
	@UseDataProvider("timeZone")
	public void MTC001a_validMsgIndicator_Payload1(String key, String value) {
		Response resp = 
		given()
			.spec(reqSpec)
			.header("DateTime", ReusableMethods.isoLocalDateTime())
			.header(key, value)
			.body(ValidMsgHeader.payLoad_One())
		.when()
			.post()
		.then()
			.spec(respSpec)
			.statusCode(200)
			.extract().response();
		
		JsonPath jsPath = RestUtilities.getJsonPath(resp);
		boolean isError = jsPath.get("isError");
		String responseMessage = jsPath.get("respMsg");
		String responseCode = jsPath.get("respCode");
		
		Assert.assertThat(isError, equalTo(false));
		Assert.assertThat(responseMessage, equalTo("Message successfully published"));
		Assert.assertThat(responseCode, equalTo("message.sent.success"));
		
		log.info("TEST --> MTC001a Valid Message Indicator (" + value + "): ");
		log.info(resp.asString() + "\n");
	}
	
	@Test
	@UseDataProvider("timeZone")
	public void MTC001b_validMsgIndicator_Payload2(String key, String value) {
		Response resp = 
		given()
			.spec(reqSpec)
			.header("DateTime", ReusableMethods.isoLocalDateTime())
			.header(key, value)
			.body(ValidMsgHeader.payLoad_Two())
		.when()
			.post()
		.then()
			.spec(respSpec)
			.extract().response();
		
		JsonPath jsPath = RestUtilities.getJsonPath(resp);
		boolean isError = jsPath.get("isError");
		String responseMessage = jsPath.get("respMsg");
		String responseCode = jsPath.get("respCode");
		
		Assert.assertThat(isError, equalTo(false));
		Assert.assertThat(responseMessage, equalTo("Message successfully published"));
		Assert.assertThat(responseCode, equalTo("message.sent.success"));
		
		log.info("TEST --> MTC001b Valid Message Indicator (" + value + "): ");
		log.info(resp.asString() + "\n");
	}
	
	@Test
	@UseDataProvider("timeZone")
	public void MTC001c_validMsgIndicator_Payload3(String key, String value) {
		Response resp = 
		given()
			.spec(reqSpec)
			.header("DateTime", ReusableMethods.isoLocalDateTime())
			.header(key, value)
			.body(ValidMsgHeader.payload_Three())
		.when()
			.post()
		.then()
			.spec(respSpec)
			.statusCode(200)
			.extract().response();
		
		JsonPath jsPath = RestUtilities.getJsonPath(resp);
		boolean isError = jsPath.get("isError");
		String responseMessage = jsPath.get("respMsg");
		String responseCode = jsPath.get("respCode");
		
		Assert.assertThat(isError, equalTo(false));
		Assert.assertThat(responseMessage, equalTo("Message successfully published"));
		Assert.assertThat(responseCode, equalTo("message.sent.success"));
		
		log.info("TEST --> MTC001c Valid Message Indicator (" + value + "): ");
		log.info(resp.asString() + "\n");
	}
	
	@Test
	@UseDataProvider("timeZone")
	public void MTC002a_oneItemId(String key, String value) {
		Response resp = 
		given()
			.spec(reqSpec)
			.header("DateTime", ReusableMethods.isoLocalDateTime())
			.header(key, value)
			.body(ItemId.oneItemId())
		.when()
			.post()
		.then()
			.spec(respSpec)
			.statusCode(200)
			.extract().response();
		
		JsonPath jsPath = RestUtilities.getJsonPath(resp);
		boolean isError = jsPath.get("isError");
		String responseMessage = jsPath.get("respMsg");
		String responseCode = jsPath.get("respCode");
		
		Assert.assertThat(isError, equalTo(false));
		Assert.assertThat(responseMessage, equalTo("Message successfully published"));
		Assert.assertThat(responseCode, equalTo("message.sent.success"));
		
		log.info("TEST --> MTC002a One Item ID (" + value + "): ");
		log.info(resp.asString() + "\n");
	}
	
	@Test
	@UseDataProvider("timeZone")
	public void MTC002b_noItemIdsInPayload(String key, String value) {
		Response resp = 
		given()
			.spec(reqSpec)
			.header("DateTime", ReusableMethods.isoLocalDateTime())
			.header(key, value)
			.body(ItemId.emptyItemIdPayload())
		.when()
			.post()
		.then()
			.spec(respSpec)
			.statusCode(400)
			.extract().response();
		
		JsonPath jsPath = RestUtilities.getJsonPath(resp);
		boolean isError = jsPath.get("isError");
		String responseMessage = jsPath.get("respMsg");
		String responseCode = jsPath.get("respCode");
		
		Assert.assertThat(isError, equalTo(true));
		Assert.assertThat(responseMessage, equalTo("{null=->null/empty Extended}"));
		Assert.assertThat(responseCode, equalTo("invalid.items"));
		
		log.info("TEST --> MTC002b No Items in Payload (" + value + "): ");
		log.info(resp.asString() + "\n");
	}
	
	@Test
	@UseDataProvider("timeZone")
	public void MTC002c_fiftyItemIdsInPayload(String key, String value) {
		Response resp = 
		given()
			.spec(reqSpec)
			.header("DateTime", ReusableMethods.isoLocalDateTime())
			.header(key, value)
			.body(ItemId.fiftyItemIds())
		.when()
			.post()
		.then()
			.spec(respSpec)
			.statusCode(200)
			.extract().response();
		
		JsonPath jsPath = RestUtilities.getJsonPath(resp);
		boolean isError = jsPath.get("isError");
		String responseMessage = jsPath.get("respMsg");
		String responseCode = jsPath.get("respCode");
		
		Assert.assertThat(isError, equalTo(false));
		Assert.assertThat(responseMessage, equalTo("Message successfully published"));
		Assert.assertThat(responseCode, equalTo("message.sent.success"));
		
		log.info("TEST --> MTC002c Fifty Items in Payload (" + value + "): ");
		log.info(resp.asString() + "\n");
	}
	
	@Test
	@UseDataProvider("timeZone")
	public void MTC002d_greaterThanFiftyItemIdsInPayload(String key, String value) {
		Response resp = 
		given()
			.spec(reqSpec)
			.header("DateTime", ReusableMethods.isoLocalDateTime())
			.header(key, value)
			.body(ItemId.greaterThanFiftyItemIds())
		.when()
			.post()
		.then()
			.spec(respSpec)
			.statusCode(400)
			.extract().response();
		
		JsonPath jsPath = RestUtilities.getJsonPath(resp);
		boolean isError = jsPath.get("isError");
		String responseMessage = jsPath.get("respMsg");
		String responseCode = jsPath.get("respCode");
		
		Assert.assertThat(isError, equalTo(true));
		Assert.assertThat(responseMessage, equalTo("[The request payload must contain between 1 and 50 ItemIDs]"));
		Assert.assertThat(responseCode, equalTo("invalid.request"));
		
		log.info("TEST --> MTC002d Greater than Fifty Items in Payload (" + value + "): ");
		log.info(resp.asString() + "\n");
	}
	
	@Test
	@UseDataProvider("timeZone")
	public void MTC004a_sizeOfChannelIndicator_lessThan100(String key, String value) {
		Response resp =
		given()
			.spec(reqSpec)
			.header("DateTime", ReusableMethods.isoLocalDateTime())
			.header(key, value)
			.body(SizeOfChannelIndicator.lessThan100())
		.when()
			.post()
		.then()
			.spec(respSpec)
			.statusCode(200)
			.extract().response();
		
		JsonPath jsPath = RestUtilities.getJsonPath(resp);
		boolean isError = jsPath.get("isError");
		String responseMessage = jsPath.get("respMsg");
		String responseCode = jsPath.get("respCode");
		
		Assert.assertThat(isError, equalTo(false));
		Assert.assertThat(responseMessage, equalTo("Message successfully published"));
		Assert.assertThat(responseCode, equalTo("message.sent.success"));
	}
	
	@Test
	@UseDataProvider("timeZone")
	public void MTC004b_sizeOfChannelIndicator_greaterThan100(String key, String value) {
		Response resp =
		given()
			.spec(reqSpec)
			.header("DateTime", ReusableMethods.isoLocalDateTime())
			.header(key, value)
			.body(SizeOfChannelIndicator.greaterThan100())
		.when()
			.post()
		.then()
			.spec(respSpec)
			.statusCode(400)
			.extract().response();
		
		JsonPath jsPath = RestUtilities.getJsonPath(resp);
		boolean isError = jsPath.get("isError");
		String responseMessage = jsPath.get("respMsg");
		String responseCode = jsPath.get("respCode");
		
		Assert.assertThat(isError, equalTo(true));
		Assert.assertThat(responseMessage, containsString("IncludedIn length > 100"));
		Assert.assertThat(responseCode, equalTo("invalid.items"));
	}
	
	@Test
	@UseDataProvider("timeZone")
	public void MTC006a_validateChannelIndicator(String key, String value) {
		Response resp =
		given()
			.spec(reqSpec)
			.header("DateTime", ReusableMethods.isoLocalDateTime())
			.header(key, value)
			.body(ValidateChannelIndicator.validateChannel())
		.when()
			.post()
		.then()
			.spec(respSpec)
			.statusCode(400)
			.extract().response();
		
		JsonPath jsPath = RestUtilities.getJsonPath(resp);
		boolean isError = jsPath.get("isError");
		String responseMessage = jsPath.get("respMsg");
		String responseCode = jsPath.get("respCode");
		
		Assert.assertThat(isError, equalTo(true));
		Assert.assertThat(responseMessage, containsString("Invalid includedIn ATCs"));
		Assert.assertThat(responseMessage, containsString("ExcludedPrevIncl not a subset of includedIn"));
		Assert.assertThat(responseCode, equalTo("invalid.items"));
	}
	
	@Test
	@UseDataProvider("timeZone")
	public void MTC007a_validateChannelIndicator(String key, String value) {
		Response resp =
		given()
			.spec(reqSpec)
			.header("DateTime", ReusableMethods.isoLocalDateTime())
			.header(key, value)
			.body(MultipleItemIdsWithValidAndInvalidChannelIndicators.multipleItemIds())
		.when()
			.post()
		.then()
			.spec(respSpec)
			.statusCode(206)
			.extract().response();
		
		JsonPath jsPath = RestUtilities.getJsonPath(resp);
		boolean isError = jsPath.get("isError");
		String responseMessage = jsPath.get("respMsg");
		String responseCode = jsPath.get("respCode");
		
		Assert.assertThat(isError, equalTo(false));
		Assert.assertThat(responseMessage, containsString("ExcludedPrevIncl not a subset of includedIn"));
		Assert.assertThat(responseMessage, containsString("Invalid excludedPrevIncl ATCs"));
		Assert.assertThat(responseCode, equalTo("invalid.items"));
	}
	
	@Test
	@UseDataProvider("timeZone")
	public void MTC011a_messageWithMultipleItemIds(String key, String value) {
		Response resp =
		given()
			.spec(reqSpec)
			.header("DateTime", ReusableMethods.isoLocalDateTime())
			.header(key, value)
			.body(MessageWithMultipleItemId.messageWMultipleItemIds())
		.when()
			.post()
		.then()
			.spec(respSpec)
			.statusCode(200)
			.extract().response();
		
		JsonPath jsPath = RestUtilities.getJsonPath(resp);
		boolean isError = jsPath.get("isError");
		String responseMessage = jsPath.get("respMsg");
		String responseCode = jsPath.get("respCode");
		
		Assert.assertThat(isError, equalTo(false));
		Assert.assertThat(responseMessage, equalTo("Message successfully published"));
		Assert.assertThat(responseCode, equalTo("message.sent.success"));
	}
	
	@DataProvider
	public static Object[][] timeZone() {
		return new Object[][] {
			{"TimeZone", "EST"},
			{"TimeZone", "CST"},
			{"TimeZone", "MST"},
			{"TimeZone", "PST"},
			{"TimeZone", "AST"},
			{"TimeZone", "HST"}
		};
	}

}
