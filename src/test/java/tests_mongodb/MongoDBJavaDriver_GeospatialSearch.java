package tests_mongodb;

import org.bson.Document;

import com.mongodb.Block;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Indexes;
import com.mongodb.client.model.geojson.Point;
import com.mongodb.client.model.geojson.Position;

public class MongoDBJavaDriver_GeospatialSearch {
	
	// static String uriString = "mongodb+srv://automation_svc:T_tww3GSUZND94!@fei-dev-rti-001-wio8i.azure.mongodb.net";

	@SuppressWarnings("deprecation")
	public static void main(String[] args) {
		
		Block<Document> printBlock = new Block<Document>() {
			public void apply(final Document document) {
				System.out.println(document.toJson());
			}
		};
		
		// Connect to MongoDB Deployment
		@SuppressWarnings("resource")
		MongoClient mongoClient = new MongoClient();
		MongoDatabase database = mongoClient.getDatabase("test");
		
		// Create the 2dsphere Index
		MongoCollection<Document> collection = database.getCollection("restaurants");
		collection.createIndex(Indexes.geo2dsphere("contact.location"));
		
		// Query for Locations Near a GEOJSON Point
		Point refPoint = new Point(new Position(-73.9667, 40.78));
		collection.find(Filters.near("contact.location", refPoint, 5000.0, 1000.0)).forEach(printBlock);
	}
}
