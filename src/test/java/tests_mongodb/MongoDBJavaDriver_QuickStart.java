package tests_mongodb;

import static com.mongodb.client.model.Filters.eq;

import org.bson.Document;

import com.mongodb.Block;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class MongoDBJavaDriver_QuickStart {
	
	// static String uriString = "mongodb+srv://automation_svc:T_tww3GSUZND94!@fei-dev-rti-001-wio8i.azure.mongodb.net";

	@SuppressWarnings("deprecation")
	public static void main(String[] args) {
			/** Connect to MongoDB instance **/
			MongoClientURI connectionString = new MongoClientURI(
					"mongodb+srv://automation_svc:T_tww3GSUZND94!@fei-dev-rti-001-wio8i.azure.mongodb.net");
			MongoClient mongoClient = new MongoClient(connectionString);
			
			/** Access the DB **/
			MongoDatabase dataBase = mongoClient.getDatabase("eventMessages");
			
			/** Access a collection **/
			MongoCollection<Document> collection = dataBase.getCollection("hash");
			// System.out.println(collection.count());
			
			
			/** Find the first document in the collection **/
			Document myDoc = collection.find().first();
			// System.out.println(myDoc.toJson());
			
			/** Find ALL documents in collection **/
//			MongoCursor<Document> cursor = collection.find().iterator();
//			try {
//				while(cursor.hasNext()) {
//					System.out.println(cursor.next().toJson());
//				}
//			} finally {
//				cursor.close();
//			}
			
			/** get a single document that matches a filter **/
			myDoc = collection.find(eq("recordId", "1571844041172")).first();
			// System.out.println(myDoc.toJson());
			
			/** get ALL documents that match a filter **/
			Block<Document> printBlock = new Block<Document>() {
				public void apply(final Document document) {
					System.out.println(document.toJson());
				}
			};
			
			collection.find(eq("recordId", "1571844041172")).forEach(printBlock);
			
			/** Update a single document **/
			// collection.updateOne(eq("_id", "1571844041172"), new Document("$set", new Document("masterProduct.productDesc2", "UPDATE BY KJT")));
	}
}
