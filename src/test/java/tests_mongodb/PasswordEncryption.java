package tests_mongodb;

import org.apache.commons.codec.binary.Base64;

public class PasswordEncryption {

	public static void main(String[] args) {
		String str = "VF90d3czR1NVWk5EOTQh";
		
		byte[] encode = Base64.encodeBase64(str.getBytes());
		
		System.out.println("String before encoding: " + str);
		System.out.println("String after encoding: " + new String(encode));
		
		byte[] decode = Base64.decodeBase64(encode);
		System.out.println("String after decoding: " + new String(decode));

	}

}
