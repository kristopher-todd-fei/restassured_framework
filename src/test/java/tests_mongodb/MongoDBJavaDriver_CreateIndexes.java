package tests_mongodb;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.MongoCollection;
import org.bson.Document;

import com.mongodb.client.model.Indexes;
import com.mongodb.client.model.IndexOptions;
import com.mongodb.client.model.Filters;

public class MongoDBJavaDriver_CreateIndexes {

	public static void main(String[] args) {
		/**
		 * Indexes support the efficient execution of queries in MongoDB. 
		 * To create an index on a field or fields, pass an index specification document to the MongoCollection.createIndex() method.
		 */
		// Connect to a MongoDB Deployment
		@SuppressWarnings("resource")
		MongoClient mongoClient = new MongoClient();
		MongoDatabase database = mongoClient.getDatabase("test");
		MongoCollection<Document> collection = database.getCollection("restaurants");
		
		// ASCENDING INDEX
		// Single Ascending Index
		System.out.println("Ascending Index");
		System.out.println(collection.createIndex(Indexes.ascending("name")));
		
		// Compound Ascending Index
		System.out.println("\nCompound Ascending Index");
		System.out.println(collection.createIndex(Indexes.ascending("stars", "name")));
		
		//DESCENDING INDEX
		// Single Descending Index
		System.out.println("\nDescending Index");
		System.out.println(collection.createIndex(Indexes.descending("stars")));
		
		// Compound Descending Index
		System.out.println("\nCompound Descending Index");
		System.out.println(collection.createIndex(Indexes.descending("stars", "name")));
		
		// Compound Indexes
		System.out.println("\nCompound Indexes");
		System.out.println(collection.createIndex(Indexes.compoundIndex(
				Indexes.descending("stars"),
				Indexes.ascending("name"))));
		
		// Text Indexes
		System.out.println("\nText Index");
		System.out.println(collection.createIndex(Indexes.text("name")));
		
		// Hashed Index
		System.out.println("\nHashed Index");
		System.out.println(collection.createIndex(Indexes.hashed("_id")));
		
		// GEOSPATIAL INDEXES
		// 2dspere
		System.out.println("\nGEOSPATIAL INDEX: 2dsphere");
		System.out.println(collection.createIndex(Indexes.geo2dsphere("contact.location")));
		
		// 2d
		System.out.println("\nGEOSPATIAL INDEX: 2d");
		System.out.println(collection.createIndex(Indexes.geo2d("contact.location")));
		
		// geoHaystack
		System.out.println("\nGEOSPATIAL INDEX: geoHaystack");
		IndexOptions hayStackOption = new IndexOptions().bucketSize(1.0);
		System.out.println(collection.createIndex(
				Indexes.geoHaystack("contact.location", Indexes.ascending("stars")),
				hayStackOption));
		
		// INDEX OPTIONS
		// Unique Index
		System.out.println("\nINDEX OPTIONS: Unique Index");
		IndexOptions indexOptions = new IndexOptions().unique(true);
		System.out.println(collection.createIndex(Indexes.ascending("name", "stars"), indexOptions));
		
		// Partial Index
		System.out.println("\nINDEX OPTIONS: Partial Index");
		IndexOptions partialFilterIndexOptions = new IndexOptions()
				.partialFilterExpression(Filters.exists("contact.email"));
		System.out.println(collection.createIndex(
				Indexes.descending("name", "stars"), partialFilterIndexOptions));
		
		// Get a list of Indexes on a Collection
		System.out.println("\nList of Indexes: ");
		for(Document index : collection.listIndexes()) {
			System.out.println(index.toJson());
		}
	}
}
