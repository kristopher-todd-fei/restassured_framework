package tests_mongodb;

import org.bson.Document;

import com.mongodb.Block;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Indexes;
import com.mongodb.client.model.Projections;
import com.mongodb.client.model.Sorts;
import com.mongodb.client.model.TextSearchOptions;

public class MongoDBJavaDriver_TextSearch {
	
	@SuppressWarnings("deprecation")
	public static void main(String[] args) {
	
		Block<Document> printBlock = new Block<Document>() {
			
			public void apply(final Document document) {
				System.out.println(document.toJson());
			}
		};
		
		@SuppressWarnings("resource")
		MongoClient mongoClient = new MongoClient();
		MongoDatabase database = mongoClient.getDatabase("test");
		
		// Create the text Index
		System.out.println("CREATE THE TEXT INDEX: To create a text index, use the Indexes.text static helper to create a specification for a text index and pass to MongoCollection.createIndex() method.");
		MongoCollection<Document> collection = database.getCollection("restaurants");
		collection.createIndex(Indexes.text("name"));
		
		// Perform Text Search
		System.out.println("\nPERFORM TEXT SEARCH: To perform text search, use the Filters.text() helper to specify the text search query filter.");
		long matchCount = collection.count(Filters.text("bakery coffee"));
		System.out.println("Text search matches: " + matchCount);
		
		// Text Score
		System.out.println("\nTEXT SCORE: For each matching document, text search assigns a score, representing the relevance of a document to the specified text search query filter. "
				+ "To return and sort by score, use the $meta operator in the projection document and the sort expression.");
		collection.find(Filters.text("bakery cafe"))
			.projection(Projections.metaTextScore("score"))
			.sort(Sorts.metaTextScore("score"))
			.forEach(printBlock);
		
		// Specify a Text Search Option
		System.out.println("\nSPECIFY A TEXT SEARCH OPTION: The Filters.text() helper can accept various text search options. The Java driver provides TextSearchOptions class to specify these options.");
		long matchCountEnglish = collection.count(Filters.text("cafe", new TextSearchOptions().language("english")));
		System.out.println("Text search matches (english): " + matchCountEnglish);
	}
}
