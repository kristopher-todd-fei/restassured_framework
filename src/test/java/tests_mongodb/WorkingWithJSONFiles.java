package tests_mongodb;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.jayway.jsonpath.JsonPath;

public class WorkingWithJSONFiles {

	static String json = "{\r\n" + 
			"   \"eventMeta\":{\r\n" + 
			"      \"type\":\"DELETE\",\r\n" + 
			"      \"syncId\":{\r\n" + 
			"         \"$numberLong\":\"1572291781819\"\r\n" + 
			"      },\r\n" + 
			"      \"expireDate\":{\r\n" + 
			"         \"$date\":1572291781575\r\n" + 
			"      }\r\n" + 
			"   }\r\n" + 
			"}";
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
	    Long expireDate = JsonPath.read(json, "$.eventMeta.expireDate.$date");
	    System.out.println(expireDate);
	    
//	    Integer posts = JsonPath.read(json, "$.posts.length()");
//	    
//	    for(int i = 0; i < posts; i++) {
//	    	String post_id = JsonPath.read(json, "$.posts["+i+"].post_id");
//	    	System.out.println(post_id);
//	    }
	    
	    Date d2 = new Date(expireDate);
	    // System.out.println(d2);
	    
	    String pattern = "yyyy-MM-dd'T'HH:mm:ss.sssZ";
	    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
	    String date = simpleDateFormat.format(d2);
	    System.out.println(date);

	}

}
