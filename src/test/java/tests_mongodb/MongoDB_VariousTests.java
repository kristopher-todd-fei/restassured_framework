package tests_mongodb;

import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Projections.excludeId;
import static com.mongodb.client.model.Projections.fields;
import static com.mongodb.client.model.Projections.include;

import org.bson.Document;

import com.mongodb.Block;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class MongoDB_VariousTests {	
	final static String uriString = "mongodb+srv://automation_svc:T_tww3GSUZND94!@fei-dev-rti-001-wio8i.azure.mongodb.net";
	
	@SuppressWarnings("deprecation")
	public static void main(String[] args) {
	
		// code to create the connection
		MongoClientURI uri = new MongoClientURI(uriString);
		MongoClient mongoClient = new MongoClient(uri);
		// code to connect to database
		System.out.println(mongoClient);
		
		MongoDatabase mongoDB = mongoClient.getDatabase("eventMessages");
		MongoCollection<Document> collection = mongoDB.getCollection("hash");
		
		Block<Document> printBlock = new Block<Document>() {
			public void apply(final Document document) {
				System.out.println(document.toJson());
			}
		};
		
		collection.find(eq("recordId", "1572032737852"))
			.projection(fields(include("hashCode"), excludeId()))
			.forEach(printBlock);
	}
			
		
		
		

}
