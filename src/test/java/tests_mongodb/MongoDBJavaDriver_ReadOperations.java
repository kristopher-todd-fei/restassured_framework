package tests_mongodb;

import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Filters.gte;
import static com.mongodb.client.model.Filters.lt;
import static com.mongodb.client.model.Projections.excludeId;
import static com.mongodb.client.model.Projections.fields;
import static com.mongodb.client.model.Projections.include;

import org.bson.Document;

import com.mongodb.Block;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Sorts;

public class MongoDBJavaDriver_ReadOperations {

	@SuppressWarnings("deprecation")
	public static void main(String[] args) {
		/**
		 * Find operations retrieve documents from a collection. You can specify a filter to select only those documents that match the filter condition.
		 */
		
		Block<Document> printBlock = new Block<Document>() {
		   
		       public void apply(final Document document) {
		           System.out.println(document.toJson());
		       }
		};
		
		@SuppressWarnings("resource")
		MongoClient mongoClient = new MongoClient();
		MongoDatabase database = mongoClient.getDatabase("test");
		MongoCollection<Document> collection = database.getCollection("restaurants");
		
		// Query a collection
		// call the method without any arguments to query all documents in a collection:
		collection.find().forEach(printBlock);
		System.out.println("");
		
		// pass a filter to query for documents that match the filter criteria:
		collection.find(eq("name", "456 Cookies Shop"))
			.forEach(printBlock);
		System.out.println("");
		
		// QUERY FILTERS
		// Empty Filter
		collection.find(new Document()).forEach(printBlock);
		System.out.println("");
		
		// Filters Helper
		collection.find(
				new Document("stars", new Document("$gte", 2)
						.append("$lt", 5))
						.append("categories", "Bakery"))
		.forEach(printBlock);
		System.out.println("");
		
		collection.find(and(
				gte("stars", 2), 
				lt("stars", 5), 
				eq("categories", "Bakery")))
		.forEach(printBlock);
		
		// FindIterable
		/**
		 * The find() method returns an instance of the FindIterable interface. 
		 * The interface provides various methods that you can chain to the find() method to modify the output or behavior of the query, 
		 * such as sort() or projection(), as well as for iterating the results, such as iterator() and forEach().
		 */
		
		// Projections
		/**
		 * By default, queries in MongoDB return all fields in matching documents.
		 * To specify the fields to return in the matching documents, you can specify a projection document.
		 */
		System.out.println("\nThe following find operation which includes a projection Document which specifies that the matching "
				+ "documents return only the name field, stars field, and the categories field.");
		collection.find(and(
				gte("stars", 2), 
				lt("stars", 5), 
				eq("categories", "Bakery")))
		.projection(new Document("name", 1)
				.append("stars", 1)
				.append("categories", 1)
				.append("_id", 0))
		.forEach(printBlock);
		
		System.out.println("\nTo facilitate the creation of projection documents, the Java driver provides the Projections class.");
		
		collection.find(and(gte("stars", 2), lt("stars", 5), eq("categories", "Bakery")))
			.projection(fields(
					include(
							"name", 
							"stars", 
							"categories"), 
					excludeId()))
			.forEach(printBlock);
		
		System.out.println("\nSORT: To sort documents, pass a sort specification document to the FindIterable.sort() method."
				+ " The Java driver provides Sorts helpers to facilitate the sort specification document.");
		collection.find(and(
				gte("stars", 2), 
				lt("stars", 5), 
				eq("categories", "Bakery")))
		.sort(Sorts.ascending("name"))
		.forEach(printBlock);
		
		System.out.println("\nSORT WITH PROJECTIONS: The FindIterable methods themselves return FindIterable objects, and as such, "
				+ "you can append multiple FindIterable methods to the find() method.");
		collection.find(and(
				gte("stars", 2), 
				lt("stars", 5), 
				eq("categories", "Bakery")))
		.sort(Sorts.ascending("name"))
		.projection(fields(
				include(
						"name", 
						"stars", 
						"categories"), 
				excludeId()))
		.forEach(printBlock);
	}
}
