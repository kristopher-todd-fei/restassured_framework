package tests_mongodb;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

public class MongoDBJavaDriver_RunCommands {

	public static void main(String[] args) {
		
		// Connect to MongoDB Deployment
		@SuppressWarnings("resource")
		MongoClient mongoClient = new MongoClient();
		MongoDatabase database = mongoClient.getDatabase("test");
		
		// Run buildInfo and collStats Commands
		Document buildInfoResults = database.runCommand(new Document("buildInfo", 1));
		System.out.println(buildInfoResults.toJson() + "\n");
		
		Document collStatsResults = database.runCommand(new Document("collstats", "restaurants"));
		System.out.println(collStatsResults);
	}
}
