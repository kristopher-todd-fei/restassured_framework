package tests_mongodb;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.ValidationOptions;

public class MongoDBJavaDriver_DataBasesAndCollections {

	public static void main(String[] args) {
			// Connect to a MongoDB Deployment
			System.out.println("Connect to a running MongoDB deployment.");
			MongoClient mongoClient = new MongoClient();
			
			// Access Database
			System.out.println("\nSpecify the name of the database to the getDatabase() method. If a database does not exist, MongoDB creates the database when you first store data for that database.");
			MongoDatabase database = mongoClient.getDatabase("test");
			
			// Access collection
			System.out.println("\nOnce you have a MongoDatabase instance, use its getCollection() method to access a collection. "
					+ "Specify the name of the collection to the getCollection() method.");
			MongoCollection<Document> coll = database.getCollection("myTestCollection");
			
			// Explicitly Create a Collection
			System.out.println("\nThe MongoDB driver provides the createCollection() method to explicitly create a collection. When you explicitly create a collection, you can"
					+ " specify various collection options, such as a maximum size or the documentation validation rules, with the CreateCollectionOptions class. "
					+ "If you are not specifying these options, you do not need to explicitly create the collection since MongoDB creates new collections when you first store data for the collections.");
			// database.createCollection("cappedCollection", new CreateCollectionOptions().capped(true).sizeInBytes(0x100000));
			
			// Document Validation
			System.out.println("\nMongoDB provides the capability to validate documents during updates and insertions. "
					+ "Validation rules are specified on a per-collection basis using the ValidationOptions, which takes a filter document that specifies the validation rules or expressions.");
			ValidationOptions collOptions = new ValidationOptions().validator(Filters.or(
					Filters.exists("email"), 
					Filters.exists("phone")));
			// database.createCollection("contacts", new CreateCollectionOptions().validationOptions(collOptions));
			
			// Get a List of Collections
			System.out.println("\nYou can get a list of the collections in a database using the MongoDatabase.listCollectionNames() method:");
			for (String name : database.listCollectionNames()) {
				System.out.println(name);
			}
			
			// Drop a Collection
			System.out.println("\nYou can drop a collection by using the MongoCollection.drop() method:");
			MongoCollection<Document> collection = database.getCollection("contacts");
			collection.drop();
			
			System.out.println("\nYou can get a list of the collections in a database using the MongoDatabase.listCollectionNames() method:");
			for (String name : database.listCollectionNames()) {
				System.out.println(name);
			}
	}
}
