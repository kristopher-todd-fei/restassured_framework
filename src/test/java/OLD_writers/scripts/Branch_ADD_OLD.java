package OLD_writers.scripts;

import static io.restassured.RestAssured.given;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Test;

import camsvc.writers.common.ReusableMethods;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import payLoads.Branch;
import resources.Resources;

public class Branch_ADD_OLD {
	
	private static Logger log = LogManager.getLogger(Branch_ADD_OLD.class.getName());	
	static Properties prop = new Properties();
	
	@BeforeClass
	public static void init() throws IOException, Exception {
		FileInputStream fis = new FileInputStream(System.getProperty("user.dir") + "\\writer.properties");
		prop.load(fis);
		
		RestAssured.baseURI = prop.getProperty("HOST");
	}
	
	@Test
	public void AddBranch() throws IOException {		
		Response add_branch = given()
				.queryParam("key", prop.getProperty("KEY"))
				.queryParam("name", prop.getProperty("NAME"))
				.queryParam("value", prop.getProperty("VALUE"))
				.queryParam("type", prop.getProperty("TYPE"))
				.body(Branch.branchData(
					"ADD",
					ReusableMethods.dateTime(),
					ReusableMethods.syncId()))
				.when()
				.post(Resources.resource_one())
				.then()
				.assertThat()
				.statusCode(200)
				.and()
				.contentType(ContentType.JSON)
				.extract()
				.response();
		
		JsonPath jsp = ReusableMethods.rawToJSON(add_branch);
		log.info("UpdateBranch => isError : " + jsp.getString("isError"));
		log.info("UpdateBranch => respMsg : " + jsp.getString("respMsg"));
		log.info("UpdateBranch => respCode: " + jsp.getString("respCode"));
		log.info("UpdateBranch => transId : " + jsp.getString("transId"));
	}
}
