package OLD_writers.scripts;

import static io.restassured.RestAssured.given;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import camsvc.writers.common.RestUtilities;
import camsvc.writers.common.ReusableMethods;
import camsvc.writers.constants.Paths;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import payLoads.Branch;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class BranchWriterTests {
	private static Logger log = LogManager.getLogger(BranchWriterTests.class.getName());
	
	Long syncId;
	RequestSpecification reqSpec;
	ResponseSpecification resSpec;
	
	@Before
	public void setup() {
		syncId = ReusableMethods.syncId();
		
		reqSpec = RestUtilities.getRequestSpecification();
		reqSpec.basePath(Paths.BASE_PATH);
		
		resSpec = RestUtilities.getResponseSpecification();
		resSpec.header("amq-destination-name", "rti.branch");
	}
	
	@Test
	public void MTC009_branch_invalid_branch_type() throws IOException {
		log.info("Sync ID used for this test: " + syncId);
		given()
			.spec(reqSpec)
			.body(Branch.branchData("TEST", ReusableMethods.dateTime(), syncId))
		.when()
			.post()
		.then()
			.spec(resSpec);
		
		ReusableMethods.validateMongoDBData("branch", "TEST", "eventMeta.syncId", syncId);
	}
	
	@Test
	public void MTC011_branch_EMP_ADD() throws IOException {
		log.info("Sync ID used for this test: " + syncId);
		given()
			.spec(reqSpec)
			.body(Branch.branchData("ADD", ReusableMethods.dateTime(), syncId))
		.when()
			.post()
		.then()
			.spec(resSpec);
		
		ReusableMethods.validateMongoDBData("branch", "ADD", "eventMeta.syncId", syncId);
	}
	
	@Test
	public void MTC012_branch_EMP_UPDATE() throws IOException {
		log.info("Sync ID used for this test: " + syncId);
		given()
			.spec(reqSpec)
			.body(Branch.branchData("UPDATE", ReusableMethods.dateTime(), syncId))
		.when()
			.post()
		.then()
			.spec(resSpec);
		
		ReusableMethods.validateMongoDBData("branch", "UPDATE", "eventMeta.syncId", syncId);
	}
	
	@Test
	public void MTC013_branch_EMP_BULK() throws IOException {
		log.info("Sync ID used for this test: " + syncId);
		given()
			.spec(reqSpec)
			.body(Branch.branchData("BULK", ReusableMethods.dateTime(), syncId))
		.when()
			.post()
		.then()
			.spec(resSpec);
		
		ReusableMethods.validateMongoDBData("branch", "BULK", "eventMeta.syncId", syncId);
	}
	
	@Test
	public void MTC014_branch_EMP_DELETE() throws IOException {
		log.info("Sync ID used for this test: " + syncId);
		given()
			.spec(reqSpec)
			.body(Branch.branchData("DELETE", ReusableMethods.dateTime(), syncId))
		.when()
			.post()
		.then()
			.spec(resSpec);
		
		ReusableMethods.validateMongoDBData("branch", "DELETE", "eventMeta.syncId", syncId);
	}
	
	@After
	public void teardown() throws IOException {
		log.info("Test Complete\n");
	}
}
