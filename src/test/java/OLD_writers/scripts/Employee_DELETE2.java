package OLD_writers.scripts;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import camsvc.writers.common.ReusableMethods;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import payLoads.Employee;
import resources.Resources;

public class Employee_DELETE2 {
	
	private static Logger log = LogManager.getLogger(Employee_DELETE2.class.getName());
	
	Properties prop = new Properties();
	Long syncId;
	RequestSpecBuilder requestBuilder;
	RequestSpecification requestSpec;
	ResponseSpecBuilder responseBuilder;
	ResponseSpecification responseSpec;
	
	@Before
	public void init() throws IOException, Exception {
		FileInputStream fis = new FileInputStream(System.getProperty("user.dir") + File.separator + "writer.properties");
		prop.load(fis);
		RestAssured.baseURI = prop.getProperty("HOST");
		syncId = ReusableMethods.syncId();
		
		/** Build the REQUEST Specifications **/
		requestBuilder = new RequestSpecBuilder();
		
		requestBuilder.addQueryParam("key", prop.getProperty("KEY"));
		requestBuilder.addQueryParam("name", prop.getProperty("NAME"));
		requestBuilder.addQueryParam("value", prop.getProperty("VALUE"));
		requestBuilder.addQueryParam("type", prop.getProperty("TYPE"));
		requestBuilder.setBody(Employee.employeeData(
				"DELETE",
				ReusableMethods.dateTime(),
				syncId));
		requestSpec = requestBuilder.build();
		
		/** Build the RESPONSE specifications **/
		responseBuilder = new ResponseSpecBuilder();
		
		responseBuilder.expectStatusCode(200);
		responseBuilder.expectHeader("Server", "Apache-Coyote/1.1");
		responseBuilder.expectHeader("accept", "*/*");
		responseBuilder.expectHeader("accept-encoding", "gzip,deflate");
		responseBuilder.expectHeader("amq-destination-name", "rti.employee");
		responseBuilder.expectHeader("key", "Content-Type");
		responseBuilder.expectHeader("name", "Content-Type");
		responseBuilder.expectHeader("type", "text");
		responseBuilder.expectHeader("user-agent", "Apache-HttpClient/4.5.3 (Java/1.8.0_231)");
		responseBuilder.expectHeader("value", "application/json");
		responseBuilder.expectHeader("Content-Type", "application/json");
		responseBuilder.expectHeader("Transfer-Encoding", "chunked");
		
		responseBuilder.expectBody("isError", equalTo(false));
		responseBuilder.expectBody("respMsg", equalTo("The document was successfully published"));
		responseBuilder.expectBody("respCode", equalTo("document.published"));
		responseSpec = responseBuilder.build();
	}
	
	@Test
	public void DeleteEmployee() throws IOException {
		log.info("Sync ID used for this test: " + syncId);
		given()
			.spec(requestSpec)
		
			.when()
			.post(Resources.resource_one())
			
			.then()
			.spec(responseSpec);
	}
	
	@After
	public void tearDown() throws IOException {
		/**
		 * Validate MongoDB Data needed
		 * 1.) MongoDB Collection (e.g. quote)
		 * 2.) VALUE for the eventMeta.type KEY (e.g. ADD)
		 * 3.) KEY   --> eventMeta.syncId
		 * 4.) VALUE --> syncId
		 */
		ReusableMethods.validateMongoDBData(
				"employee", 
				"DELETE", 
				"eventMeta.syncId", 
				syncId);
		log.info("********** END TEST **********\n");
	}

}
