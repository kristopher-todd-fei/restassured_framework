package OLD_writers.scripts;

import static io.restassured.RestAssured.given;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import camsvc.writers.common.RestUtilities;
import camsvc.writers.common.ReusableMethods;
import camsvc.writers.constants.Paths;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import payLoads.bid.quotegl.Quote;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class QuoteWriterTests2 {
	private static Logger log = LogManager.getLogger(QuoteWriterTests2.class.getName());
	
	Long syncId;
	RequestSpecification reqSpec;
	ResponseSpecification resSpec;
	
	@Before
	public void setup() {
		syncId = ReusableMethods.syncId();
		
		reqSpec = RestUtilities.getRequestSpecification_Quotes();
		reqSpec.basePath(Paths.BASE_PATH);
		
		resSpec = RestUtilities.getResponseSpecification();
		resSpec.header("amq-destination-name", "rti.order");
	}
	
	@Test
	public void MTC001_quote_invalid_quote_type() throws IOException {
		log.info("Sync ID used for this test: " + syncId);
		given()
			.spec(reqSpec)
			.body(Quote.quoteData("TEST", ReusableMethods.dateTime(), syncId))
		.when()
			.post()
		.then()
			.spec(resSpec);
		
		ReusableMethods.validateMongoDBData("quote", "TEST", "eventMeta.syncId", syncId);
	}
	
	@Test
	public void MTC002_quote_EMP_ADD() throws IOException {
		log.info("Sync ID used for this test: " + syncId);
		given()
			.spec(reqSpec)
			.body(Quote.quoteData("ADD", ReusableMethods.dateTime(), syncId))
		.when()
			.post()
		.then()
			.log().all()
			.spec(resSpec);
		
		ReusableMethods.validateMongoDBData("quote", "ADD", "eventMeta.syncId", syncId);
	}
	
	@Test
	public void MTC003_quote_EMP_UPDATE() throws IOException {
		log.info("Sync ID used for this test: " + syncId);
		given()
			.spec(reqSpec)
			.body(Quote.quoteData("UPDATE", ReusableMethods.dateTime(), syncId))
		.when()
			.post()
		.then()
			.spec(resSpec);
		
		ReusableMethods.validateMongoDBData("quote", "UPDATE", "eventMeta.syncId", syncId);
	}
	
	@Test
	public void MTC004_quote_EMP_BULK() throws IOException {
		log.info("Sync ID used for this test: " + syncId);
		given()
			.spec(reqSpec)
			.body(Quote.quoteData("BULK", ReusableMethods.dateTime(), syncId))
		.when()
			.post()
		.then()
			.spec(resSpec);
		
		ReusableMethods.validateMongoDBData("quote", "BULK", "eventMeta.syncId", syncId);
	}
	
	@Test
	public void MTC005_quote_EMP_DELETE() throws IOException {
		log.info("Sync ID used for this test: " + syncId);
		given()
			.spec(reqSpec)
			.body(Quote.quoteData("DELETE", ReusableMethods.dateTime(), syncId))
		.when()
			.post()
		.then()
			.spec(resSpec);
		
		ReusableMethods.validateMongoDBData("quote", "DELETE", "eventMeta.syncId", syncId);
	}
	
	@After
	public void teardown() throws IOException {
		log.info("Test Complete\n");
	}

}
