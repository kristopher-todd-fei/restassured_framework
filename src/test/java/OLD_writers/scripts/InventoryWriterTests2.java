package OLD_writers.scripts;

import static io.restassured.RestAssured.given;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import camsvc.writers.common.RestUtilities;
import camsvc.writers.common.ReusableMethods;
import camsvc.writers.constants.Paths;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import payLoads.Inventory;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class InventoryWriterTests2 {
	private static Logger log = LogManager.getLogger(InventoryWriterTests2.class.getName());
	
	Long syncId;
	RequestSpecification reqSpec;
	ResponseSpecification resSpec;
	
	@Before
	public void setup() {
		syncId = ReusableMethods.syncId();
		
		reqSpec = RestUtilities.getRequestSpecification();
		reqSpec.basePath(Paths.BASE_PATH);
		
		resSpec = RestUtilities.getResponseSpecification();
		resSpec.header("amq-destination-name", "rti.inventory");
	}
	
	@Test
	public void MTC009_inventory_invalid_inventory_type() throws IOException {
		log.info("Sync ID used for this test: " + syncId);
		given()
			.spec(reqSpec)
			.body(Inventory.inventoryData("TEST", ReusableMethods.dateTime(), syncId))
		.when()
			.post()
		.then()
			.spec(resSpec);
		
		ReusableMethods.validateMongoDBData("inventory", "TEST", "eventMeta.syncId", syncId);
	}
	
	@Test
	public void MTC011_inventory_EMP_ADD() throws IOException {
		log.info("Sync ID used for this test: " + syncId);
		given()
			.spec(reqSpec)
			.body(Inventory.inventoryData("ADD", ReusableMethods.dateTime(), syncId))
		.when()
			.post()
		.then()
			.spec(resSpec);
		
		ReusableMethods.validateMongoDBData("inventory", "ADD", "eventMeta.syncId", syncId);
	}
	
	@Test
	public void MTC012_inventory_EMP_UPDATE() throws IOException {
		log.info("Sync ID used for this test: " + syncId);
		given()
			.spec(reqSpec)
			.body(Inventory.inventoryData("UPDATE", ReusableMethods.dateTime(), syncId))
		.when()
			.post()
		.then()
			.spec(resSpec);
		
		ReusableMethods.validateMongoDBData("inventory", "UPDATE", "eventMeta.syncId", syncId);
	}
	
	@Test
	public void MTC013_inventory_EMP_BULK() throws IOException {
		log.info("Sync ID used for this test: " + syncId);
		given()
			.spec(reqSpec)
			.body(Inventory.inventoryData("BULK", ReusableMethods.dateTime(), syncId))
		.when()
			.post()
		.then()
			.spec(resSpec);
		
		ReusableMethods.validateMongoDBData("inventory", "BULK", "eventMeta.syncId", syncId);
	}
	
	@Test
	public void MTC014_inventory_EMP_DELETE() throws IOException {
		log.info("Sync ID used for this test: " + syncId);
		given()
			.spec(reqSpec)
			.body(Inventory.inventoryData("DELETE", ReusableMethods.dateTime(), syncId))
		.when()
			.post()
		.then()
			.spec(resSpec);
		
		ReusableMethods.validateMongoDBData("inventory", "DELETE", "eventMeta.syncId", syncId);
	}
	
	@After
	public void teardown() throws IOException {
		log.info("Test Complete\n");
	}
}
