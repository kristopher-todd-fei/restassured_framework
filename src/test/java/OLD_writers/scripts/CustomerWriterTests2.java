package OLD_writers.scripts;

import static io.restassured.RestAssured.given;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import camsvc.writers.common.RestUtilities;
import camsvc.writers.common.ReusableMethods;
import camsvc.writers.constants.Paths;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import payLoads.Customer;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CustomerWriterTests2 {
	private static Logger log = LogManager.getLogger(CustomerWriterTests2.class.getName());
	
	Long syncId;
	RequestSpecification reqSpec;
	ResponseSpecification resSpec;
	
	@Before
	public void setup() {
		syncId = ReusableMethods.syncId();
		
		reqSpec = RestUtilities.getRequestSpecification();
		reqSpec.basePath(Paths.BASE_PATH);
		
		resSpec = RestUtilities.getResponseSpecification();
		resSpec.header("amq-destination-name", "rti.customer");
	}
	
	@Test
	public void MTC001_customer_invalid_customer_type() throws IOException {
		log.info("Sync ID used for this test: " + syncId);
		given()
			.spec(reqSpec)
			.body(Customer.customerData("TEST", ReusableMethods.dateTime(), syncId))
		.when()
			.post()
		.then()
			.spec(resSpec);
		
		ReusableMethods.validateMongoDBData("customer", "TEST", "eventMeta.syncId", syncId);
	}
	
	@Test
	public void MTC002_customer_EMP_ADD() throws IOException {
		log.info("Sync ID used for this test: " + syncId);
		given()
			.spec(reqSpec)
			.body(Customer.customerData("ADD", ReusableMethods.dateTime(), syncId))
		.when()
			.post()
		.then()
			.spec(resSpec);
		
		ReusableMethods.validateMongoDBData("customer", "ADD", "eventMeta.syncId", syncId);
	}
	
	@Test
	public void MTC003_customer_EMP_UPDATE() throws IOException {
		log.info("Sync ID used for this test: " + syncId);
		given()
			.spec(reqSpec)
			.body(Customer.customerData("UPDATE", ReusableMethods.dateTime(), syncId))
		.when()
			.post()
		.then()
			.spec(resSpec);
		
		ReusableMethods.validateMongoDBData("customer", "UPDATE", "eventMeta.syncId", syncId);
	}
	
	@Test
	public void MTC004_customer_EMP_BULK() throws IOException {
		log.info("Sync ID used for this test: " + syncId);
		given()
			.spec(reqSpec)
			.body(Customer.customerData("BULK", ReusableMethods.dateTime(), syncId))
		.when()
			.post()
		.then()
			.spec(resSpec);
		
		ReusableMethods.validateMongoDBData("customer", "BULK", "eventMeta.syncId", syncId);
	}
	
	@Test
	public void MTC005_customer_EMP_DELETE() throws IOException {
		log.info("Sync ID used for this test: " + syncId);
		given()
			.spec(reqSpec)
			.body(Customer.customerData("DELETE", ReusableMethods.dateTime(), syncId))
		.when()
			.post()
		.then()
			.spec(resSpec);
		
		ReusableMethods.validateMongoDBData("customer", "DELETE", "eventMeta.syncId", syncId);
	}
	
	@After
	public void teardown() throws IOException {
		log.info("Test Complete\n");
	}
}
