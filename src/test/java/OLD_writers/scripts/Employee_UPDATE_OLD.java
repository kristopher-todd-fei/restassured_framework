package OLD_writers.scripts;

import static io.restassured.RestAssured.given;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import camsvc.writers.common.ReusableMethods;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import payLoads.Employee;
import resources.Resources;

public class Employee_UPDATE_OLD {
	
	private static Logger log = LogManager.getLogger(Employee_UPDATE_OLD.class.getName());
	
	// GLOBAL VARIABLES
	Properties prop = new Properties();
	
	@Before
	public void setUp() throws IOException {
		FileInputStream fis = new FileInputStream(System.getProperty("user.dir") + "\\writer.properties");
		prop.load(fis);
	}
	
	@Test
	public void UpdateEmployee() throws IOException {
		RestAssured.baseURI = prop.getProperty("HOST");
		
		Response update_employee = given().
			queryParam("key", prop.getProperty("KEY")).
			queryParam("name", prop.getProperty("NAME")).
			queryParam("value", prop.getProperty("VALUE")).
			queryParam("type", prop.getProperty("TYPE")).
			body(Employee.employeeData("UPDATE", 
					ReusableMethods.dateTime(), ReusableMethods.syncId())).
			when().post(Resources.resource_one()).
			then().assertThat().statusCode(200).and().
			contentType(ContentType.JSON).
			extract().response();
		
		JsonPath jsp = ReusableMethods.rawToJSON(update_employee);
		log.info("UpdateEmployee => isError : " + jsp.getString("isError"));
		log.info("UpdateEmployee => respMsg : " + jsp.getString("respMsg"));
		log.info("UpdateEmployee => respCode: " + jsp.getString("respCode"));
		log.info("UpdateEmployee => transId : " + jsp.getString("transId"));
	}
	
}
