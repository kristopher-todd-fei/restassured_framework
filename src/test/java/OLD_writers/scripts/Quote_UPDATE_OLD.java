package OLD_writers.scripts;

import static io.restassured.RestAssured.given;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import camsvc.writers.common.ReusableMethods;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import payLoads.bid.quotegl.Quote;
import resources.Resources;

public class Quote_UPDATE_OLD {
	
	private static Logger log = LogManager.getLogger(Quote_UPDATE_OLD.class.getName());
	
	// GLOBAL VARIABLES
	Properties prop = new Properties();
	
	@Before
	public void setUp() throws IOException {
		FileInputStream fis = new FileInputStream(System.getProperty("user.dir") + File.separator + "writer.properties");
		prop.load(fis);
	}
	
	@Test
	public void UpdateQuote() throws IOException {
		RestAssured.baseURI = prop.getProperty("HOST_QUOTE");
		
		Response quote = given().
			queryParam("key", prop.getProperty("KEY")).
			queryParam("name", prop.getProperty("NAME")).
			queryParam("value", prop.getProperty("VALUE")).
			queryParam("type", prop.getProperty("TYPE")).
			body(Quote.quoteData("UPDATE", ReusableMethods.dateTime(), ReusableMethods.syncId())).
			when().post(Resources.resource_one()).
			then().assertThat().statusCode(200).and().
			contentType(ContentType.JSON).
			extract().response();
		
		JsonPath jsp = ReusableMethods.rawToJSON(quote);
		log.info("UpdateQuote => isError : " + jsp.getString("isError"));
		log.info("UpdateQuote => respMsg : " + jsp.getString("respMsg"));
		log.info("UpdateQuote => respCode: " + jsp.getString("respCode"));
		log.info("UpdateQuote => transId : " + jsp.getString("transId"));
		
	}

}
