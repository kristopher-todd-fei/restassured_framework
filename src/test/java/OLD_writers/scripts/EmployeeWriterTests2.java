package OLD_writers.scripts;

import static io.restassured.RestAssured.given;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import camsvc.writers.common.RestUtilities;
import camsvc.writers.common.ReusableMethods;
import camsvc.writers.constants.Paths;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import payLoads.Employee;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class EmployeeWriterTests2 {
	private static Logger log = LogManager.getLogger(EmployeeWriterTests2.class.getName());
	
	Long syncId;
	RequestSpecification reqSpec;
	ResponseSpecification resSpec;
	
	@Before
	public void setup() {
		syncId = ReusableMethods.syncId();
		
		reqSpec = RestUtilities.getRequestSpecification();
		reqSpec.basePath(Paths.BASE_PATH);
		
		resSpec = RestUtilities.getResponseSpecification();
		resSpec.header("amq-destination-name", "rti.employee");
	}
	
	@Test
	public void MTC017_employee_invalid_employee_type() throws IOException {
		log.info("Sync ID used for this test: " + syncId);
		given()
			.spec(reqSpec)
			.body(Employee.employeeData("TEST", ReusableMethods.dateTime(), syncId))
		.when()
			.post()
		.then()
			.spec(resSpec);
		
		ReusableMethods.validateMongoDBData("employee", "TEST", "eventMeta.syncId", syncId);
	}
	
	@Test
	public void MTC018_employee_EMP_ADD() throws IOException {
		log.info("Sync ID used for this test: " + syncId);
		given()
			.spec(reqSpec)
			.body(Employee.employeeData("ADD", ReusableMethods.dateTime(), syncId))
		.when()
			.post()
		.then()
			.spec(resSpec);
		
		ReusableMethods.validateMongoDBData("employee", "ADD", "eventMeta.syncId", syncId);
	}
	
	@Test
	public void MTC019_employee_EMP_UPDATE() throws IOException {
		log.info("Sync ID used for this test: " + syncId);
		given()
			.spec(reqSpec)
			.body(Employee.employeeData("UPDATE", ReusableMethods.dateTime(), syncId))
		.when()
			.post()
		.then()
			.spec(resSpec);
		
		ReusableMethods.validateMongoDBData("employee", "UPDATE", "eventMeta.syncId", syncId);
	}
	
	@Test
	public void MTC020_employee_EMP_DELETE() throws IOException {
		log.info("Sync ID used for this test: " + syncId);
		given()
			.spec(reqSpec)
			.body(Employee.employeeData("DELETE", ReusableMethods.dateTime(), syncId))
		.when()
			.post()
		.then()
			.spec(resSpec);
		
		ReusableMethods.validateMongoDBData("employee", "DELETE", "eventMeta.syncId", syncId);
	}
	
	@After
	public void teardown() throws IOException {
		log.info("Test Complete\n");
	}
	
}
