package OLD_writers.scripts;

import static io.restassured.RestAssured.given;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import camsvc.writers.common.ReusableMethods;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import payLoads.Branch;
import resources.Resources;

public class Branch_DELETE_OLD {
	
	private static Logger log = LogManager.getLogger(Branch_DELETE_OLD.class.getName());
	
	// GLOBAL VARIABLES
	Properties prop = new Properties();
	
	@Before
	public void setUp() throws IOException, Exception {
		FileInputStream fis = new FileInputStream(System.getProperty("user.dir") + File.separator + "writer.properties");
		prop.load(fis);
		
	}
	
	@Test
	public void DeleteBranch() throws IOException {
		RestAssured.baseURI = prop.getProperty("HOST");
		
		Response delete_branch = given().
			queryParam("key", prop.getProperty("KEY")).
			queryParam("name", prop.getProperty("NAME")).
			queryParam("value", prop.getProperty("VALUE")).
			queryParam("type", prop.getProperty("TYPE")).
			body(Branch.branchData("DELETE", 
					ReusableMethods.dateTime(), ReusableMethods.syncId())).
			when().post(Resources.resource_one()).
			then().assertThat().statusCode(200).and().
			contentType(ContentType.JSON).
			extract().response();
		
		JsonPath jsp = ReusableMethods.rawToJSON(delete_branch);
		log.info("DeleteBranch => isError : " + jsp.getString("isError"));
		log.info("DeleteBranch => respMsg : " + jsp.getString("respMsg"));
		log.info("DeleteBranch => respCode: " + jsp.getString("respCode"));
		log.info("DeleteBranch => transId : " + jsp.getString("transId"));
	}

}
