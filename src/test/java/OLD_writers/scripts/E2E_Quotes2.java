package OLD_writers.scripts;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;

import camsvc.writers.common.ReusableMethods;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import payLoads.bid.quotegl.Quote;
import resources.Resources;

@RunWith(DataProviderRunner.class)
public class E2E_Quotes2 {
	
	private static Logger log = LogManager.getLogger(E2E_Quotes2.class.getName());
		
	Properties prop = new Properties();
	Long syncId;
	RequestSpecBuilder requestBuilder;
	RequestSpecification requestSpec;
	ResponseSpecBuilder responseBuilder;
	ResponseSpecification responseSpec;
	
	@Before
	public void setUp() throws IOException, Exception {
		FileInputStream fis = new FileInputStream(System.getProperty("user.dir") + File.separator + "writer.properties");
		prop.load(fis);		
		RestAssured.baseURI = prop.getProperty("HOST_QUOTE");
		syncId = ReusableMethods.syncId();
		
		/** Build the REQUEST Specifications **/
		requestBuilder = new RequestSpecBuilder();
		
		requestBuilder.addQueryParam("key", prop.getProperty("KEY"));
		requestBuilder.addQueryParam("name", prop.getProperty("NAME"));
		requestBuilder.addQueryParam("value", prop.getProperty("VALUE"));
		requestBuilder.addQueryParam("type", prop.getProperty("TYPE"));
		requestSpec = requestBuilder.build();
		
		/** Build the RESPONSE specifications **/
		responseBuilder = new ResponseSpecBuilder();
		
		responseBuilder.expectStatusCode(200);
		responseBuilder.expectHeader("Server", "Apache-Coyote/1.1");
		responseBuilder.expectHeader("accept", "*/*");
		responseBuilder.expectHeader("accept-encoding", "gzip,deflate");
		responseBuilder.expectHeader("amq-destination-name", "rti.order");
		responseBuilder.expectHeader("key", "Content-Type");
		responseBuilder.expectHeader("name", "Content-Type");
		responseBuilder.expectHeader("type", "text");
		responseBuilder.expectHeader("value", "application/json");
		responseBuilder.expectHeader("Content-Type", "application/json");
		responseBuilder.expectHeader("Transfer-Encoding", "chunked");
		
		responseBuilder.expectBody("isError", equalTo(false));
		responseBuilder.expectBody("respMsg", equalTo("The document was successfully published"));
		responseBuilder.expectBody("respCode", equalTo("document.published"));
		responseSpec = responseBuilder.build();		
	}
	
	@Test
	@UseDataProvider("data")
	public void quote(
			String collection,
			String eventMeta_type,
			String eventMeta_key) throws IOException {
		
		log.info(eventMeta_type + " Quote");
		
		given()
			.spec(requestSpec)
			.body(Quote.quoteData(
					eventMeta_type, 
					ReusableMethods.dateTime(),
					syncId))
			.when()
			.post(Resources.resource_one())
			.then()
			.spec(responseSpec);
		
		ReusableMethods.validateMongoDBData(
				collection, 
				eventMeta_type, 
				eventMeta_key, 
				syncId);
	}
	
	@After
	public void tearDown() throws IOException, InterruptedException {
		Thread.sleep(1000);
	}
	
	@DataProvider
	public static Object[][] data() {
		return new Object[][] {
			{"quote", "ADD", "eventMeta.syncId"},
			{"quote", "UPDATE", "eventMeta.syncId"},
			{"quote", "DELETE", "eventMeta.syncId"}
		};
	}

}
