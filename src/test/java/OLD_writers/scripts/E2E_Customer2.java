package OLD_writers.scripts;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;

import camsvc.writers.common.ReusableMethods;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import payLoads.Customer;
import resources.Resources;

@RunWith(DataProviderRunner.class)
public class E2E_Customer2 {
	
	private static Logger log = LogManager.getLogger(E2E_Customer2.class.getName());
	
	static Properties prop = new Properties();
	static Long syncId;
	static RequestSpecBuilder requestBuilder;
	static RequestSpecification requestSpec;
	static ResponseSpecBuilder responseBuilder;
	static ResponseSpecification responseSpec;
	
	@Before
	public void init() throws IOException, Exception {
		
		FileInputStream fis = new FileInputStream(System.getProperty("user.dir") + File.separator + "writer.properties");
		prop.load(fis);
		RestAssured.baseURI = prop.getProperty("HOST");
		syncId = ReusableMethods.syncId();	
		
		/** Build the REQUEST Specifications **/
		requestBuilder = new RequestSpecBuilder();
		
		requestBuilder.addQueryParam("key", prop.getProperty("KEY"));
		requestBuilder.addQueryParam("name", prop.getProperty("NAME"));
		requestBuilder.addQueryParam("value", prop.getProperty("VALUE"));
		requestBuilder.addQueryParam("type", prop.getProperty("TYPE"));
		requestSpec = requestBuilder.build();
		
		/** Build the RESPONSE specifications **/
		responseBuilder = new ResponseSpecBuilder();
		
		responseBuilder.expectStatusCode(200);
		responseBuilder.expectHeader("Server", "Apache-Coyote/1.1");
		responseBuilder.expectHeader("accept", "*/*");
		responseBuilder.expectHeader("accept-encoding", "gzip,deflate");
		responseBuilder.expectHeader("amq-destination-name", "rti.customer");
		responseBuilder.expectHeader("key", "Content-Type");
		responseBuilder.expectHeader("name", "Content-Type");
		responseBuilder.expectHeader("type", "text");
		responseBuilder.expectHeader("value", "application/json");
		responseBuilder.expectHeader("Content-Type", "application/json");
		responseBuilder.expectHeader("Transfer-Encoding", "chunked");
		
		responseBuilder.expectBody("isError", equalTo(false));
		responseBuilder.expectBody("respMsg", equalTo("The document was successfully published"));
		responseBuilder.expectBody("respCode", equalTo("document.published"));
		responseSpec = responseBuilder.build();
	}
	
	@Test
	@UseDataProvider("data")
	public void customer(
			String collection,
			String eventMeta_type,
			String eventMeta_key) throws IOException {
		
		log.info(eventMeta_type + " Customer");
		
		given()
			.spec(requestSpec)
			.body(Customer.customerData(
					eventMeta_type,
					ReusableMethods.dateTime(),
					syncId))
			.when()
			.post(Resources.resource_one())
			.then()
			.spec(responseSpec);
		
		ReusableMethods.validateMongoDBData(
				collection, 
				eventMeta_type, 
				eventMeta_key, 
				syncId);
	}
	
	@After
	public void tearDown() throws IOException, InterruptedException {
		Thread.sleep(1000);		
	}
	
	@DataProvider
	public static Object[][] data() {
		return new Object[][] {
			{"customer", "ADD", "eventMeta.syncId"},
			{"customer", "UPDATE", "eventMeta.syncId"},
			{"customer", "DELETE", "eventMeta.syncId"}
		};
	}

}
