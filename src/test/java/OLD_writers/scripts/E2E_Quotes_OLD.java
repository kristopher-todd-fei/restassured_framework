package OLD_writers.scripts;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import camsvc.writers.common.ReusableMethods;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import payLoads.bid.quotegl.Quote;
import resources.Resources;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class E2E_Quotes_OLD {
	
	private static Logger log = LogManager.getLogger(E2E_Quotes_OLD.class.getName());
		
	Properties prop = new Properties();
	Long syncId;
	RequestSpecBuilder requestBuilder;
	RequestSpecification requestSpec;
	ResponseSpecBuilder responseBuilder;
	ResponseSpecification responseSpec;
	
	@Before
	public void setUp() throws IOException, Exception {
		log.info("********** BEGIN TEST **********");
		FileInputStream fis = new FileInputStream(System.getProperty("user.dir") + File.separator + "writer.properties");
		prop.load(fis);		
		RestAssured.baseURI = prop.getProperty("HOST_QUOTE");
		syncId = ReusableMethods.syncId();
		
		/** Build the REQUEST Specifications **/
		requestBuilder = new RequestSpecBuilder();
		
		requestBuilder.addQueryParam("key", prop.getProperty("KEY"));
		requestBuilder.addQueryParam("name", prop.getProperty("NAME"));
		requestBuilder.addQueryParam("value", prop.getProperty("VALUE"));
		requestBuilder.addQueryParam("type", prop.getProperty("TYPE"));
		requestSpec = requestBuilder.build();
		
		/** Build the RESPONSE specifications **/
		responseBuilder = new ResponseSpecBuilder();
		
		responseBuilder.expectStatusCode(200);
		responseBuilder.expectHeader("Server", "Apache-Coyote/1.1");
		responseBuilder.expectHeader("accept", "*/*");
		responseBuilder.expectHeader("accept-encoding", "gzip,deflate");
		responseBuilder.expectHeader("amq-destination-name", "rti.order");
		responseBuilder.expectHeader("key", "Content-Type");
		responseBuilder.expectHeader("name", "Content-Type");
		responseBuilder.expectHeader("type", "text");
		// responseBuilder.expectHeader("user-agent", "Apache-HttpClient/4.5.3 (Java/1.8.0_231)");
		responseBuilder.expectHeader("value", "application/json");
		responseBuilder.expectHeader("Content-Type", "application/json");
		responseBuilder.expectHeader("Transfer-Encoding", "chunked");
		
		responseBuilder.expectBody("isError", equalTo(false));
		responseBuilder.expectBody("respMsg", equalTo("The document was successfully published"));
		responseBuilder.expectBody("respCode", equalTo("document.published"));
		responseSpec = responseBuilder.build();
		
	}
	
	@Test
	public void testA_addQuote() throws IOException {
		log.info("Sync ID used for this test: " + syncId);
		given()
			.spec(requestSpec)
			.body(Quote.quoteData(
					"ADD", 
					ReusableMethods.dateTime(),
					syncId))
			.when()
			.post(Resources.resource_one())
			.then()
			.spec(responseSpec);
	}
	
	@Test
	public void testB_updateQuote() throws IOException {
		log.info("Sync ID used for this test: " + syncId);
		given()
			.spec(requestSpec)
			.body(Quote.quoteData(
					"UPDATE", 
					ReusableMethods.dateTime(),
					syncId))
			.when()
			.post(Resources.resource_one())
			.then()
			.spec(responseSpec);
	}
	
	@Test
	public void testC_deleteQuote() throws IOException {
		log.info("Sync ID used for this test: " + syncId);
		given()
			.spec(requestSpec)
			.body(Quote.quoteData(
					"DELETE", 
					ReusableMethods.dateTime(),
					syncId))
			.when()
			.post(Resources.resource_one())
			.then()
			.spec(responseSpec);
	}
	
	@After
	public void tearDown() throws IOException, InterruptedException {
		Thread.sleep(1000);
		/** *************************** Collection********Key********* value **/
		ReusableMethods.validateMongoDBData("quote", "", "eventMeta.syncId", syncId);
		log.info("********** END TEST **********\n");
	}

}
