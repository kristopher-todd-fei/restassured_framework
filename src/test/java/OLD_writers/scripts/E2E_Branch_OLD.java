package OLD_writers.scripts;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import camsvc.writers.common.ReusableMethods;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import payLoads.Branch;
import resources.Resources;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class E2E_Branch_OLD {
	
	private static Logger log = LogManager.getLogger(E2E_Branch_OLD.class.getName());
	
	static Properties prop = new Properties();
	static Long syncId;
	static RequestSpecBuilder requestBuilder;
	static RequestSpecification requestSpec;
	static ResponseSpecBuilder responseBuilder;
	static ResponseSpecification responseSpec;
	
	@Before
	public void init() throws IOException, Exception {
		log.info("********** BEGIN TEST **********");
		FileInputStream fis = new FileInputStream(System.getProperty("user.dir") + File.separator + "writer.properties");
		prop.load(fis);
		RestAssured.baseURI = prop.getProperty("HOST");
		syncId = ReusableMethods.syncId();	
		
		/** Build the REQUEST Specifications **/
		requestBuilder = new RequestSpecBuilder();
		
		requestBuilder.addQueryParam("key", prop.getProperty("KEY"));
		requestBuilder.addQueryParam("name", prop.getProperty("NAME"));
		requestBuilder.addQueryParam("value", prop.getProperty("VALUE"));
		requestBuilder.addQueryParam("type", prop.getProperty("TYPE"));
		requestSpec = requestBuilder.build();
		
		/** Build the RESPONSE specifications **/
		responseBuilder = new ResponseSpecBuilder();
		
		responseBuilder.expectStatusCode(200);
		responseBuilder.expectHeader("Server", "Apache-Coyote/1.1");
		responseBuilder.expectHeader("accept", "*/*");
		responseBuilder.expectHeader("accept-encoding", "gzip,deflate");
		responseBuilder.expectHeader("amq-destination-name", "rti.branch");
		responseBuilder.expectHeader("key", "Content-Type");
		responseBuilder.expectHeader("name", "Content-Type");
		responseBuilder.expectHeader("type", "text");
		// responseBuilder.expectHeader("user-agent", "Apache-HttpClient/4.5.3 (Java/1.8.0_231)");
		responseBuilder.expectHeader("value", "application/json");
		responseBuilder.expectHeader("Content-Type", "application/json");
		responseBuilder.expectHeader("Transfer-Encoding", "chunked");
		
		responseBuilder.expectBody("isError", equalTo(false));
		responseBuilder.expectBody("respMsg", equalTo("The document was successfully published"));
		responseBuilder.expectBody("respCode", equalTo("document.published"));
		responseSpec = responseBuilder.build();
	}
	
	@Test
	public void testA_addBranch() throws IOException {
		log.info("Sync ID used for this test: " + syncId);
		given()
			.spec(requestSpec)
			.body(Branch.branchData(
					"ADD",
					ReusableMethods.dateTime(),
					syncId))
			.when()
			.post(Resources.resource_one())
			.then()
			.spec(responseSpec);
	}
	
	@Test
	public void testB_updateBranch() throws IOException {
		log.info("Sync ID used for this test: " + syncId);
		given()
			.spec(requestSpec)
			.body(Branch.branchData(
					"UPDATE",
					ReusableMethods.dateTime(),
					syncId))
			.when()
			.post(Resources.resource_one())
			.then()
			.spec(responseSpec);
	}
	
	@Test
	public void testC_deleteBranch() throws IOException {
		log.info("Sync ID used for this test: " + syncId);
		given()
			.spec(requestSpec)
			.body(Branch.branchData(
					"DELETE",
					ReusableMethods.dateTime(),
					syncId))
			.when()
			.post(Resources.resource_one())
			.then()
			.spec(responseSpec);
	}
	
	@After
	public void tearDown() throws IOException, InterruptedException {
		Thread.sleep(1000);
		/**
		 * Validate MongoDB Data needed
		 * 1.) MongoDB Collection (e.g. quote)
		 * 2.) VALUE for the eventMeta.type KEY (e.g. ADD)
		 * 3.) KEY   --> eventMeta.syncId
		 * 4.) VALUE --> syncId
		 */
		ReusableMethods.validateMongoDBData("branch", "ADD", "eventMeta.syncId", syncId);
		log.info("********** END TEST **********\n");
	}

}
