package payLoads.product_price;

public class GetProductPrice {
	
	public static String validFields() {
		String payload = "{\r\n" + 
				"  \"customerId\": \"27893\",\r\n" + 
				"  \"branchId\": \"DIST\",\r\n" + 
				"  \"contractId\": \"12345\",\r\n" + 
				"  \"shipWhseId\": \"986\",\r\n" + 
				"  \"products\": [\r\n" + 
				"    {\r\n" + 
				"      \"productId\": \"88526\",\r\n" + 
				"      \"quantity\": \"3\"\r\n" + 
				"    }\r\n" + 
				"  ]\r\n" + 
				"}";
		
		return payload;
	}
	
	public static String invalidCustomerId() {
		String payload = "{\r\n" + 
				"  \"customerId\": \"454354354354\",\r\n" + 
				"  \"branchId\": \"DIST\",\r\n" + 
				"  \"contractId\": \"12345\",\r\n" + 
				"  \"shipWhseId\": \"986\",\r\n" + 
				"  \"products\": [\r\n" + 
				"    {\r\n" + 
				"      \"productId\": \"88526\",\r\n" + 
				"      \"quantity\": \"3\"\r\n" + 
				"    }\r\n" + 
				"  ]\r\n" + 
				"}";
		
		return payload;
	}
	
	public static String emptyCustomerId() {
		String payload = "{\r\n" + 
				"  \"customerId\": \"\",\r\n" + 
				"  \"branchId\": \"DIST\",\r\n" + 
				"  \"contractId\": \"12345\",\r\n" + 
				"  \"shipWhseId\": \"986\",\r\n" + 
				"  \"products\": [\r\n" + 
				"    {\r\n" + 
				"      \"productId\": \"88526\",\r\n" + 
				"      \"quantity\": \"3\"\r\n" + 
				"    }\r\n" + 
				"  ]\r\n" + 
				"}\r\n" + 
				"";
		
		return payload;
	}
	
	public static String invalidBranchId() {
		String payload = "{\r\n" + 
				"  \"customerId\": \"27893\",\r\n" + 
				"  \"branchId\": \"dfdsfdsfdsf\",\r\n" + 
				"  \"contractId\": \"12345\",\r\n" + 
				"  \"shipWhseId\": \"986\",\r\n" + 
				"  \"products\": [\r\n" + 
				"    {\r\n" + 
				"      \"productId\": \"88526\",\r\n" + 
				"      \"quantity\": \"3\"\r\n" + 
				"    }\r\n" + 
				"  ]\r\n" + 
				"}";
		
		return payload;
	}
	
	public static String emptyBranchId() {
		String payload = "{\r\n" + 
				"  \"customerId\": \"27893\",\r\n" + 
				"  \"branchId\": \"\",\r\n" + 
				"  \"contractId\": \"12345\",\r\n" + 
				"  \"shipWhseId\": \"986\",\r\n" + 
				"  \"products\": [\r\n" + 
				"    {\r\n" + 
				"      \"productId\": \"88526\",\r\n" + 
				"      \"quantity\": \"3\"\r\n" + 
				"    }\r\n" + 
				"  ]\r\n" + 
				"}";
		
		return payload;
	}
	
	public static String invalidProduct() {
		String payload = "{\r\n" + 
				"  \"customerId\": \"27893\",\r\n" + 
				"  \"branchId\": \"DIST\",\r\n" + 
				"  \"contractId\": \"12345\",\r\n" + 
				"  \"shipWhseId\": \"986\",\r\n" + 
				"  \"products\": [\r\n" + 
				"    {\r\n" + 
				"      \"productId\": \"53434324354354352\",\r\n" + 
				"      \"quantity\": \"3\"\r\n" + 
				"    }\r\n" + 
				"  ]\r\n" + 
				"}";
		
		return payload;
	}
	
	public static String emptyProduct() {
		String payload = "{\r\n" + 
				"  \"customerId\": \"27893\",\r\n" + 
				"  \"branchId\": \"DIST\",\r\n" + 
				"  \"contractId\": \"12345\",\r\n" + 
				"  \"shipWhseId\": \"986\",\r\n" + 
				"  \"products\": [\r\n" + 
				"    {\r\n" + 
				"      \r\n" + 
				"    }\r\n" + 
				"  ]\r\n" + 
				"}";
		
		return payload;
	}
	
	public static String invalidProductId() {
		String payload = "{\r\n" + 
				"  \"customerId\": \"27893\",\r\n" + 
				"  \"branchId\": \"DIST\",\r\n" + 
				"  \"contractId\": \"12345\",\r\n" + 
				"  \"shipWhseId\": \"986\",\r\n" + 
				"  \"products\": [\r\n" + 
				"    {\r\n" + 
				"      \"productId\": \"53434324354354352\",\r\n" + 
				"      \"quantity\": \"3\"\r\n" + 
				"    }\r\n" + 
				"  ]\r\n" + 
				"}";
		
		return payload;
	}
	
	public static String emptyProductId() {
		String payload = "{\r\n" + 
				"  \"customerId\": \"27893\",\r\n" + 
				"  \"branchId\": \"DIST\",\r\n" + 
				"  \"contractId\": \"12345\",\r\n" + 
				"  \"shipWhseId\": \"986\",\r\n" + 
				"  \"products\": [\r\n" + 
				"    {\r\n" + 
				"      \"productId\": \"\",\r\n" + 
				"      \"quantity\": \"3\"\r\n" + 
				"    }\r\n" + 
				"  ]\r\n" + 
				"}";
		
		return payload;
	}
	
	public static String invalidQuantity() {
		String payload = "{\r\n" + 
				"  \"customerId\": \"27893\",\r\n" + 
				"  \"branchId\": \"DIST\",\r\n" + 
				"  \"contractId\": \"12345\",\r\n" + 
				"  \"shipWhseId\": \"986\",\r\n" + 
				"  \"products\": [\r\n" + 
				"    {\r\n" + 
				"      \"productId\": \"88526\",\r\n" + 
				"      \"quantity\": \"34343124343\"\r\n" + 
				"    }\r\n" + 
				"  ]\r\n" + 
				"}";
		
		return payload;
	}
	
	public static String emptyQuantity() {
		String payload = "{\r\n" + 
				"  \"customerId\": \"27893\",\r\n" + 
				"  \"branchId\": \"DIST\",\r\n" + 
				"  \"contractId\": \"12345\",\r\n" + 
				"  \"shipWhseId\": \"986\",\r\n" + 
				"  \"products\": [\r\n" + 
				"    {\r\n" + 
				"      \"productId\": \"88526\",\r\n" + 
				"      \"quantity\": \"\"\r\n" + 
				"    }\r\n" + 
				"  ]\r\n" + 
				"}";
		
		return payload;
	}
}
