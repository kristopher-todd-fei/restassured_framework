package payLoads;

public class Customer {
	
	public static String customerData(String type, String date, Long syncId) {
		
		String payload = "{\r\n" + 
				"  \"docType\": \"customer\",\r\n" + 
				"  \"version\": 1,\r\n" + 
				"  \"events\": [\r\n" + 
				"    {\r\n" + 
				"      \"event\": {\r\n" + 
				"        \"system\": \"DEV\",\r\n" + 
				"        \"account\": \"DALLAS\",\r\n" + 
				"        \"file\": \"CUSTOMER\",\r\n" + 
				"        \"record\": \"458500\",\r\n" + 
				"        \"author\": \"AAO8676\",\r\n" + 
				"        \"type\": \""+type+"\",\r\n" + 
				"        \"date\": \""+date+"\",\r\n" + 
				"        \"syncId\":\""+syncId+"\",\r\n" + 
				"        \"data\": {\r\n" + 
				"          \"header\": {\r\n" + 
				"            \"custName\": \"BLACKMOND SHEETMETAL\",\r\n" + 
				"            \"custAddr1\": \"1617 WEST LUCAS STREET\",\r\n" + 
				"            \"custAddr2\": \"FLORENCE, SC 29502\",\r\n" + 
				"            \"custAddr3\": null,\r\n" + 
				"            \"custAddr4\": null,\r\n" + 
				"            \"custZip\": \"29502\",\r\n" + 
				"            \"contactName\": null,\r\n" + 
				"            \"custPhone\": \"843-665-7155\",\r\n" + 
				"            \"shipViaCode\": null,\r\n" + 
				"            \"shipInstr\": null,\r\n" + 
				"            \"custTerms\": \"COD\",\r\n" + 
				"            \"creditCode\": \"COD\",\r\n" + 
				"            \"creditLimitAmt\": 0,\r\n" + 
				"            \"custType\": \"B_INDUS\",\r\n" + 
				"            \"slsmCode\": \"KG\",\r\n" + 
				"            \"taxJur\": \"SC2286\",\r\n" + 
				"            \"custSinceMths\": 64,\r\n" + 
				"            \"taxExemptNum\": null,\r\n" + 
				"            \"latePayPct\": 0,\r\n" + 
				"            \"blindBillFlag\": null,\r\n" + 
				"            \"reprintNumCopies\": 0,\r\n" + 
				"            \"lastSaleDate\": \"02/26/18\",\r\n" + 
				"            \"lastRcptDate\": \"10/01/19\",\r\n" + 
				"            \"arBalAmt\": -599989.21,\r\n" + 
				"            \"openOrdBalAmt\": 28661184000.96,\r\n" + 
				"            \"slsMtdAmt\": 9.99,\r\n" + 
				"            \"slsYtdAmt\": 22.09,\r\n" + 
				"            \"returnsMtdAmt\": 0.41,\r\n" + 
				"            \"returnsYtdAmt\": 0.41,\r\n" + 
				"            \"gpMtdAmt\": 2.99,\r\n" + 
				"            \"gpYtdAmt\": 5.89,\r\n" + 
				"            \"noBumpFlag\": null,\r\n" + 
				"            \"rstrAmt\": null,\r\n" + 
				"            \"prtListdiscFlag\": \"N\",\r\n" + 
				"            \"priceClassCode\": \"001\",\r\n" + 
				"            \"creditRatingCode\": null,\r\n" + 
				"            \"cutoffCode\": null,\r\n" + 
				"            \"futureInvAmt\": -599989.21,\r\n" + 
				"            \"currInvAmt\": null,\r\n" + 
				"            \"over30Amt\": null,\r\n" + 
				"            \"over60Amt\": null,\r\n" + 
				"            \"over90Amt\": null,\r\n" + 
				"            \"smallCrgFlag\": \"N\",\r\n" + 
				"            \"alphaSortCode\": \"ZZZZZZZZZZ\",\r\n" + 
				"            \"custAlpha\": \"BLACK\",\r\n" + 
				"            \"custBranchId\": \"1119\",\r\n" + 
				"            \"territory\": \"456530\",\r\n" + 
				"            \"slsGlId\": null,\r\n" + 
				"            \"arGlId\": \"1300\",\r\n" + 
				"            \"dlvChargeAmt\": 0,\r\n" + 
				"            \"jobNameReqrdFlag\": \"N\",\r\n" + 
				"            \"boAcceptFlag\": \"0\",\r\n" + 
				"            \"fincContactName\": null,\r\n" + 
				"            \"reqrdCustPoFlag\": \"N\",\r\n" + 
				"            \"unprocArAmt\": 0,\r\n" + 
				"            \"corpName\": null,\r\n" + 
				"            \"dunsNum\": null,\r\n" + 
				"            \"pmtMtdAmt\": 600000,\r\n" + 
				"            \"custStatus\": \"T\",\r\n" + 
				"            \"printPriceFlag\": \"Y\",\r\n" + 
				"            \"dontforgetComment\": null,\r\n" + 
				"            \"mainCustId\": null,\r\n" + 
				"            \"custPoPattern\": null,\r\n" + 
				"            \"jobCustFlag\": \"N\",\r\n" + 
				"            \"custCreateDate\": \"10/24/08\",\r\n" + 
				"            \"altSlsmCode\": \"H99\",\r\n" + 
				"            \"crLimStatus\": null,\r\n" + 
				"            \"crLimExpDate\": null,\r\n" + 
				"            \"crLimLastchgDate\": null,\r\n" + 
				"            \"guaranteeFlag\": \"N\",\r\n" + 
				"            \"fincStmtLastDate\": null,\r\n" + 
				"            \"grossSlsAmt\": null,\r\n" + 
				"            \"netWrkgCptlAmt\": null,\r\n" + 
				"            \"netWorthAmt\": null,\r\n" + 
				"            \"custEstbDate\": null,\r\n" + 
				"            \"lastDnbRptDate\": null,\r\n" + 
				"            \"dnbRating\": null,\r\n" + 
				"            \"creditRegion\": null,\r\n" + 
				"            \"ctrlBranchId\": \"1430\",\r\n" + 
				"            \"creditAppOnfileFlag\": null,\r\n" + 
				"            \"noAutoCostFlag\": null,\r\n" + 
				"            \"reserveForId\": null,\r\n" + 
				"            \"reserveById\": null,\r\n" + 
				"            \"ediPartner\": null,\r\n" + 
				"            \"printEdiOrdFlag\": \"N\",\r\n" + 
				"            \"ediPriceDiscrpFlag\": null,\r\n" + 
				"            \"creditMgrEmpId\": \"302918\",\r\n" + 
				"            \"rcptSortType\": \"JD\",\r\n" + 
				"            \"rstrRsnCode\": null,\r\n" + 
				"            \"custFaxNum\": null,\r\n" + 
				"            \"asnReqrdFlag\": null,\r\n" + 
				"            \"shipCompleteFlag\": \"N\",\r\n" + 
				"            \"priceAdjustId\": null,\r\n" + 
				"            \"creditScore\": null,\r\n" + 
				"            \"printSortFlag\": \"Y\",\r\n" + 
				"            \"claimbackFlag\": null,\r\n" + 
				"            \"stmtSeq\": null,\r\n" + 
				"            \"faxConfCode\": null,\r\n" + 
				"            \"pricePrecision\": null,\r\n" + 
				"            \"futurePayAmt\": null,\r\n" + 
				"            \"assocInfo\": null,\r\n" + 
				"            \"labelInfo\": null,\r\n" + 
				"            \"supplierNum\": null,\r\n" + 
				"            \"consignBal\": null,\r\n" + 
				"            \"prepDays\": null,\r\n" + 
				"            \"reviewFrtDlvFlag\": null,\r\n" + 
				"            \"custSeq\": null,\r\n" + 
				"            \"expNocntrtFlag\": null,\r\n" + 
				"            \"expCntrtFlag\": null,\r\n" + 
				"            \"reqrdOrdByFlag\": null,\r\n" + 
				"            \"useFrtTableFlag\": \"N\",\r\n" + 
				"            \"mcustId\": \"190554\",\r\n" + 
				"            \"mcustChgDate\": \"02/27/18\",\r\n" + 
				"            \"mcustChgByEmpId\": \"711150\",\r\n" + 
				"            \"processStatus\": \"UNP\",\r\n" + 
				"            \"processDate\": null,\r\n" + 
				"            \"processByEmpId\": null,\r\n" + 
				"            \"shipInstr2\": null,\r\n" + 
				"            \"shipInstr3\": null,\r\n" + 
				"            \"shipInstr4\": null,\r\n" + 
				"            \"shipToAttn\": null,\r\n" + 
				"            \"shipToPhone\": null,\r\n" + 
				"            \"prtExtPriceFlag\": null,\r\n" + 
				"            \"rndType\": null,\r\n" + 
				"            \"rndNearDec\": null,\r\n" + 
				"            \"rndMinAmt\": null,\r\n" + 
				"            \"rndCntrtFlag\": null,\r\n" + 
				"            \"countryCode\": \"US\",\r\n" + 
				"            \"ccRptFlag\": null,\r\n" + 
				"            \"countyName\": \"FLORENCE\",\r\n" + 
				"            \"gsaLink\": \"N\",\r\n" + 
				"            \"geocode\": \"410410310\",\r\n" + 
				"            \"auditReqrdFlag\": null,\r\n" + 
				"            \"verifyCheckFlag\": null,\r\n" + 
				"            \"bestPriceCode\": null,\r\n" + 
				"            \"ecreditArBal\": 0,\r\n" + 
				"            \"ecreditArFlag\": \"Y\",\r\n" + 
				"            \"ecreditArCurrBal\": null,\r\n" + 
				"            \"ecreditAr30Bal\": null,\r\n" + 
				"            \"ecreditAr60Bal\": null,\r\n" + 
				"            \"ecreditAr90Bal\": null,\r\n" + 
				"            \"dealerInfo\": null,\r\n" + 
				"            \"dealers\": null,\r\n" + 
				"            \"showPriceFlag\": null,\r\n" + 
				"            \"ecreditArFutureBal\": 0,\r\n" + 
				"            \"b2bCustFlag\": null,\r\n" + 
				"            \"frtBumpFlag\": null,\r\n" + 
				"            \"autoPodFlag\": null,\r\n" + 
				"            \"scndSlsm\": null,\r\n" + 
				"            \"commPct\": null,\r\n" + 
				"            \"referralId\": null,\r\n" + 
				"            \"prtCommentsType\": null,\r\n" + 
				"            \"jobNamePattern\": null,\r\n" + 
				"            \"emailConfCode\": null,\r\n" + 
				"            \"bmiBudCustType\": \"B_INDUS\",\r\n" + 
				"            \"bmiRptCustType\": \"B_INDUS\",\r\n" + 
				"            \"ldlMdm\": null,\r\n" + 
				"            \"printMstrProdFlag\": null,\r\n" + 
				"            \"specialtySalesman\": null,\r\n" + 
				"            \"sourceBidNum\": null,\r\n" + 
				"            \"showCustOnlineFlag\": \"Y\",\r\n" + 
				"            \"webHoldCtrl\": null,\r\n" + 
				"            \"tcWhse\": null,\r\n" + 
				"            \"tcSlsm\": null,\r\n" + 
				"            \"rEmpNum\": null,\r\n" + 
				"            \"vCrossCustAcct\": null,\r\n" + 
				"            \"vCrossCustFlag\": null,\r\n" + 
				"            \"vCrossCustId\": null,\r\n" + 
				"            \"vGenrInv2Code\": null,\r\n" + 
				"            \"vGenrInvCode\": null,\r\n" + 
				"            \"vGenrStmt2Code\": null,\r\n" + 
				"            \"vGenrStmtCode\": \"N\",\r\n" + 
				"            \"vPrtNegInvFlag\": \"N\",\r\n" + 
				"            \"vPrtStmtFlag\": \"N\",\r\n" + 
				"            \"vPrtStmtNegFlag\": \"N\",\r\n" + 
				"            \"vPrtStmtNoactFlag\": \"N\",\r\n" + 
				"            \"vPrtStmtZeroFlag\": \"N\",\r\n" + 
				"            \"vPrtZeroInvFlag\": \"N\"\r\n" + 
				"          },\r\n" + 
				"          \"bumps\": {\r\n" + 
				"            \"rankBump\": [\r\n" + 
				"              null\r\n" + 
				"            ]\r\n" + 
				"          },\r\n" + 
				"          \"shipTo\": {\r\n" + 
				"            \"shipToSuffix\": [\r\n" + 
				"              \"0000\"\r\n" + 
				"            ]\r\n" + 
				"          },\r\n" + 
				"          \"ttlByMth\": {\r\n" + 
				"            \"roll12SlsAmt\": [\r\n" + 
				"              null,\r\n" + 
				"              null,\r\n" + 
				"              null,\r\n" + 
				"              null,\r\n" + 
				"              12.1,\r\n" + 
				"              null,\r\n" + 
				"              null,\r\n" + 
				"              null,\r\n" + 
				"              553.2,\r\n" + 
				"              null,\r\n" + 
				"              null,\r\n" + 
				"              null\r\n" + 
				"            ],\r\n" + 
				"            \"roll12GpAmt\": [\r\n" + 
				"              null,\r\n" + 
				"              null,\r\n" + 
				"              null,\r\n" + 
				"              null,\r\n" + 
				"              2.9,\r\n" + 
				"              null,\r\n" + 
				"              null,\r\n" + 
				"              null,\r\n" + 
				"              86.93,\r\n" + 
				"              null,\r\n" + 
				"              null,\r\n" + 
				"              null\r\n" + 
				"            ]\r\n" + 
				"          },\r\n" + 
				"          \"cntrts\": {\r\n" + 
				"            \"contractId\": [\r\n" + 
				"              null\r\n" + 
				"            ],\r\n" + 
				"            \"ovrWhseId\": [\r\n" + 
				"              null\r\n" + 
				"            ],\r\n" + 
				"            \"exprInfo\": [\r\n" + 
				"              null\r\n" + 
				"            ],\r\n" + 
				"            \"lastReviewInfo\": [\r\n" + 
				"              null\r\n" + 
				"            ],\r\n" + 
				"            \"marketFundFlag\": [\r\n" + 
				"              null\r\n" + 
				"            ],\r\n" + 
				"            \"tripPointsFlag\": [\r\n" + 
				"              null\r\n" + 
				"            ],\r\n" + 
				"            \"quoteId\": [\r\n" + 
				"              null\r\n" + 
				"            ],\r\n" + 
				"            \"claimbackCustId\": [\r\n" + 
				"              null\r\n" + 
				"            ],\r\n" + 
				"            \"contractName\": [\r\n" + 
				"              null\r\n" + 
				"            ],\r\n" + 
				"            \"restrictContract\": [\r\n" + 
				"              null\r\n" + 
				"            ],\r\n" + 
				"            \"vCntrtExpDate\": [\r\n" + 
				"              null\r\n" + 
				"            ],\r\n" + 
				"            \"vCntrtExpText\": [\r\n" + 
				"              null\r\n" + 
				"            ]\r\n" + 
				"          },\r\n" + 
				"          \"jobs\": {\r\n" + 
				"            \"jobCustId\": [\r\n" + 
				"              null\r\n" + 
				"            ]\r\n" + 
				"          },\r\n" + 
				"          \"emails\": {\r\n" + 
				"            \"ediNotifyEmpId\": [\r\n" + 
				"              null\r\n" + 
				"            ],\r\n" + 
				"            \"omlWhseId\": [\r\n" + 
				"              null\r\n" + 
				"            ],\r\n" + 
				"            \"emailNotifyEmpId\": [\r\n" + 
				"              null\r\n" + 
				"            ],\r\n" + 
				"            \"emailNotifyWhseId\": [\r\n" + 
				"              null\r\n" + 
				"            ],\r\n" + 
				"            \"emailNotifyEmpName\": [\r\n" + 
				"              null\r\n" + 
				"            ]\r\n" + 
				"          },\r\n" + 
				"          \"audits\": {\r\n" + 
				"            \"rcptCashIrsTrackAmt\": [\r\n" + 
				"              null\r\n" + 
				"            ],\r\n" + 
				"            \"crLineAudit\": [\r\n" + 
				"              null\r\n" + 
				"            ]\r\n" + 
				"          },\r\n" + 
				"          \"rstrs\": {\r\n" + 
				"            \"restrictCode\": [\r\n" + 
				"              null\r\n" + 
				"            ],\r\n" + 
				"            \"restrictCodeDate\": [\r\n" + 
				"              null\r\n" + 
				"            ],\r\n" + 
				"            \"restrictCodeNotes\": [\r\n" + 
				"              null\r\n" + 
				"            ],\r\n" + 
				"            \"restrictByEmpId\": [\r\n" + 
				"              null\r\n" + 
				"            ]\r\n" + 
				"          },\r\n" + 
				"          \"buyers\": {\r\n" + 
				"            \"buyerName\": [\r\n" + 
				"              null\r\n" + 
				"            ],\r\n" + 
				"            \"buyerTitle\": [\r\n" + 
				"              null\r\n" + 
				"            ]\r\n" + 
				"          },\r\n" + 
				"          \"dtpds\": {\r\n" + 
				"            \"dtpdWhseId\": [\r\n" + 
				"              null\r\n" + 
				"            ],\r\n" + 
				"            \"dtpdPrepDays\": [\r\n" + 
				"              null\r\n" + 
				"            ]\r\n" + 
				"          },\r\n" + 
				"          \"stmts\": {\r\n" + 
				"            \"prtInvOptFlag\": [\r\n" + 
				"              null\r\n" + 
				"            ],\r\n" + 
				"            \"prtDlvOptFlag\": [\r\n" + 
				"              null\r\n" + 
				"            ]\r\n" + 
				"          },\r\n" + 
				"          \"sources\": {\r\n" + 
				"            \"srcMain\": [\r\n" + 
				"              \"1390\"\r\n" + 
				"            ],\r\n" + 
				"            \"srcAccount\": [\r\n" + 
				"              \"IHUBCAR\"\r\n" + 
				"            ],\r\n" + 
				"            \"srcCust\": [\r\n" + 
				"              \"456530\"\r\n" + 
				"            ],\r\n" + 
				"            \"srcPcol\": [\r\n" + 
				"              \"005\"\r\n" + 
				"            ],\r\n" + 
				"            \"srcConvDate\": [\r\n" + 
				"              \"03/31/12\"\r\n" + 
				"            ]\r\n" + 
				"          },\r\n" + 
				"          \"payAccts\": {\r\n" + 
				"            \"cardsOnfile\": [\r\n" + 
				"              null\r\n" + 
				"            ],\r\n" + 
				"            \"payOnAcctId\": [\r\n" + 
				"              \"PA043172\"\r\n" + 
				"            ]\r\n" + 
				"          },\r\n" + 
				"          \"specShipInstrs\": {\r\n" + 
				"            \"specShipInstr\": [\r\n" + 
				"              null\r\n" + 
				"            ]\r\n" + 
				"          }\r\n" + 
				"        }\r\n" + 
				"      }\r\n" + 
				"    }\r\n" + 
				"  ]\r\n" + 
				"}";
		
		return payload;
		
	}

}
