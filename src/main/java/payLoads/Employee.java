package payLoads;

public class Employee {
	
	public static String employeeData(String type, String date, Long syncId) {
		
		String payLoad = "{\r\n" + 
				"   \"docType\":\"employee\",\r\n" + 
				"   \"version\":2,\r\n" + 
				"   \"events\":[\r\n" + 
				"      {\r\n" + 
				"         \"event\":{\r\n" + 
				"            \"system\":\"DEV\",\r\n" + 
				"            \"account\":\"MASTER\",\r\n" + 
				"            \"file\":\"EMPLOYEE\",\r\n" + 
				"            \"record\":\"612954\",\r\n" + 
				"            \"author\":\"AAO8676\",\r\n" + 
				"            \"type\":\"" + type + "\",\r\n" + 
				"            \"date\":\"" + date + "\",\r\n" + 
				"            \"syncId\":\"" + syncId + "\",\r\n" + 
				"            \"data\":{\r\n" + 
				"               \"header\":{\r\n" + 
				"                  \"forwardAddr\":null,\r\n" + 
				"                  \"empName\":\"Kristopher Todd\",\r\n" + 
				"                  \"homeBranch\":\"HQ\",\r\n" + 
				"                  \"homeMachine\":\"DEV\",\r\n" + 
				"                  \"psoftEmpId\":\"2000848\",\r\n" + 
				"                  \"empInitials\":\"KJT\",\r\n" + 
				"                  \"titleCode\":\"6632\",\r\n" + 
				"                  \"titleSuffix\":null,\r\n" + 
				"                  \"empType\":null,\r\n" + 
				"                  \"payType\":null,\r\n" + 
				"                  \"emailListFlag\":null,\r\n" + 
				"                  \"payrollBranchId\":\"18\",\r\n" + 
				"                  \"payrollWhseId\":\"8969\",\r\n" + 
				"                  \"mailAcctName\":\"HQ\",\r\n" + 
				"                  \"slaveType\":null,\r\n" + 
				"                  \"emailAttchFlag\":\"2\",\r\n" + 
				"                  \"psoftDeptCode\":\"8969\",\r\n" + 
				"                  \"trilLogonId\":\"AAO8676\",\r\n" + 
				"                  \"emailAliasName\":null,\r\n" + 
				"                  \"vpnAccessFlag\":null,\r\n" + 
				"                  \"trilLog2Flag\":\"+\",\r\n" + 
				"                  \"clientlessVpnFlag\":\"Y\",\r\n" + 
				"                  \"smtpAuth\":\"GATEWAY\",\r\n" + 
				"                  \"smtpDomain\":\"ferguson.com\",\r\n" + 
				"                  \"imAccessFlag\":null,\r\n" + 
				"                  \"rightfaxId\":\"AAO8676\",\r\n" + 
				"                  \"tnlFlag\":null,\r\n" + 
				"                  \"vLastChgTd\":\"09/04/19 11:33:05AM\",\r\n" + 
				"                  \"vName1\":\"Kristopher\",\r\n" + 
				"                  \"vName2\":null,\r\n" + 
				"                  \"vName3\":\"Todd\",\r\n" + 
				"                  \"vName4\":null\r\n" + 
				"               },\r\n" + 
				"               \"auth\":{\r\n" + 
				"                  \"trilAuthCode\":[\r\n" + 
				"                     \"ALLOG\",\r\n" + 
				"                     \"TCL\",\r\n" + 
				"                     \"EMAIL\",\r\n" + 
				"                     \"POE\",\r\n" + 
				"                     \"INQ\",\r\n" + 
				"                     \"TMS\",\r\n" + 
				"                     \"FORMS\",\r\n" + 
				"                     \"AUTH\",\r\n" + 
				"                     \"AUDIT\",\r\n" + 
				"                     \"ACOM\",\r\n" + 
				"                     \"SACSUP\",\r\n" + 
				"                     \"HQPF\",\r\n" + 
				"                     \"SOX\",\r\n" + 
				"                     \"ETL\",\r\n" + 
				"                     \"CFM\",\r\n" + 
				"                     \"SAPPJOB\",\r\n" + 
				"                     \"PPOED\",\r\n" + 
				"                     \"HAZ\",\r\n" + 
				"                     \"FM\",\r\n" + 
				"                     \"VIP\",\r\n" + 
				"                     \"FM\",\r\n" + 
				"                     \"CCODE\",\r\n" + 
				"                     \"VPI\",\r\n" + 
				"                     \"PASS\",\r\n" + 
				"                     \"VFM\",\r\n" + 
				"                     \"VFDELETE\",\r\n" + 
				"                     \"AGE\",\r\n" + 
				"                     \"MCFM\",\r\n" + 
				"                     \"AP\",\r\n" + 
				"                     \"CR\",\r\n" + 
				"                     \"CMA\",\r\n" + 
				"                     \"OSCASH\",\r\n" + 
				"                     \"OSCHECK\",\r\n" + 
				"                     \"CFDELETE\",\r\n" + 
				"                     \"ACA\",\r\n" + 
				"                     \"TXEX\",\r\n" + 
				"                     \"AR\",\r\n" + 
				"                     \"UAC\",\r\n" + 
				"                     \"MATCH\",\r\n" + 
				"                     \"CD\",\r\n" + 
				"                     \"GL\",\r\n" + 
				"                     \"OE\",\r\n" + 
				"                     \"PPM\",\r\n" + 
				"                     \"RF\",\r\n" + 
				"                     \"RPT\",\r\n" + 
				"                     \"SBRE\",\r\n" + 
				"                     \"HQ\",\r\n" + 
				"                     \"ADMIN\",\r\n" + 
				"                     \"PWORD\",\r\n" + 
				"                     \"RTI\"\r\n" + 
				"                  ],\r\n" + 
				"                  \"trilAuthLevel\":[\r\n" + 
				"                     \"20\",\r\n" + 
				"                     \"20\",\r\n" + 
				"                     \"20\",\r\n" + 
				"                     \"20\",\r\n" + 
				"                     \"20\",\r\n" + 
				"                     \"20\",\r\n" + 
				"                     \"20\",\r\n" + 
				"                     \"20\",\r\n" + 
				"                     \"20\",\r\n" + 
				"                     \"20\",\r\n" + 
				"                     \"20\",\r\n" + 
				"                     \"20\",\r\n" + 
				"                     \"20\",\r\n" + 
				"                     \"20\",\r\n" + 
				"                     \"20\",\r\n" + 
				"                     \"20\",\r\n" + 
				"                     \"20\",\r\n" + 
				"                     \"20\",\r\n" + 
				"                     \"20\",\r\n" + 
				"                     \"20\",\r\n" + 
				"                     \"20\",\r\n" + 
				"                     \"20\",\r\n" + 
				"                     \"20\",\r\n" + 
				"                     \"20\",\r\n" + 
				"                     \"20\",\r\n" + 
				"                     \"20\",\r\n" + 
				"                     \"20\",\r\n" + 
				"                     \"20\",\r\n" + 
				"                     \"20\",\r\n" + 
				"                     \"20\",\r\n" + 
				"                     \"20\",\r\n" + 
				"                     \"20\",\r\n" + 
				"                     \"20\",\r\n" + 
				"                     \"20\",\r\n" + 
				"                     \"20\",\r\n" + 
				"                     \"20\",\r\n" + 
				"                     \"25\",\r\n" + 
				"                     \"20\",\r\n" + 
				"                     \"20\",\r\n" + 
				"                     \"10\",\r\n" + 
				"                     \"1\",\r\n" + 
				"                     \"4\",\r\n" + 
				"                     \"1\",\r\n" + 
				"                     \"20\",\r\n" + 
				"                     \"20\",\r\n" + 
				"                     \"20\",\r\n" + 
				"                     \"20\",\r\n" + 
				"                     \"20\",\r\n" + 
				"                     \"20\",\r\n" + 
				"                     \"15\"\r\n" + 
				"                  ]\r\n" + 
				"               },\r\n" + 
				"               \"accts\":{\r\n" + 
				"                  \"empAcct\":[\r\n" + 
				"                     \"HQMASTER\"\r\n" + 
				"                  ]\r\n" + 
				"               },\r\n" + 
				"               \"smtp\":{\r\n" + 
				"                  \"smtpUserid\":[\r\n" + 
				"                     \"kristopher.todd\"\r\n" + 
				"                  ],\r\n" + 
				"                  \"smtpGateway\":[\r\n" + 
				"                     \"TRILOGIE\"\r\n" + 
				"                  ]\r\n" + 
				"               }\r\n" + 
				"            }\r\n" + 
				"         }\r\n" + 
				"      }\r\n" + 
				"   ]\r\n" + 
				"}";
		
		return payLoad;
	}

}
