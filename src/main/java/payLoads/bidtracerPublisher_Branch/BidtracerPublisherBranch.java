package payLoads.bidtracerPublisher_Branch;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class BidtracerPublisherBranch {
	
	public static String dateTime() {
		LocalDateTime myDateObj = LocalDateTime.now();
		DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		String formattedDate = myDateObj.format(myFormatObj);

		return formattedDate;
	}
	
	public static String branchCustomer(String type, Long syncId, String jobCustFlag) {
		String payload = "{\r\n" + 
				"  \"docType\": \"customer\",\r\n" + 
				"  \"version\": 1,\r\n" + 
				"  \"events\": [\r\n" + 
				"    {\r\n" + 
				"      \"event\": {\r\n" + 
				"        \"system\": \"DEV\",\r\n" + 
				"        \"account\": \"GAUG\",\r\n" + 
				"        \"file\": \"CUSTOMER\",\r\n" + 
				"        \"record\": \"24000\",\r\n" + 
				"        \"author\": \"AAO8676\",\r\n" + 
				"        \"type\": \""+type+"\",\r\n" + 
				"        \"date\": \""+dateTime()+"\",\r\n" + 
				"        \"syncId\":\""+ syncId +"\",\r\n" + 
				"        \"data\": {\r\n" + 
				"          \"header\": {\r\n" + 
				"            \"custName\": \"KEATING BROTHERS CONST CO\",\r\n" + 
				"            \"custAddr1\": \"4225 JVL INDUSTRIAL PARK\",\r\n" + 
				"            \"custAddr2\": \"MARIETTA, GA 30066\",\r\n" + 
				"            \"custAddr3\": null,\r\n" + 
				"            \"custAddr4\": null,\r\n" + 
				"            \"custZip\": \"30066\",\r\n" + 
				"            \"contactName\": null,\r\n" + 
				"            \"custPhone\": \"7704218799\",\r\n" + 
				"            \"shipViaCode\": null,\r\n" + 
				"            \"shipInstr\": null,\r\n" + 
				"            \"custTerms\": \"COD\",\r\n" + 
				"            \"creditCode\": null,\r\n" + 
				"            \"creditLimitAmt\": 10000,\r\n" + 
				"            \"custType\": \"U_UTLTYRES\",\r\n" + 
				"            \"slsmCode\": \"015\",\r\n" + 
				"            \"taxJur\": \"GA033\",\r\n" + 
				"            \"custSinceMths\": 144,\r\n" + 
				"            \"taxExemptNum\": null,\r\n" + 
				"            \"latePayPct\": 0,\r\n" + 
				"            \"blindBillFlag\": null,\r\n" + 
				"            \"reprintNumCopies\": 1,\r\n" + 
				"            \"lastSaleDate\": \"06/26/12\",\r\n" + 
				"            \"lastRcptDate\": \"05/24/12\",\r\n" + 
				"            \"arBalAmt\": 4704.63,\r\n" + 
				"            \"openOrdBalAmt\": null,\r\n" + 
				"            \"slsMtdAmt\": null,\r\n" + 
				"            \"slsYtdAmt\": 10762.59,\r\n" + 
				"            \"returnsMtdAmt\": null,\r\n" + 
				"            \"returnsYtdAmt\": 217.77,\r\n" + 
				"            \"gpMtdAmt\": null,\r\n" + 
				"            \"gpYtdAmt\": 3291.57,\r\n" + 
				"            \"noBumpFlag\": null,\r\n" + 
				"            \"rstrAmt\": null,\r\n" + 
				"            \"prtListdiscFlag\": \"N\",\r\n" + 
				"            \"priceClassCode\": \"143\",\r\n" + 
				"            \"creditRatingCode\": null,\r\n" + 
				"            \"cutoffCode\": \"25\",\r\n" + 
				"            \"futureInvAmt\": 78.73,\r\n" + 
				"            \"currInvAmt\": 4300.77,\r\n" + 
				"            \"over30Amt\": 325.13,\r\n" + 
				"            \"over60Amt\": null,\r\n" + 
				"            \"over90Amt\": null,\r\n" + 
				"            \"smallCrgFlag\": \"N\",\r\n" + 
				"            \"alphaSortCode\": \"KEATINGBRO\",\r\n" + 
				"            \"custAlpha\": \"KEATINGBRO\",\r\n" + 
				"            \"custBranchId\": \"554\",\r\n" + 
				"            \"territory\": null,\r\n" + 
				"            \"slsGlId\": null,\r\n" + 
				"            \"arGlId\": \"1300\",\r\n" + 
				"            \"dlvChargeAmt\": 0,\r\n" + 
				"            \"jobNameReqrdFlag\": \"Y\",\r\n" + 
				"            \"boAcceptFlag\": \"0\",\r\n" + 
				"            \"fincContactName\": null,\r\n" + 
				"            \"reqrdCustPoFlag\": \"N\",\r\n" + 
				"            \"unprocArAmt\": 54.74,\r\n" + 
				"            \"corpName\": \"COUPON APR2006\",\r\n" + 
				"            \"dunsNum\": \"14-850-3522\",\r\n" + 
				"            \"pmtMtdAmt\": null,\r\n" + 
				"            \"custStatus\": \"A\",\r\n" + 
				"            \"printPriceFlag\": \"N\",\r\n" + 
				"            \"dontforgetComment\": \"FORMAN'S NAME IN PO FIELD\",\r\n" + 
				"            \"mainCustId\": null,\r\n" + 
				"            \"custPoPattern\": null,\r\n" + 
				"            \"jobCustFlag\": \"" + jobCustFlag + "\",\r\n" + 
				"            \"custCreateDate\": \"07/06/00\",\r\n" + 
				"            \"altSlsmCode\": null,\r\n" + 
				"            \"crLimStatus\": null,\r\n" + 
				"            \"crLimExpDate\": null,\r\n" + 
				"            \"crLimLastchgDate\": \"05/11/07\",\r\n" + 
				"            \"guaranteeFlag\": \"N\",\r\n" + 
				"            \"fincStmtLastDate\": null,\r\n" + 
				"            \"grossSlsAmt\": null,\r\n" + 
				"            \"netWrkgCptlAmt\": null,\r\n" + 
				"            \"netWorthAmt\": null,\r\n" + 
				"            \"custEstbDate\": \"01/01/87\",\r\n" + 
				"            \"lastDnbRptDate\": \"09/17/03\",\r\n" + 
				"            \"dnbRating\": \"1R2\",\r\n" + 
				"            \"creditRegion\": \"01\",\r\n" + 
				"            \"ctrlBranchId\": \"554\",\r\n" + 
				"            \"creditAppOnfileFlag\": \"Y\",\r\n" + 
				"            \"noAutoCostFlag\": null,\r\n" + 
				"            \"reserveForId\": null,\r\n" + 
				"            \"reserveById\": null,\r\n" + 
				"            \"ediPartner\": null,\r\n" + 
				"            \"printEdiOrdFlag\": \"H\",\r\n" + 
				"            \"ediPriceDiscrpFlag\": null,\r\n" + 
				"            \"creditMgrEmpId\": \"78318\",\r\n" + 
				"            \"rcptSortType\": \"D\",\r\n" + 
				"            \"rstrRsnCode\": null,\r\n" + 
				"            \"custFaxNum\": \"7704218796\",\r\n" + 
				"            \"asnReqrdFlag\": null,\r\n" + 
				"            \"shipCompleteFlag\": \"N\",\r\n" + 
				"            \"priceAdjustId\": null,\r\n" + 
				"            \"creditScore\": \"ECREDIT\",\r\n" + 
				"            \"printSortFlag\": \"N\",\r\n" + 
				"            \"claimbackFlag\": null,\r\n" + 
				"            \"stmtSeq\": null,\r\n" + 
				"            \"faxConfCode\": null,\r\n" + 
				"            \"pricePrecision\": null,\r\n" + 
				"            \"futurePayAmt\": null,\r\n" + 
				"            \"assocInfo\": \"N\",\r\n" + 
				"            \"labelInfo\": \"N\",\r\n" + 
				"            \"supplierNum\": null,\r\n" + 
				"            \"consignBal\": null,\r\n" + 
				"            \"prepDays\": null,\r\n" + 
				"            \"reviewFrtDlvFlag\": null,\r\n" + 
				"            \"custSeq\": null,\r\n" + 
				"            \"expNocntrtFlag\": null,\r\n" + 
				"            \"expCntrtFlag\": null,\r\n" + 
				"            \"reqrdOrdByFlag\": null,\r\n" + 
				"            \"useFrtTableFlag\": null,\r\n" + 
				"            \"mcustId\": \"164963\",\r\n" + 
				"            \"mcustChgDate\": \"08/27/09\",\r\n" + 
				"            \"mcustChgByEmpId\": \"OPER\",\r\n" + 
				"            \"processStatus\": \"PROC\",\r\n" + 
				"            \"processDate\": \"09/17/06\",\r\n" + 
				"            \"processByEmpId\": \"50719\",\r\n" + 
				"            \"shipInstr2\": null,\r\n" + 
				"            \"shipInstr3\": null,\r\n" + 
				"            \"shipInstr4\": null,\r\n" + 
				"            \"shipToAttn\": null,\r\n" + 
				"            \"shipToPhone\": null,\r\n" + 
				"            \"prtExtPriceFlag\": null,\r\n" + 
				"            \"rndType\": null,\r\n" + 
				"            \"rndNearDec\": null,\r\n" + 
				"            \"rndMinAmt\": null,\r\n" + 
				"            \"rndCntrtFlag\": null,\r\n" + 
				"            \"countryCode\": \"US\",\r\n" + 
				"            \"ccRptFlag\": null,\r\n" + 
				"            \"countyName\": \"COBB\",\r\n" + 
				"            \"gsaLink\": \"N\",\r\n" + 
				"            \"geocode\": \"110670790\",\r\n" + 
				"            \"auditReqrdFlag\": null,\r\n" + 
				"            \"verifyCheckFlag\": null,\r\n" + 
				"            \"bestPriceCode\": null,\r\n" + 
				"            \"ecreditArBal\": 4704.63,\r\n" + 
				"            \"ecreditArFlag\": \"Y\",\r\n" + 
				"            \"ecreditArCurrBal\": 4300.77,\r\n" + 
				"            \"ecreditAr30Bal\": 325.13,\r\n" + 
				"            \"ecreditAr60Bal\": null,\r\n" + 
				"            \"ecreditAr90Bal\": null,\r\n" + 
				"            \"dealerInfo\": null,\r\n" + 
				"            \"dealers\": null,\r\n" + 
				"            \"showPriceFlag\": null,\r\n" + 
				"            \"ecreditArFutureBal\": 78.73,\r\n" + 
				"            \"b2bCustFlag\": null,\r\n" + 
				"            \"frtBumpFlag\": null,\r\n" + 
				"            \"autoPodFlag\": null,\r\n" + 
				"            \"scndSlsm\": null,\r\n" + 
				"            \"commPct\": null,\r\n" + 
				"            \"referralId\": null,\r\n" + 
				"            \"prtCommentsType\": null,\r\n" + 
				"            \"jobNamePattern\": null,\r\n" + 
				"            \"emailConfCode\": null,\r\n" + 
				"            \"bmiBudCustType\": null,\r\n" + 
				"            \"bmiRptCustType\": null,\r\n" + 
				"            \"ldlMdm\": null,\r\n" + 
				"            \"printMstrProdFlag\": null,\r\n" + 
				"            \"specialtySalesman\": null,\r\n" + 
				"            \"sourceBidNum\": null,\r\n" + 
				"            \"showCustOnlineFlag\": null,\r\n" + 
				"            \"webHoldCtrl\": null,\r\n" + 
				"            \"tcWhse\": null,\r\n" + 
				"            \"tcSlsm\": null,\r\n" + 
				"            \"rEmpNum\": \"45063\",\r\n" + 
				"            \"vCrossCustAcct\": null,\r\n" + 
				"            \"vCrossCustFlag\": null,\r\n" + 
				"            \"vCrossCustId\": null,\r\n" + 
				"            \"vGenrInv2Code\": null,\r\n" + 
				"            \"vGenrInvCode\": \"P\",\r\n" + 
				"            \"vGenrStmt2Code\": null,\r\n" + 
				"            \"vGenrStmtCode\": \"F\",\r\n" + 
				"            \"vPrtNegInvFlag\": \"Y\",\r\n" + 
				"            \"vPrtStmtFlag\": \"F\",\r\n" + 
				"            \"vPrtStmtNegFlag\": \"N\",\r\n" + 
				"            \"vPrtStmtNoactFlag\": \"N\",\r\n" + 
				"            \"vPrtStmtZeroFlag\": \"N\",\r\n" + 
				"            \"vPrtZeroInvFlag\": \"Y\"\r\n" + 
				"          },\r\n" + 
				"          \"bumps\": {\r\n" + 
				"            \"rankBump\": [\r\n" + 
				"              null\r\n" + 
				"            ]\r\n" + 
				"          },\r\n" + 
				"          \"shipTo\": {\r\n" + 
				"            \"shipToSuffix\": [\r\n" + 
				"              \"0000\",\r\n" + 
				"              \"0001\"\r\n" + 
				"            ]\r\n" + 
				"          },\r\n" + 
				"          \"ttlByMth\": {\r\n" + 
				"            \"roll12SlsAmt\": [\r\n" + 
				"              4131.6,\r\n" + 
				"              289.38,\r\n" + 
				"              916.27,\r\n" + 
				"              382.8,\r\n" + 
				"              null,\r\n" + 
				"              11.47,\r\n" + 
				"              55,\r\n" + 
				"              1854.68,\r\n" + 
				"              2198.04,\r\n" + 
				"              923.35,\r\n" + 
				"              null,\r\n" + 
				"              null\r\n" + 
				"            ],\r\n" + 
				"            \"roll12GpAmt\": [\r\n" + 
				"              1300.24,\r\n" + 
				"              83.05,\r\n" + 
				"              171.65,\r\n" + 
				"              147.24,\r\n" + 
				"              null,\r\n" + 
				"              5.8,\r\n" + 
				"              29.47,\r\n" + 
				"              517.42,\r\n" + 
				"              704.93,\r\n" + 
				"              331.77,\r\n" + 
				"              null,\r\n" + 
				"              null\r\n" + 
				"            ]\r\n" + 
				"          },\r\n" + 
				"          \"cntrts\": {\r\n" + 
				"            \"contractId\": [\r\n" + 
				"              null\r\n" + 
				"            ],\r\n" + 
				"            \"ovrWhseId\": [\r\n" + 
				"              null\r\n" + 
				"            ],\r\n" + 
				"            \"exprInfo\": [\r\n" + 
				"              null\r\n" + 
				"            ],\r\n" + 
				"            \"lastReviewInfo\": [\r\n" + 
				"              \"15606\"\r\n" + 
				"            ],\r\n" + 
				"            \"marketFundFlag\": [\r\n" + 
				"              null\r\n" + 
				"            ],\r\n" + 
				"            \"tripPointsFlag\": [\r\n" + 
				"              null\r\n" + 
				"            ],\r\n" + 
				"            \"quoteId\": [\r\n" + 
				"              null\r\n" + 
				"            ],\r\n" + 
				"            \"claimbackCustId\": [\r\n" + 
				"              null\r\n" + 
				"            ],\r\n" + 
				"            \"contractName\": [\r\n" + 
				"              null\r\n" + 
				"            ],\r\n" + 
				"            \"restrictContract\": [\r\n" + 
				"              null\r\n" + 
				"            ],\r\n" + 
				"            \"vCntrtExpDate\": [\r\n" + 
				"              \"04/05/11\"\r\n" + 
				"            ],\r\n" + 
				"            \"vCntrtExpText\": [\r\n" + 
				"              null\r\n" + 
				"            ]\r\n" + 
				"          },\r\n" + 
				"          \"jobs\": {\r\n" + 
				"            \"jobCustId\": [\r\n" + 
				"              \"32136\",\r\n" + 
				"              \"38798\"\r\n" + 
				"            ]\r\n" + 
				"          },\r\n" + 
				"          \"emails\": {\r\n" + 
				"            \"ediNotifyEmpId\": [\r\n" + 
				"              null\r\n" + 
				"            ],\r\n" + 
				"            \"omlWhseId\": [\r\n" + 
				"              null\r\n" + 
				"            ],\r\n" + 
				"            \"emailNotifyEmpId\": [\r\n" + 
				"              null\r\n" + 
				"            ],\r\n" + 
				"            \"emailNotifyWhseId\": [\r\n" + 
				"              null\r\n" + 
				"            ],\r\n" + 
				"            \"emailNotifyEmpName\": [\r\n" + 
				"              null\r\n" + 
				"            ]\r\n" + 
				"          },\r\n" + 
				"          \"audits\": {\r\n" + 
				"            \"rcptCashIrsTrackAmt\": [\r\n" + 
				"              null\r\n" + 
				"            ],\r\n" + 
				"            \"crLineAudit\": [\r\n" + 
				"              [\r\n" + 
				"                \"14376\",\r\n" + 
				"                \"3000\",\r\n" + 
				"                \"93705\"\r\n" + 
				"              ]\r\n" + 
				"            ]\r\n" + 
				"          },\r\n" + 
				"          \"rstrs\": {\r\n" + 
				"            \"restrictCode\": [\r\n" + 
				"              null\r\n" + 
				"            ],\r\n" + 
				"            \"restrictCodeDate\": [\r\n" + 
				"              null\r\n" + 
				"            ],\r\n" + 
				"            \"restrictCodeNotes\": [\r\n" + 
				"              null\r\n" + 
				"            ],\r\n" + 
				"            \"restrictByEmpId\": [\r\n" + 
				"              null\r\n" + 
				"            ]\r\n" + 
				"          },\r\n" + 
				"          \"buyers\": {\r\n" + 
				"            \"buyerName\": [\r\n" + 
				"              null\r\n" + 
				"            ],\r\n" + 
				"            \"buyerTitle\": [\r\n" + 
				"              null\r\n" + 
				"            ]\r\n" + 
				"          },\r\n" + 
				"          \"dtpds\": {\r\n" + 
				"            \"dtpdWhseId\": [\r\n" + 
				"              null\r\n" + 
				"            ],\r\n" + 
				"            \"dtpdPrepDays\": [\r\n" + 
				"              null\r\n" + 
				"            ]\r\n" + 
				"          },\r\n" + 
				"          \"stmts\": {\r\n" + 
				"            \"prtInvOptFlag\": [\r\n" + 
				"              null\r\n" + 
				"            ],\r\n" + 
				"            \"prtDlvOptFlag\": [\r\n" + 
				"              null\r\n" + 
				"            ]\r\n" + 
				"          },\r\n" + 
				"          \"sources\": {\r\n" + 
				"            \"srcMain\": [\r\n" + 
				"              \"480\"\r\n" + 
				"            ],\r\n" + 
				"            \"srcAccount\": [\r\n" + 
				"              \"LUBBOCK\"\r\n" + 
				"            ],\r\n" + 
				"            \"srcCust\": [\r\n" + 
				"              \"66853\"\r\n" + 
				"            ],\r\n" + 
				"            \"srcPcol\": [\r\n" + 
				"              \"020\"\r\n" + 
				"            ],\r\n" + 
				"            \"srcConvDate\": [\r\n" + 
				"              \"07/10/13\"\r\n" + 
				"            ]\r\n" + 
				"          },\r\n" + 
				"          \"payAccts\": {\r\n" + 
				"            \"cardsOnfile\": [\r\n" + 
				"              null\r\n" + 
				"            ],\r\n" + 
				"            \"payOnAcctId\": [\r\n" + 
				"              \"PA012357\"\r\n" + 
				"            ]\r\n" + 
				"          },\r\n" + 
				"          \"specShipInstrs\": {\r\n" + 
				"            \"specShipInstr\": [\r\n" + 
				"              null\r\n" + 
				"            ]\r\n" + 
				"          }\r\n" + 
				"        }\r\n" + 
				"      }\r\n" + 
				"    }\r\n" + 
				"  ]\r\n" + 
				"}";
		
		return payload;
	}

}
