package payLoads;

public class Branch {
	
	public static String branchData(String type, String date, Long syncId) {
		
		String payload = "{\r\n" + 
				"  \"docType\": \"branch\",\r\n" + 
				"  \"version\": 2,\r\n" + 
				"  \"events\": [\r\n" + 
				"    {\r\n" + 
				"      \"event\": {\r\n" + 
				"        \"system\": \"DEV\",\r\n" + 
				"        \"account\": \"OHVAL\",\r\n" + 
				"        \"file\": \"BRANCH\",\r\n" + 
				"        \"record\": \"1119\",\r\n" + 
				"        \"author\": \"AAO8676\",\r\n" + 
				"        \"type\": \""+ type +"\",\r\n" + 
				"        \"date\": \""+date+"\",\r\n" + 
				"        \"syncId\":\""+syncId+"\",\r\n" + 
				"        \"data\": {\r\n" + 
				"          \"header\": {\r\n" + 
				"            \"remitToName\": \"FERGUSON ENTERPRISES INC #1119\",\r\n" + 
				"            \"remitToAddr1\": \"REMIT TO:\",\r\n" + 
				"            \"remitToAddr2\": \"PO BOX 100286\",\r\n" + 
				"            \"remitToAddr3\": \"ATLANTA, GA 30384-0286\",\r\n" + 
				"            \"shipToName\": \"FERGUSON ENTERPRISES #1119\",\r\n" + 
				"            \"shipToAddr1\": \"12085 METRO PARKWAY\",\r\n" + 
				"            \"shipToAddr2\": \"FT MYERS, FL 33966-0000\",\r\n" + 
				"            \"shipToAddr3\": null,\r\n" + 
				"            \"taxJur\": \"FL2LEE\",\r\n" + 
				"            \"freeOnBoardCode\": null,\r\n" + 
				"            \"cashSalesAcctId\": \"222\",\r\n" + 
				"            \"ibOpenPoAmt\": 45.96,\r\n" + 
				"            \"ibOpenOrderAmt\": \"-597,683.20\",\r\n" + 
				"            \"prefixLetter\": \"S\",\r\n" + 
				"            \"purAgentName\": \"BOB LLOYD\",\r\n" + 
				"            \"remitToPhone\": \"239-332-3072\",\r\n" + 
				"            \"remitToFax\": \"239-332-8340\",\r\n" + 
				"            \"shipToPhone\": \"239-332-3072\",\r\n" + 
				"            \"shipToFax\": \"239-332-8340\",\r\n" + 
				"            \"billToName\": \"FERGUSON ENTERPRISES #1119\",\r\n" + 
				"            \"billToCity\": \"HAMPTON\",\r\n" + 
				"            \"billToState\": \"VA\",\r\n" + 
				"            \"billToZip\": \"23670-0406\",\r\n" + 
				"            \"billToPhone\": \"239-332-4242\",\r\n" + 
				"            \"billToFax\": \"239-332-7709\",\r\n" + 
				"            \"remitBarcode\": \"862\",\r\n" + 
				"            \"remitToZip4\": \"303840286\",\r\n" + 
				"            \"proofOfDlvFlag\": \"N\",\r\n" + 
				"            \"billToCountry\": \"US\",\r\n" + 
				"            \"remitToCountry\": \"US\",\r\n" + 
				"            \"shipToCountry\": \"US\",\r\n" + 
				"            \"billToCounty\": null,\r\n" + 
				"            \"remitToCounty\": null,\r\n" + 
				"            \"shipToCounty\": \"LEE\",\r\n" + 
				"            \"taxGeocode\": \"100710470\",\r\n" + 
				"            \"ecreditKob\": \"Blended Branch\",\r\n" + 
				"            \"autoFrsType\": null,\r\n" + 
				"            \"arSecureMsgFlag\": null,\r\n" + 
				"            \"shwrmInbndFrtFlag\": \"Y\",\r\n" + 
				"            \"nonshwrmInbndFrtFlag\": \"N\",\r\n" + 
				"            \"omlContactId\": null,\r\n" + 
				"            \"omlContactName\": null,\r\n" + 
				"            \"vBillToAddr1\": \"PO BOX 9406\",\r\n" + 
				"            \"vBillToAddr2\": null\r\n" + 
				"          },\r\n" + 
				"          \"dispWhses\": {\r\n" + 
				"            \"displayWhse\": [\"761\", \"533\", \"423\", \"986\", \"208\", \"625\", \"474\", \"2528\"]\r\n" + 
				"          },\r\n" + 
				"          \"sellWhses\": {\r\n" + 
				"            \"sellWhse\": [null],\r\n" + 
				"            \"transitDays\": [null]\r\n" + 
				"          },\r\n" + 
				"          \"printInfo\": {\r\n" + 
				"            \"dlvTktFormId\": [\"DT0201\"],\r\n" + 
				"            \"showBoFlag\": [\"N\"],\r\n" + 
				"            \"printerQueId\": [\"1119\"]\r\n" + 
				"          },\r\n" + 
				"          \"kobs\": {\r\n" + 
				"            \"kobName\": [\"TEST6\"]\r\n" + 
				"          }\r\n" + 
				"        }\r\n" + 
				"      }\r\n" + 
				"    }\r\n" + 
				"  ]\r\n" + 
				"}";
		
		return payload;
		
	}

}
