package payLoads;

public class Carrier_XML {
	
	public static String carrierData() {
		
		String payload = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" + 
				"<freightCostWarehouseRequest>\n" + 
				"   <preferredCarrier>AAA</preferredCarrier>\n" + 
				"   <parcelType />\n" + 
				"   <maxCosts>3</maxCosts>\n" + 
				"   <originAddress>\n" + 
				"      <warehouseId />\n" + 
				"      <city>Pooler</city>\n" + 
				"      <state>GA</state>\n" + 
				"      <zipCode>31322</zipCode>\n" + 
				"      <countryCode>USA</countryCode>\n" + 
				"   </originAddress>\n" + 
				"   <destinationAddress>\n" + 
				"      <city>DALLAS</city>\n" + 
				"      <state>TX</state>\n" + 
				"      <zipCode>75252</zipCode>\n" + 
				"      <countryCode>USA</countryCode>\n" + 
				"   </destinationAddress>\n" + 
				"   <products>\n" + 
				"      <product>\n" + 
				"         <id>367233</id>\n" + 
				"         <qty>8</qty>\n" + 
				"         <dimLength>45</dimLength>\n" + 
				"         <dimWidth>9</dimWidth>\n" + 
				"         <dimHeight>47</dimHeight>\n" + 
				"         <units>in</units>\n" + 
				"      </product>\n" + 
				"      <product>\n" + 
				"         <id>865426</id>\n" + 
				"         <qty>6</qty>\n" + 
				"         <dimLength />\n" + 
				"         <dimWidth />\n" + 
				"         <dimHeight>47</dimHeight>\n" + 
				"         <units>in</units>\n" + 
				"      </product>\n" + 
				"   </products>\n" + 
				"</freightCostWarehouseRequest>";
		
		return payload;
	}

}
