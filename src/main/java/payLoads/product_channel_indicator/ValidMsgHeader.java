package payLoads.product_channel_indicator;

public class ValidMsgHeader {
	
	public static String payLoad_One() {
		String payload = "{\r\n" + 
				"   \"ItemIDs\":[\r\n" + 
				"      {\r\n" + 
				"         \"ItemId\":\"C345675\",\r\n" + 
				"         \"Extended\":{\r\n" + 
				"            \"IncludedInBuild\":\"B\",\r\n" + 
				"            \"ExcludedPrevInclBuild\":\"B\"\r\n" + 
				"         }\r\n" + 
				"      }\r\n" + 
				"   ]\r\n" + 
				"}";
		
		return payload;
	}
	
	public static String payLoad_Two() {
		String payload = "{\r\n" + 
				"   \"ItemIDs\":[\r\n" + 
				"      {\r\n" + 
				"         \"ItemId\":\"C345675\",\r\n" + 
				"         \"Extended\":{\r\n" + 
				"            \"IncludedInSupply\":\"S,N\",\r\n" + 
				"            \"ExcludedPrevInclSupply\":\"S\"\r\n" + 
				"         }\r\n" + 
				"      }\r\n" + 
				"   ]\r\n" + 
				"}";
		
		return payload;
	}
	
	public static String payload_Three() {
		String payload = "{\r\n" + 
				"   \"ItemIDs\":[\r\n" + 
				"      {\r\n" + 
				"         \"ItemId\":\"C345675\",\r\n" + 
				"         \"Extended\":{\r\n" + 
				"            \"IncludedInHMW\":\"H\",\r\n" + 
				"            \"ExcludedPrevInclHMW\":\"H\"\r\n" + 
				"         }\r\n" + 
				"      }\r\n" + 
				"   ]\r\n" + 
				"}";
		
		return payload;
	}
}
