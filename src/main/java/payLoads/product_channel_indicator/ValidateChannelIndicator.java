package payLoads.product_channel_indicator;

public class ValidateChannelIndicator {
	
	public static String validateChannel() {
		String payload = "{\r\n" + 
				"  \"ItemIDs\": [\r\n" + 
				"    {\r\n" + 
				"      \"ItemId\": \"C345350\",\r\n" + 
				"      \"Extended\": {\r\n" + 
				"        \"IncludedInSupply\": \"A,B\",\r\n" + 
				"        \"ExcludedPrevInclSupply\": \"C\"\r\n" + 
				"      }\r\n" + 
				"    },\r\n" + 
				"    {\r\n" + 
				"      \"ItemId\": \"C345351\",\r\n" + 
				"      \"Extended\": {\r\n" + 
				"        \"IncludedInBuild\": \"D\",\r\n" + 
				"        \"ExcludedPrevInclBuild\": \"E\"\r\n" + 
				"      }\r\n" + 
				"    },\r\n" + 
				"    {\r\n" + 
				"      \"ItemId\": \"C345352\",\r\n" + 
				"      \"Extended\": {\r\n" + 
				"        \"IncludedInHMW\": \"F\",\r\n" + 
				"        \"ExcludedPrevInclHMW\": \"G\"\r\n" + 
				"      }\r\n" + 
				"    },\r\n" + 
				"    {\r\n" + 
				"      \"ItemId\": \"C345353\",\r\n" + 
				"      \"Extended\": {\r\n" + 
				"        \"IncludedInSupply\": \"H,I\",\r\n" + 
				"        \"ExcludedPrevInclSupply\": \"J\",\r\n" + 
				"        \"IncludedInHMW\": \"K\",\r\n" + 
				"        \"ExcludedPrevInclHMW\": \"L\"\r\n" + 
				"      }\r\n" + 
				"    }\r\n" + 
				"  ]\r\n" + 
				"}";
		
		return payload;
	}
}
