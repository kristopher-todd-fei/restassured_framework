package payLoads.product_channel_indicator;

public class SizeOfChannelIndicator {
	
	public static String lessThan100() {
		String payload = "{\r\n" + 
				"  \"ItemIDs\": [\r\n" + 
				"    {\r\n" + 
				"      \"ItemId\": \"C345319\",\r\n" + 
				"      \"Extended\": {\r\n" + 
				"        \"IncludedInSupply\": \"S,N\",\r\n" + 
				"        \"ExcludedPrevInclSupply\": \"S\",\r\n" + 
				"        \"IncludedInHMW\": \"H\",\r\n" + 
				"        \"ExcludedPrevInclHMW\": \"H\"\r\n" + 
				"      }\r\n" + 
				"    },\r\n" + 
				"    {\r\n" + 
				"      \"ItemId\": \"C345320\",\r\n" + 
				"      \"Extended\": {\r\n" + 
				"        \"IncludedInSupply\": \"S,N\",\r\n" + 
				"        \"ExcludedPrevInclSupply\": \"S\"\r\n" + 
				"      }\r\n" + 
				"    },\r\n" + 
				"    {\r\n" + 
				"      \"ItemId\": \"C345321\",\r\n" + 
				"      \"Extended\": {\r\n" + 
				"        \"IncludedInHMW\": \"H\",\r\n" + 
				"        \"ExcludedPrevInclHMW\": \"H\"\r\n" + 
				"      }\r\n" + 
				"    },\r\n" + 
				"    {\r\n" + 
				"      \"ItemId\": \"C345322\",\r\n" + 
				"      \"Extended\": {\r\n" + 
				"        \"IncludedInBuild\": \"B\",\r\n" + 
				"        \"ExcludedPrevInclBuild\": \"B\"\r\n" + 
				"      }\r\n" + 
				"    },\r\n" + 
				"    {\r\n" + 
				"      \"ItemId\": \"C345323\",\r\n" + 
				"      \"Extended\": {\r\n" + 
				"        \"IncludedInSupply\": \"S,N\",\r\n" + 
				"        \"ExcludedPrevInclSupply\": \"N\"\r\n" + 
				"      }\r\n" + 
				"    }\r\n" + 
				"  ]\r\n" + 
				"}";
		
		return payload;
	}
	
	public static String greaterThan100() {
		String payload = "{\r\n" + 
				"  \"ItemIDs\": [\r\n" + 
				"    {\r\n" + 
				"      \"ItemId\": \"C345335\",\r\n" + 
				"      \"Extended\": {\r\n" + 
				"        \"IncludedInSupply\": \"S,N,DM,JK,DGH,DGJU,SGH,HJ,SHJIK,SG,SJH,DGH,GH,SHK,SHI,SYIU,SYUI,AS,AS,DF,GH,GT,R,GTR,TW,WFD,DRG,DF,SF,SF,WSF,SGFG,DGC,DRT,ETC,ESRG,SDFT,WE,ERT,SW,RT,YU,WH,WM,RY,EN,UI,FV,UL,BH,DZ,ACX,VB,V,MK,HY,TY,NJ,OP,TRT,DF,DG,JH,ET,YU,LI,CB,WE,TR,GB,SF,SF,AF,AG,BC,HGF,EDG,H,EWR,CS,CV,BC,XZVC,BVC,ZX,SC,Z,SS,AC,VXC,AX,ZCX,VX,ADS,FBC,AD,QD,VC,ZXC,SAD,D,X,ZV,ZCSA,WD,ZVXZ,WE,DVQ,CX,ZCX,EF,CVDXV,VS,WSF,CX,AF,SS,AWD,AOS,WED,ERT\",\r\n" + 
				"        \"ExcludedPrevInclSupply\": \"P\",\r\n" + 
				"        \"IncludedInHMW\": \"H\",\r\n" + 
				"        \"ExcludedPrevInclHMW\": \"S\"\r\n" + 
				"      }\r\n" + 
				"    },\r\n" + 
				"    {\r\n" + 
				"      \"ItemId\": \"C345336\",\r\n" + 
				"      \"Extended\": {\r\n" + 
				"        \"IncludedInSupply\": \"S,N,DM,JK,DGH,DGJU,SGH,HJ,SHJIK,SG,SJH,DGH,GH,SHK,SHI,SYIU,SYUI,AS,AS,DF,GH,GT,R,GTR,TW,WFD,DRG,DF,SF,SF,WSF,SGFG,DGC,DRT,ETC,ESRG,SDFT,WE,ERT,SW,RT,YU,WH,WM,RY,EN,UI,FV,UL,BH,DZ,ACX,VB,V,MK,HY,TY,NJ,OP,TRT,DF,DG,JH,ET,YU,LI,CB,WE,TR,GB,SF,SF,AF,AG,BC,HGF,EDG,H,EWR,CS,CV,BC,XZVC,BVC,ZX,SC,Z,SS,AC,VXC,AX,ZCX,VX,ADS,FBC,AD,QD,VC,ZXC,SAD,D,X,ZV,ZCSA,WD,ZVXZ,WE,DVQ,CX,ZCX,EF,CVDXV,VS,WSF,CX,AF,SS,AWD,AOS,WED,ERT\",\r\n" + 
				"        \"ExcludedPrevInclSupply\": \"V\"\r\n" + 
				"      }\r\n" + 
				"    },\r\n" + 
				"    {\r\n" + 
				"      \"ItemId\": \"C345337\",\r\n" + 
				"      \"Extended\": {\r\n" + 
				"        \"IncludedInHMW\": \"S,N,DM,JK,DGH,DGJU,SGH,HJ,SHJIK,SG,SJH,DGH,GH,SHK,SHI,SYIU,SYUI,AS,AS,DF,GH,GT,R,GTR,TW,WFD,DRG,DF,SF,SF,WSF,SGFG,DGC,DRT,ETC,ESRG,SDFT,WE,ERT,SW,RT,YU,WH,WM,RY,EN,UI,FV,UL,BH,DZ,ACX,VB,V,MK,HY,TY,NJ,OP,TRT,DF,DG,JH,ET,YU,LI,CB,WE,TR,GB,SF,SF,AF,AG,BC,HGF,EDG,H,EWR,CS,CV,BC,XZVC,BVC,ZX,SC,Z,SS,AC,VXC,AX,ZCX,VX,ADS,FBC,AD,QD,VC,ZXC,SAD,D,X,ZV,ZCSA,WD,ZVXZ,WE,DVQ,CX,ZCX,EF,CVDXV,VS,WSF,CX,AF,SS,AWD,AOS,WED,ERT\",\r\n" + 
				"        \"ExcludedPrevInclHMW\": \"L\"\r\n" + 
				"      }\r\n" + 
				"    },\r\n" + 
				"    {\r\n" + 
				"      \"ItemId\": \"C345338\",\r\n" + 
				"      \"Extended\": {\r\n" + 
				"        \"IncludedInBuild\": \"S,N,DM,JK,DGH,DGJU,SGH,HJ,SHJIK,SG,SJH,DGH,GH,SHK,SHI,SYIU,SYUI,AS,AS,DF,GH,GT,R,GTR,TW,WFD,DRG,DF,SF,SF,WSF,SGFG,DGC,DRT,ETC,ESRG,SDFT,WE,ERT,SW,RT,YU,WH,WM,RY,EN,UI,FV,UL,BH,DZ,ACX,VB,V,MK,HY,TY,NJ,OP,TRT,DF,DG,JH,ET,YU,LI,CB,WE,TR,GB,SF,SF,AF,AG,BC,HGF,EDG,H,EWR,CS,CV,BC,XZVC,BVC,ZX,SC,Z,SS,AC,VXC,AX,ZCX,VX,ADS,FBC,AD,QD,VC,ZXC,SAD,D,X,ZV,ZCSA,WD,ZVXZ,WE,DVQ,CX,ZCX,EF,CVDXV,VS,WSF,CX,AF,SS,AWD,AOS,WED,ERT\",\r\n" + 
				"        \"ExcludedPrevInclBuild\": \"M\"\r\n" + 
				"      }\r\n" + 
				"    },\r\n" + 
				"    {\r\n" + 
				"      \"ItemId\": \"C345339\",\r\n" + 
				"      \"Extended\": {\r\n" + 
				"        \"IncludedInSupply\": \"S,N,DM,JK,DGH,DGJU,SGH,HJ,SHJIK,SG,SJH,DGH,GH,SHK,SHI,SYIU,SYUI,AS,AS,DF,GH,GT,R,GTR,TW,WFD,DRG,DF,SF,SF,WSF,SGFG,DGC,DRT,ETC,ESRG,SDFT,WE,ERT,SW,RT,YU,WH,WM,RY,EN,UI,FV,UL,BH,DZ,ACX,VB,V,MK,HY,TY,NJ,OP,TRT,DF,DG,JH,ET,YU,LI,CB,WE,TR,GB,SF,SF,AF,AG,BC,HGF,EDG,H,EWR,CS,CV,BC,XZVC,BVC,ZX,SC,Z,SS,AC,VXC,AX,ZCX,VX,ADS,FBC,AD,QD,VC,ZXC,SAD,D,X,ZV,ZCSA,WD,ZVXZ,WE,DVQ,CX,ZCX,EF,CVDXV,VS,WSF,CX,AF,SS,AWD,AOS,WED,ERT\",\r\n" + 
				"        \"ExcludedPrevInclSupply\": \"K\"\r\n" + 
				"      }\r\n" + 
				"    }\r\n" + 
				"  ]\r\n" + 
				"}";
		
		return payload;
	}
}
