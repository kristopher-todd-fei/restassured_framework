package payLoads.product_channel_indicator;

public class MultipleItemIdsWithValidAndInvalidChannelIndicators {
	
	public static String multipleItemIds() {
		String payload = "{ \r\n" + 
				"   \"ItemIDs\":[ \r\n" + 
				"      { \r\n" + 
				"         \"ItemId\":\"C345354\",\r\n" + 
				"         \"Extended\":{ \r\n" + 
				"            \"IncludedInSupply\":\"S,N\",\r\n" + 
				"            \"ExcludedPrevInclSupply\":\"S\"\r\n" + 
				"         }\r\n" + 
				"      },\r\n" + 
				"      { \r\n" + 
				"         \"ItemId\":\"C345355\",\r\n" + 
				"         \"Extended\":{ \r\n" + 
				"            \"IncludedInBuild\":\"B\",\r\n" + 
				"            \"ExcludedPrevInclBuild\": \"B\"\r\n" + 
				"         }\r\n" + 
				"      },\r\n" + 
				"      { \r\n" + 
				"         \"ItemId\":\"C345356\",\r\n" + 
				"         \"Extended\":{ \r\n" + 
				"            \"IncludedInHMW\":\"H\",\r\n" + 
				"            \"ExcludedPrevInclHMW\":\"H\"\r\n" + 
				"         }\r\n" + 
				"      },\r\n" + 
				"      { \r\n" + 
				"         \"ItemId\":\"C345357\",\r\n" + 
				"         \"Extended\":{ \r\n" + 
				"            \"IncludedInSupply\":\"S,N\",\r\n" + 
				"            \"ExcludedPrevInclSupply\":\"N\",\r\n" + 
				"            \"IncludedInHMW\":\"H\",\r\n" + 
				"            \"ExcludedPrevInclHMW\":\"H\"\r\n" + 
				"         }\r\n" + 
				"      },\r\n" + 
				"       { \r\n" + 
				"         \"ItemId\":\"C345358\",\r\n" + 
				"         \"Extended\":{ \r\n" + 
				"            \"IncludedInSupply\":\"S,N\",\r\n" + 
				"            \"ExcludedPrevInclSupply\":\"K\"\r\n" + 
				"         }\r\n" + 
				"      },\r\n" + 
				"      { \r\n" + 
				"         \"ItemId\":\"C345359\",\r\n" + 
				"         \"Extended\":{ \r\n" + 
				"            \"IncludedInBuild\":\"B\",\r\n" + 
				"            \"ExcludedPrevInclBuild\": \"L\"\r\n" + 
				"         }\r\n" + 
				"      },\r\n" + 
				"      { \r\n" + 
				"         \"ItemId\":\"C345360\",\r\n" + 
				"         \"Extended\":{ \r\n" + 
				"            \"IncludedInHMW\":\"H\",\r\n" + 
				"            \"ExcludedPrevInclHMW\":\"P\"\r\n" + 
				"         }\r\n" + 
				"      },\r\n" + 
				"      { \r\n" + 
				"         \"ItemId\":\"C345361\",\r\n" + 
				"         \"Extended\":{ \r\n" + 
				"            \"IncludedInSupply\":\"S,N\",\r\n" + 
				"            \"ExcludedPrevInclSupply\":\"V\",\r\n" + 
				"            \"IncludedInHMW\":\"H\",\r\n" + 
				"            \"ExcludedPrevInclHMW\":\"Y\"\r\n" + 
				"         }\r\n" + 
				"      }\r\n" + 
				"   ]\r\n" + 
				"}\r\n" + 
				"";
		
		return payload;
	}
}
