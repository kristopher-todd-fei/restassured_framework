package payLoads.product_channel_indicator;

public class MessageWithMultipleItemId {
	
	public static String messageWMultipleItemIds() {
		String payload = "{\r\n" + 
				"  \"ItemIDs\": [\r\n" + 
				"    {\r\n" + 
				"      \"ItemId\": \"C345780\",\r\n" + 
				"      \"Extended\": {\r\n" + 
				"        \"IncludedInSupply\": \"S,N\",\r\n" + 
				"        \"ExcludedPrevInclSupply\": \"S\",\r\n" + 
				"        \"IncludedInHMW\": \"H\",\r\n" + 
				"        \"ExcludedPrevInclHMW\": \"H\"\r\n" + 
				"      }\r\n" + 
				"    },\r\n" + 
				"    {\r\n" + 
				"      \"ItemId\": \"C345781\",\r\n" + 
				"      \"Extended\": {\r\n" + 
				"        \"IncludedInSupply\": \"S,N\",\r\n" + 
				"        \"ExcludedPrevInclSupply\": \"S\"\r\n" + 
				"      }\r\n" + 
				"    },\r\n" + 
				"    {\r\n" + 
				"      \"ItemId\": \"C345782\",\r\n" + 
				"      \"Extended\": {\r\n" + 
				"        \"IncludedInHMW\": \"H\",\r\n" + 
				"        \"ExcludedPrevInclHMW\": \"H\"\r\n" + 
				"      }\r\n" + 
				"    },\r\n" + 
				"    {\r\n" + 
				"      \"ItemId\": \"C345783\",\r\n" + 
				"      \"Extended\": {\r\n" + 
				"        \"IncludedInBuild\": \"B\",\r\n" + 
				"        \"ExcludedPrevInclBuild\": \"B\"\r\n" + 
				"      }\r\n" + 
				"    },\r\n" + 
				"    {\r\n" + 
				"      \"ItemId\": \"C345784\",\r\n" + 
				"      \"Extended\": {\r\n" + 
				"        \"IncludedInSupply\": \"S,N\",\r\n" + 
				"        \"ExcludedPrevInclSupply\": \"N\"\r\n" + 
				"      }\r\n" + 
				"    }\r\n" + 
				"  ]\r\n" + 
				"}";
		
		return payload;
	}
}
