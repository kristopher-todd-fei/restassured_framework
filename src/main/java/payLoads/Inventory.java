package payLoads;

public class Inventory {
	
	public static String inventoryData(String type, String date, Long syncId) {
		
		String payload = "{\r\n" + 
				"  \"docType\": \"inventory\",\r\n" + 
				"  \"version\": 3,\r\n" + 
				"  \"events\": [\r\n" + 
				"    {\r\n" + 
				"      \"event\": {\r\n" + 
				"        \"system\": \"DEV\",\r\n" + 
				"        \"account\": \"OHVAL\",\r\n" + 
				"        \"file\": \"PRODUCT\",\r\n" + 
				"        \"record\": \"2249888*1980\",\r\n" + 
				"        \"author\": \"AAO8676\",\r\n" + 
				"        \"type\": \""+type+"\",\r\n" + 
				"        \"date\": \""+date+"\",\r\n" + 
				"        \"syncId\":\""+syncId+"\",\r\n" + 
				"        \"data\": {\r\n" + 
				"          \"header\": {\r\n" + 
				"            \"onHandBal\": 1,\r\n" + 
				"            \"availBal\": null,\r\n" + 
				"            \"onPoBal\": 0,\r\n" + 
				"            \"avgCostAmt\": 0\r\n" + 
				"          }\r\n" + 
				"        }\r\n" + 
				"      }\r\n" + 
				"    }\r\n" + 
				"  ]\r\n" + 
				"}";
		
		return payload;
		
	}

}
